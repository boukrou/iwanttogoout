I Want To Go Out
================

“I Want To Go Out” is a new way to create or attend activities around you.

## Install environment

`# apt install apache2 php5 php-mbstring php-intl php-gettext mariadb-client mariadb-server phpmyadmin`

## Setup Apache vhost

Edit `/etc/apache2/sites-available/000-default.conf` and add these lines at the bottom:
```
<VirtualHost *:80>
  ServerName iwanttogoout
  DocumentRoot /var/www/dev/iwanttogoout/public
  <Directory /var/www/dev/iwanttogoout/public>
    Options Indexes FollowSymLinks
    AllowOverride All
    Order deny,allow
    Allow from all
  </Directory>
</VirtualHost>
<VirtualHost *:80>
  ServerName www.iwanttogoout
  DocumentRoot /var/www/dev/iwanttogoout/public
  <Directory /var/www/dev/iwanttogoout/public>
    Options Indexes FollowSymLinks
    AllowOverride All
    Order deny,allow
    Allow from all
  </Directory>
</VirtualHost>
<VirtualHost *:80>
  ServerName api.iwanttogoout
  DocumentRoot /var/www/dev/iwanttogoout/public
  <Directory /var/www/dev/iwanttogoout/public>
    Options Indexes FollowSymLinks
    AllowOverride All
    Order deny,allow
    Allow from all
  </Directory>
</VirtualHost>
<VirtualHost *:80>
  ServerName bo.iwanttogoout
  DocumentRoot /var/www/dev/iwanttogoout/public
  <Directory /var/www/dev/iwanttogoout/public>
    Options Indexes FollowSymLinks
    AllowOverride All
    Order deny,allow
    Allow from all
  </Directory>
</VirtualHost>
```

Reload apache:

`# /etc/init.d/apache2 reload`

Attribute hostname to your localhost by editing `/etc/hosts` and append `iwanttogoout` at the end of line of your localhost.
e.g.:
```
127.0.0.1       localhost iwanttogoout www.iwanttogoout api.iwanttogoout bo.iwanttogoout
```

## Access

`http://iwanttogoout`

## Git repository

Clone bitbucket repository:

`$ mkdir /path/to/your/project`

`$ cd /path/to/your/project`

`$ git clone git@bitbucket.org:boukrou/iwanttogoout.git`

Config your local repository:

`$ git config user.name "John Doe"`

`$ git config user.email "john.doe@domain.tld"`

## Data

SQL Dump file can be found in `server/data/` folder, just import the file you want via phpmyadmin or :

`$ mysql -h localhost -u iwtgodev -piwtgodev iwtgodev < dump.sql`

Shell Script to cleanup DB and insert some mockup datas can be found in `server/data/` folder :

`$ ./setupDb.sh`