<?php

namespace Repository;

use Exception;
use Silex\Application;
use Repository\RepositoryAbstract;;
use Repository\MysqlRepositoryTrait;

/**
 * Events repository
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class EventsRepository extends RepositoryAbstract
{
    use EntityRepositoryTrait;

    public function create(
        array $params = [],
        array $options = []
    ) {
        $this->validateValueOrThrowException($params, new \Validator\Events\SaveRequest($options));

        if (!empty($params['location'])) {
            $params['location'] = $params['location']['latitude'] . ',' . $params['location']['longitude'];
        }

        $params['status'] = \Validator\StatusChoice::VALID;

        $u = $this->app['repositories.users']->findById($params['user_id']);
        $params['team_id'] = $u->team_id;

        $event = $this->firstOrCreate(
            $params
        );

        $this->app['repositories.eventsvotes']->create([
            'user_id'   => $u->id,
            'event_id'  => $event->id,
            'vote_type' => \Validator\EventsVotes\EventsVotesTypeChoice::GO
        ]);

        return $event;
    }

    public function save(
        int $eventId,
        array $params = [],
        array $options = []
    ) {
        $this->validateValueOrThrowException($params, new \Validator\Events\SaveRequest($options));

        if (!empty($params['location'])) {
            $params['location'] = $params['location']['latitude'] . ',' . $params['location']['longitude'];
        }

        $params['updated_at'] = round(microtime(true));

        return $this->findByIdAndUpdate(
            $eventId,
            $params
        );
    }

    public function changeStatus(
        int $eventId,
        array $params = []
    ) {
        $this->validateValueOrThrowException(
            $params,
            new \Validator\Events\ChangeStatusRequest()
        );

        $ts     = round(microtime(true));
        $update = ['updated_at' => $ts, 'status' => $params['new_status']];

        switch ($params['new_status']) {
            case \Validator\Events\StatusChoice::MODERATED:
            case \Validator\Events\StatusChoice::DELETED_PENDING:
            case \Validator\Events\StatusChoice::DELETED:
                break;
        }

        $event = $this->findByIdAndUpdate(
            $eventId,
            $update
        );

        $ok = !empty($event->id);

        if (!$ok) {
            throw new \Repository\Exception\NotFoundException(
                'Event not found'
            );
        }

        return ['ok' => $ok, 'ts' => $ts];
    }
}
