<?php

namespace Repository;

use UnexpectedValueException;
use Silex\Application;

trait SecurityRepositoryTrait
{
    /**
     * @var string securityAlgo the hash algorithme for generate hashed password
     */
    protected $securityAlgo = 'sha1';

    /**
     * @var securitySharedSalt the shared salt used when security strategie is 'sharedsalt'
     */
    protected $securitySharedSalt = '219340800000';

    /**
     * Constructor
     *
     * @param Silex\Application $this->app silex application container instance
     */
    public function initializeSecurity()
    {
        if (!in_array($this->securityAlgo, hash_algos())) {
            throw new UnexpectedValueException(
                sprintf(
                    '%s - %s are unknown',
                    __METHOD__,
                    $this->securityAlgo
                )
            );
        }
    }

    /**
     * generate hashed password fields this
     *
     * @param string $password password must be hashed
     * @param string $salt     default uniq() salt must be used
     *
     * @return array with hashed password def $ret['salt'] for salt and $ret['hashed_password'] for hashed password
     */
    protected function generateHashedPasswordField($password, $salt = null)
    {
        $salt = $salt ?: $this->securitySharedSalt;

        return array(
            'salt'            => $salt,
            'hashed_password' => hash_hmac(
                $this->securityAlgo,
                $password,
                $salt
            ),
        );
    }

}
