<?php

namespace Repository;

use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Exception as SymfonyException;
use Silex\Application;
use Repository\Exception as RepositoryException;

/**
 * Abstract class for repository classes
 *
 * @author Jean-Sébastien Eude <jeude@gmail.com>
 */
abstract class RepositoryAbstract
{
    /**
     * @const DEFAULT_SEARCH_RESULTS_LIMIT Default number of search results
     */
    const DEFAULT_SEARCH_RESULTS_LIMIT = 50;

    /**
     * @var Silex\Application $app  silex application container instance
     **/
    protected $app;

    /**
     * Constructor
     *
     * @param Silex\Application $app silex application container instance
     */
    public function __construct(
        Application $app,
        $model
    ) {
        $this->app   = $app;
        $this->model = $model;
    }

    public function runPaginatedSearch(
        $params  = [],
        $options = [],
        $columns = ['*']
    ) {
        $limit = static::DEFAULT_SEARCH_RESULTS_LIMIT;
        if (!empty($options['limit']) && (int) $options['limit'] > 0) {
            $limit = (int) $options['limit'];
        }

        $offset = 1;
        if (!empty($options['offset']) && (int) $options['offset'] > 0) {
            $offset = ((int) $options['offset'] - 1) * $limit;
        }

        $events = $this->find(
            array_merge(
                $params,
                [
                    'take'   => [$limit + 1],
                    'offset' => [$offset],
                ]
            ),
            $columns
        );

        $hasMore = false;
        if (count($events->toArray()) > $limit) {
            $hasMore = true;
            $events->pop();
        }

        return [
            'data'     => $events,
            'has_more' => $hasMore
        ];
    }

    protected function validateValue(
        $resource,
        Constraint $constraint,
        $throwException = false
    ) {
        if (is_null($resource)) {
            $resource = [];
        }
        try {
            $errors = $this->app['validator']->validateValue(
                $resource,
                $constraint
            );
        } catch(SymfonyException\UnexpectedTypeException $e) {
            $errors = new ConstraintViolationList([
                new ConstraintViolation(
                    'Your request has a type violation',
                    'Your request has a type violation',
                    [],
                    'Root',
                    '[BAD_REQUEST]',
                    null
                )
            ]);
        }

        if ($throwException && count($errors) > 0) {
            throw new RepositoryException\InvalidDataException(
                'Validation failed',
                $errors
            );
        }

        return $errors;
    }

    protected function validateValueOrThrowException(
        $resource,
        Constraint $constraint
    ) {
        return $this->validateValue(
            $resource,
            $constraint,
            true
        );
    }
}
