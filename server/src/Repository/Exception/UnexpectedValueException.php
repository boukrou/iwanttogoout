<?php

namespace Repository\Exception;

use UnexpectedValueException as BaseUnexpectedValueException;

/**
 * UnexpectedValueException occurred when a bad parameter is set in method call in repository classes occur
 *
 * @author Jean-Sébastien Eude <jeude@gmail.com>
 */
class UnexpectedValueException extends BaseUnexpectedValueException implements RepositoryExceptionInterface
{
    use RepositoryExceptionTrait;
}
