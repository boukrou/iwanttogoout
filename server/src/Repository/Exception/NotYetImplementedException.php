<?php

namespace Repository\Exception;

/**
 * NotYetImplementedException occured feature is not yet implemented
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class NotYetImplementedException extends RuntimeException
{
}
