<?php

namespace Repository\Exception;

/**
 * All repository exceptions must implement this interface
 *
 * @author Jean-Sébastien Eude <jeude@gmail.com>
 */
interface RepositoryExceptionInterface
{
    /**
     * Extends exception message
     *
     * @return $this for chaining
     */
    public function setMessage($message);
}
