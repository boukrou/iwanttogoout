<?php

namespace Repository\Exception;

/**
 * NotFoundException occurred when a object is not found when it's required
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class NotFoundException extends RuntimeException
{
    //do ntohing at this time
}
