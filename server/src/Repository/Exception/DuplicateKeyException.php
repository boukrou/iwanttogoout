<?php

namespace Repository\Exception;

/**
 * DuplicateKeyException occur when a duplicate entry is not allowed
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class DuplicateKeyException extends RuntimeException
{
    //do nothing at this time
}
