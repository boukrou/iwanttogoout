<?php

namespace Repository\Exception;

/**
 * Implement setMessage for RepositoryExceptionInterface
 *
 * @author Jean-Sébastien Eude <jeude@gmail.com>
 */
trait RepositoryExceptionTrait
{
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }
}
