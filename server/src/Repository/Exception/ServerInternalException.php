<?php

namespace Repository\Exception;

/**
 * ServerInternalException occur when a internal server occur we must cath it and return an 500 http status code
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class ServerInternalException extends RuntimeException
{
    //do nothing at this time
}
