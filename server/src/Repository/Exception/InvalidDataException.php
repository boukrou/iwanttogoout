<?php

namespace Repository\Exception;

use IteratorAggregate;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * InvalidDataException occurred when a validation error occur in method call in repository classes
 *
 * @author Jean-Sébastien Eude <jeude@gmail.com>
 */
class InvalidDataException extends UnexpectedValueException implements RepositoryExceptionInterface
{
    /**
     * @var array errors is associatif errors on field
     */
    private $errors = array();

    /**
     * __constructor is extended for set errors property
     *
     * @param string            $message exception message
     * @param IteratorAggregate $errors  error list
     * @param mixed             $code    exception code
     *
     * @return void
     **/
    public function __construct($message = null, IteratorAggregate $errors = null, $code = 0)
    {
        if (!empty($errors)) {
            foreach ($errors as $error) {
                if ($error instanceof ConstraintViolationInterface) {
                    $propertyPath = $error->getPropertyPath();
                    $this->errors[$propertyPath] =
                        empty($this->errors[$propertyPath])
                            ? $error->getMessage()
                            : $this->errors[$propertyPath] . "\n" . $error->getMessage();
                }
            }
        }
        parent::__construct($message, $code);
    }

    /**
     * Return errors property
     *
     * @return array errors property
     **/
    public function getErrors()
    {
        return $this->errors;
    }
}
