<?php

namespace Repository\Exception;

use RuntimeException as BaseRuntimeException;

/**
 * RuntimeException is base classe for some RepositoryException
 *
 * @author Jean-Sébastien Eude <jeude@gmail.com>
 */
class RuntimeException extends BaseRuntimeException implements RepositoryExceptionInterface
{
    use RepositoryExceptionTrait;
}
