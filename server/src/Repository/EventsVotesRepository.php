<?php

namespace Repository;

use Exception;
use Silex\Application;
use Repository\RepositoryAbstract;;
use Repository\MysqlRepositoryTrait;

/**
 * Events Votes repository
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class EventsVotesRepository extends RepositoryAbstract
{
    use EntityRepositoryTrait;

    public function create(
        array $params = []
    ) {
        $this->validateValueOrThrowException(
            $params,
            new \Validator\EventsVotes\CreateRequest()
        );

        $ev = $this->firstOrCreate(
            $params
        );

        $params['status'] = \Validator\StatusChoice::VALID;

        return $ev->update(
            $params
        );
    }

    public function remove(
        array $params = []
    ) {
        $this->validateValueOrThrowException(
            $params,
            new \Validator\EventsVotes\CreateRequest()
        );

        $ev = $this->find([
            'where' => [$params]
        ])->first();

        if (!empty($ev)) {
            $params['status'] = \Validator\StatusChoice::DELETED;

            return $ev->update(
                $params
            );
        }

        return false;
    }
}
