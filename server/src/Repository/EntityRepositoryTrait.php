<?php

namespace Repository;

use Entity;
use Validator;
use Repository\Exception as RepositoryException;

/**
 * Trait with usefull methods for entity classes 
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
trait EntityRepositoryTrait
{
    /**
     * Global bethod to find items from model
     * 
     * @param array $options    Array with model method as key and params as value
     * @param array $columns    Array of columns to filter query
     * @return \Illuminate\Support\Collection|static|null
     */
    public function find(array $options = [], array $columns = ['*'])
    {
        $collection = $this->model;
        foreach ($options as $option => $value) {
            $collection = call_user_func_array([$collection, $option], $value);
        }

        return $collection->get($columns);
    }

    /**
     * Find specific item by its id
     * 
     * @param int $id           Array with model method as key and params as value
     * @param array $columns    Array of columns to filter query
     * @return \Illuminate\Support\Collection|static|null
     */
    public function findById($id, array $columns = ['*'])
    {
        if (!empty($id)) {
            return $this->model->find($id, $columns);
        } else {
            return false;
        }
    }

    /**
     * Find specific item by its id with relations
     *
     * @param int $id           Array with model method as key and params as value
     * @param array $columns    Array of columns to filter query
     * @return \Illuminate\Support\Collection|static|null
     */
    public function findByIdWith($id, $with, array $columns = ['*'])
    {
        if (!empty($id)) {
            return $this->model->with($with)->find($id, $columns);
        } else {
            return false;
        }
    }

    /**
     * Get all of the models from the database.
     * 
     * @param array $columns    Array of columns to filter query
     * @param mixed $groupBy    Add a "group by" clause to the query
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all($columns = ['*'], $groupBy = false)
    {
        if ($groupBy) {
            return $this->model->groupBy($groupBy)->get($columns);
        }
        return $this->model->all($columns);
    }

    /**
     * Get all of the models with their relationships from the database.
     * 
     * @param array $with       Set the relationships that should be eager loaded
     * @param array $columns    Array of columns to filter query
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function allWith($with, $columns = ['*'])
    {
        return $this->model->with($with)->get($columns);
    }

    /**
     * Get the first record matching the attributes or create it.
     *
     * @param  array  $attributes
     * @return static
     */
    public function firstOrCreate(array $attributes)
    {
        return $this->model->firstOrCreate($attributes);
    }

    /**
     * Save a new model and return the instance.
     *
     * @param  array  $attributes
     * @return static
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * Save a new model model and return the id
     *
     * @param  array  $attributes
     * @return int
     */
    public function createAndReturnId(array $attributes)
    {
        return $this->model->create($attributes)->id;
    }

    /**
     * Save the model and all of its relationships.
     *
     * @return bool
     */
    public function push()
    {
        $this->model->push();
    }

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = array())
    {
        return $this->model->save($options);
    }

    /**
     * Update the model in the database.
     *
     * @param  array  $attributes
     * @return bool|int
     */
    public function update(array $attributes = array())
    {
        return $this->model->update($attributes);
    }

    /**
     * Find model by id and update the model in the database.
     *
     * @param  int    $id
     * @param  array  $attributes
     * @return bool|int
     */
    public function findByIdAndUpdate($id, array $input)
    {
        $entity = $this->model->find($id);
        if (!empty($entity)) {
            $entity->update($input);
            return $entity;
        }

        return false;
    }
}
