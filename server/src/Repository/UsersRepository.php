<?php

namespace Repository;

use Exception;
use Silex\Application;
use Repository\RepositoryAbstract;
use Repository\MysqlRepositoryTrait;

use Entity\Users;

/**
 * User repository
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class UsersRepository extends RepositoryAbstract
{
    use EntityRepositoryTrait, SecurityRepositoryTrait;

    public function __construct(
        Application $app,
        Users       $model
    ) {
        parent::__construct($app, $model);
        $this->initializeSecurity();
    }

    public function create(
        array $params = []
    ) {
        $this->validateValueOrThrowException($params, new \Validator\Users\CreateRequest());

        $hashedPassword = $this->generateHashedPasswordField($params['password']);
        $attributes     = [
            'username' => strtok($params['email'], '@'),
            'email'    => $params['email'],
            'team_id'  => $params['team_id'],
            'password' => $hashedPassword['hashed_password'],
            'salt'     => $hashedPassword['salt'],
            'status'   => \Validator\StatusChoice::VALID
        ];

        try {
            return $this->firstOrCreate(
                $attributes
            );
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                throw new \Repository\Exception\DuplicateKeyException(
                    'Email already exists'
                );
            } else {
                throw $e;
            }
        }
    }

    public function save(
        int $userId,
        array $params = []
    ) {
        $this->validateValueOrThrowException($params, new \Validator\Users\SaveRequest());

        $attributes = $params;
        if (!empty($params['password'])) {
            $hashedPassword         = $this->generateHashedPasswordField($params['password']);
            $attributes['password'] = $hashedPassword['hashed_password'];
        }

        if (!empty($params['picture'])) {
            $picture = $this->app['utils.images']->upload($params['picture']);
            if (!empty($picture['filename'])) {
                $attributes['picture'] = $picture['filename'];
            }
        }

        $ts          = round(microtime(true));
        $attributes += [
            'updated_at'  => $ts,
            'activity_at' => $ts,
        ];

        try {
            return $this->findByIdAndUpdate(
                $userId,
                $attributes
            );
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                throw new \Repository\Exception\DuplicateKeyException(
                    'Email already exists'
                );
            } else {
                throw $e;
            }
        }
    }

    public function changeStatus(
        int $userId,
        array $params = []
    ) {
        $this->validateValueOrThrowException(
            $params,
            new \Validator\Users\ChangeStatusRequest()
        );

        $ts     = round(microtime(true));
        $update = ['updated_at' => $ts, 'status' => $params['new_status']];

        switch ($params['new_status']) {
            case \Validator\Users\StatusChoice::MODERATED:
            case \Validator\Users\StatusChoice::DELETED_PENDING:
            case \Validator\Users\StatusChoice::DELETED:
                //this fields must be reset
                $update['username'] = '';
                break;
        }

        $user = $this->findByIdAndUpdate(
            $userId,
            $update
        );

        $ok = !empty($user->id);

        if (!$ok) {
            throw new \Repository\Exception\NotFoundException(
                'User not found'
            );
        }

        switch ($params['new_status']) {
            case \Validator\Users\StatusChoice::MODERATED:
            case \Validator\Users\StatusChoice::DELETED_PENDING:
            case \Validator\Users\StatusChoice::DELETED:
                // TODO : delete related events
                break;
        }

        return ['ok' => $ok, 'ts' => $ts];
    }

    public function changeAcl(
        int $userId,
        array $params = []
    ) {
        $this->validateValueOrThrowException(
            $params,
            new \Validator\Users\ChangeAclRequest()
        );

        $ts     = round(microtime(true));
        $update = ['updated_at' => $ts, 'acl' => $params['new_acl']];

        $user = $this->findByIdAndUpdate(
            $userId,
            $update
        );

        $ok = !empty($user->id);

        if (!$ok) {
            throw new \Repository\Exception\NotFoundException(
                'User not found'
            );
        }

        return ['ok' => $ok, 'ts' => $ts];
    }
}
