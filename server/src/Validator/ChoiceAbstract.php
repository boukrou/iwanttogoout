<?php

namespace Validator;

use Symfony\Component\Validator\Constraints\Choice;

abstract class ChoiceAbstract extends Choice
{
    public function __construct($options = null)
    {
        if (!is_array($options)) {
            $options = array();
        }
        $options = array_merge_recursive(
            $options,
            array(
                'choices' => static::getChoices()
            )
        );
        parent::__construct($options);
    }

    public function validatedBy()
    {
        return 'Symfony\Component\Validator\Constraints\ChoiceValidator';
    }

    public static function getChoicesCombined()
    {
        $choices = static::getChoices();

        return array_combine($choices, $choices);
    }
}
