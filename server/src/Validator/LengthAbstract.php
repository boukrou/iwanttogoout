<?php

namespace Validator;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * {@inheritDoc}
 */
abstract class LengthAbstract extends Assert\Length
{
    public function validatedBy()
    {
        return 'Symfony\Component\Validator\Constraints\LengthValidator';
    }
}
