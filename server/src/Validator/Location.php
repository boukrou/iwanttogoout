<?php

namespace Validator;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * {@inheritDoc}
 */
class Location extends CollectionAbstract
{
    /**
     * {@inheritDoc}
     */
    public function __construct($options = null)
    {
        if (!is_array($options)) {
            $options = array();
        }
        $options = array_merge_recursive(
            $options,
            array(
                'fields' => array(
                    'latitude' => new Assert\Range(
                        array(
                            'min' => -90,
                            'max' => 90
                        )
                    ),
                    'longitude' => new Assert\Range(
                        array(
                            'min' => -180,
                            'max' => 180
                        )
                    )
                )
            )
        );
        parent::__construct($options);
    }
}
