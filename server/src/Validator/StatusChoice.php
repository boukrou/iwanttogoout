<?php

namespace Validator;

use Symfony\Component\Validator\Constraints\Choice;

class StatusChoice extends ChoiceAbstract
{
    /**
     * @const string MODERATION_PENDING moderation_pending
     */
    const VALID = '1';

    /**
     * @const string MODERATION_PENDING moderation_pending
     */
    const MODERATION_PENDING = '2';

    /**
     * @const string MODERATED moderated
     */
    const MODERATED = '3';

    /**
     * @const string REAL_TIME real_time
     */
    const DELETED_PENDING = '4';

    /**
     * @const string REAL_TIME real_time
     */
    const DELETED = '5';

    /**
     * Return all choices
     *
     * @return array
     */
    public static function getChoices()
    {
        return [
            self::VALID,
            self::MODERATION_PENDING,
            self::MODERATED,
            self::DELETED_PENDING,
            self::DELETED,
        ];
    }

    /**
     * Returns all choices with labels
     *
     * @return array
     */
    public static function getChoicesAndLabels()
    {
        return [
            self::VALID              => 'Valid',
            self::MODERATION_PENDING => 'Moderation pending',
            self::MODERATED          => 'Moderated',
            self::DELETED_PENDING    => 'Deletion pending',
            self::DELETED            => 'Deleted',
        ];
    }
}