<?php

namespace Validator;

use RuntimeException;
use Symfony\Component\Validator\Constraints\Regex;

abstract class RegexAbstract extends Regex
{
    const PATTERN = null;

    public function __construct($options = null)
    {
        if (!empty($options['pattern'])) {
            $this->pattern = $options['pattern'];
            unset($options['pattern']);
        }
        if (null === $this->pattern) {
            if (null === static::PATTERN) {
                throw new RuntimeException(
                    sprintf(
                        '$options[\'pattern\'] or %s::pattern property or %s::PATTERN class constant is not set',
                        get_class($this),
                        get_class($this)
                    )
                );
            } else {
                $this->pattern = static::PATTERN;
            }
        }
        if (!is_array($options)) {
            $options = array();
        }
        $options = array_merge(
            $options,
            array(
                'pattern' => $this->pattern
            )
        );

        return parent::__construct($options);
    }

    public function validatedBy()
    {
        return 'Symfony\Component\Validator\Constraints\RegexValidator';
    }
}
