<?php

namespace Validator\EventsVotes;

use Symfony\Component\Validator\Constraints as Assert;
use Validator;

/**
 * {@inheritDoc}
 */
class CreateRequest extends Validator\CollectionAbstract
{
    /**
     * {@inheritDoc}
     */
    public function __construct($options = null)
    {
        if (!is_array($options)) {
            $options = [];
        }
        $options = array_merge_recursive(
            $options,
            ['fields' => ['vote_type' => [new Assert\NotBlank(), new Validator\EventsVotes\EventsVotesTypeChoice()],
                          'user_id'   => [new Assert\NotBlank(), new Assert\GreaterThan(0)],
                          'event_id'  => [new Assert\NotBlank(), new Assert\GreaterThan(0)]],
            ]
        );
        parent::__construct($options);
    }
}
