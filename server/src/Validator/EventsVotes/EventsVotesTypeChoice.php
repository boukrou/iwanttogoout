<?php

namespace Validator\EventsVotes;

use Validator;

class EventsVotesTypeChoice extends Validator\ChoiceAbstract
{
    const GO      = '1';

    /**
     * Return all choices
     *
     * @return array
     */
    public static function getChoices()
    {
        return [
            self::GO,
        ];
    }
}