<?php

namespace Validator\Events;

use Validator\LengthAbstract;

/**
 * {@inheritDoc}
 */
class Title extends LengthAbstract
{
    /**
     * {@inheritDoc}
     */
    public $max = 160;

    /**
     * {@inheritDoc}
     */
    public $min = 5;
}
