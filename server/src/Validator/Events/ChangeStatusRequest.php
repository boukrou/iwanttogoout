<?php

namespace Validator\Events;

use Symfony\Component\Validator\Constraints as Assert;
use Validator;

/**
 * {@inheritDoc}
 */
class ChangeStatusRequest extends Validator\CollectionAbstract
{
    /**
     * {@inheritDoc}
     */
    public function __construct($options = null)
    {
        if (!is_array($options)) {
            $options = [];
        }
        $options = array_merge_recursive(
            $options,
            ['fields' => [
                'new_status' => new Validator\Events\StatusChoice(),
            ]]
        );
        parent::__construct($options);
    }
}
