<?php

namespace Validator\Events;

use Validator;

class EventTypeChoice extends Validator\ChoiceAbstract
{
    const GYM      = '1';

    const POKESTOP = '2';

    const STROLL   = '3';

    /**
     * Return all choices
     *
     * @return array
     */
    public static function getChoices()
    {
        return [
            self::GYM,
            self::POKESTOP,
            self::STROLL,
        ];
    }

    /**
     * Returns all choices with labels
     *
     * @return array
     */
    public static function getChoicesAndLabels()
    {
        return [
            self::GYM      => 'PokéGym',
            self::POKESTOP => 'PokéStop',
            self::STROLL   => 'Group walk',
        ];
    }

    /**
     * Returns one choice by id
     *
     * @return array
     */
    public static function getChoiceById($id)
    {
        return self::getChoice()[$id];
    }
}