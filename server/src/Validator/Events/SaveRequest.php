<?php

namespace Validator\Events;

use Symfony\Component\Validator\Constraints as Assert;
use Validator;

/**
 * {@inheritDoc}
 */
class SaveRequest extends Validator\CollectionAbstract
{
    /**
     * {@inheritDoc}
     */
    public function __construct($options = null)
    {
        if (!is_array($options)) {
            $options = [];
        }
        $options = array_merge_recursive(
            $options,
            ['fields' => ['user_id'        => [new Assert\NotBlank(), new Assert\GreaterThan(0)],
                          'start_at'       => [new Assert\NotBlank(), new Assert\Type('integer')],
                          'event_type'     => [new Assert\NotBlank(), new Validator\Events\EventTypeChoice()],
                          'location'       => [new Assert\NotBlank(), new Validator\Location()],
                          'location_data'  => [new Assert\NotBlank(), new Validator\Text()],
                          'team_id'        => new Assert\Optional([new Assert\NotBlank(), new Validator\Users\TeamChoice()]),
                          'title'          => new Assert\Optional([new Validator\Events\Title()]),
                          'description'    => new Assert\Optional([new Validator\Text()]),
                          'end_at'         => new Assert\Optional([new Assert\Type('integer')])],
            ]
        );
        parent::__construct($options);
    }
}
