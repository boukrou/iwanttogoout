<?php

namespace Validator;

use Symfony\Component\Validator\Constraints\Collection;

/**
 * {@inheritDoc}
 */
abstract class CollectionAbstract extends Collection
{
    /**
     * {@inheritDoc}
     */
    public function validatedBy()
    {
        return 'Symfony\Component\Validator\Constraints\CollectionValidator';
    }
}
