<?php

namespace Validator;

/**
 * {@inheritDoc}
 */
class Text extends LengthAbstract
{
    /**
     * {@inheritDoc}
     */
    public $max = 10000;

    /**
     * {@inheritDoc}
     */
    public $min = 10;
}
