<?php

namespace Validator;

use Symfony\Component\Validator\Constraints\Image;

/**
 * {@inheritDoc}
 */
abstract class ImageAbstract extends Image
{
    public $mimeTypes = 'image/jpeg';
    public $minWidth  = 50;
    public $minHeight = 50;
    public $maxHeight = 2048;
    public $maxWidth  = 2048;
    public $maxSize   = "4M";

    public function validatedBy()
    {
        return 'Symfony\Component\Validator\Constraints\ImageValidator';
    }
}
