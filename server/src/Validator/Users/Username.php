<?php

namespace Validator\Users;

use Validator\LengthAbstract;

/**
 * {@inheritDoc}
 */
class Username extends LengthAbstract
{
    /**
     * {@inheritDoc}
     */
    public $max = 20;

    /**
     * {@inheritDoc}
     */
    public $min = 1;
}
