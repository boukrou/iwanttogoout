<?php

namespace Validator\Users;

use Symfony\Component\Validator\Constraints as Assert;
use Validator;

/**
 * {@inheritDoc}
 */
class SaveRequest extends Validator\CollectionAbstract
{
    /**
     * {@inheritDoc}
     */
    public function __construct($options = null)
    {
        if (!is_array($options)) {
            $options = [];
        }
        $options = array_merge_recursive(
            $options,
            ['fields' => ['password'    => new Assert\Optional([new Assert\NotBlank(), new Validator\Users\Password()]),
                          'email'       => new Assert\Optional([new Assert\NotBlank(), new Assert\Email()]),
                          'username'    => new Assert\Optional([new Assert\NotBlank(), new Validator\Users\Username()]),
                          'team_id'     => new Assert\Optional([new Assert\NotBlank(), new Validator\Users\TeamChoice()]),
                          'description' => new Assert\Optional([new Validator\Text()]),
                          'picture'     => new Assert\Optional([new Validator\Users\Picture()])],
            ]
        );
        parent::__construct($options);
    }
}
