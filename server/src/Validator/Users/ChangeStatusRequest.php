<?php

namespace Validator\Users;

use Symfony\Component\Validator\Constraints as Assert;
use Validator;

/**
 * {@inheritDoc}
 */
class ChangeStatusRequest extends Validator\CollectionAbstract
{
    /**
     * {@inheritDoc}
     */
    public function __construct($options = null)
    {
        if (!is_array($options)) {
            $options = [];
        }
        $options = array_merge_recursive(
            $options,
            ['fields' => [
                'new_status' => new Validator\Users\StatusChoice(),
            ]]
        );
        parent::__construct($options);
    }
}
