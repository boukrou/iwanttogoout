<?php

namespace Validator\Users;

use Symfony\Component\Validator\Constraints as Assert;
use Validator;

/**
 * {@inheritDoc}
 */
class ChangeAclRequest extends Validator\CollectionAbstract
{
    /**
     * {@inheritDoc}
     */
    public function __construct($options = null)
    {
        if (!is_array($options)) {
            $options = [];
        }
        $options = array_merge_recursive(
            $options,
            ['fields' => [
                'new_acl' => new Validator\Users\BoAclTypeChoice(),
            ]]
        );
        parent::__construct($options);
    }
}
