<?php

namespace Validator\Users;

class BoAclTypeChoice extends \Validator\ChoiceAbstract
{
    /**
     * @const string User for user roles
     */
    const USER  = 0;

    /**
     * @const string ADMIN for admin roles
     */
    const ADMIN = 100;

    /**
     * @const string EDITO for editor roles
     */
    const EDITO = 200;

    /**
     * @const string MODO for moderator roles
     */
    const MODO = 300;

    /**
     * Return all status choices
     *
     * @return array
     */
    public static function getChoices()
    {
        return array(
            self::USER,
            self::ADMIN,
            self::MODO,
            self::EDITO
        );
    }

    /**
     * Returns all choices with labels
     *
     * @return array
     */
    public static function getChoicesAndLabels()
    {
        return [
            self::USER  => 'User',
            self::ADMIN => 'Admin',
            self::MODO  => 'Modo',
            self::EDITO => 'Edito',
        ];
    }
}
