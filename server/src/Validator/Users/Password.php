<?php

namespace Validator\Users;

/**
 * {@inheritDoc}
 */
class Password extends \Validator\LengthAbstract
{
    /**
     * {@inheritDoc}
     */
    public $max = 45;

    /**
     * {@inheritDoc}
     */
    public $min = 6;
}
