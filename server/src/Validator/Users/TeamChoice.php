<?php

namespace Validator\Users;

use Symfony\Component\Validator\Constraints\Choice;

class TeamChoice extends \Validator\ChoiceAbstract
{
    const RED    = '1';

    const BLUE   = '2';

    const YELLOW = '3';

    /**
     * Return all choices
     *
     * @return array
     */
    public static function getChoices()
    {
        return [
            self::RED,
            self::BLUE,
            self::YELLOW,
        ];
    }

    /**
     * Returns all choices with labels
     *
     * @return array
     */
    public static function getChoicesAndLabels()
    {
        return [
            self::RED    => 'Red',
            self::BLUE   => 'Blue',
            self::YELLOW => 'Yellow',
        ];
    }

    /**
     * Returns one choice by id
     *
     * @return array
     */
    public static function getChoiceById($id)
    {
        return self::getChoice()[$id];
    }
}