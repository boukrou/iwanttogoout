<?php

namespace Validator\Users;

use Symfony\Component\Validator\Constraints as Assert;
use Validator;

/**
 * {@inheritDoc}
 */
class CreateRequest extends Validator\CollectionAbstract
{
    /**
     * {@inheritDoc}
     */
    public function __construct($options = null)
    {
        if (!is_array($options)) {
            $options = [];
        }
        $options = array_merge_recursive(
            $options,
            ['fields' => ['password'    => [new Assert\NotBlank(), new Validator\Users\Password()],
                          'email'       => [new Assert\NotBlank(), new Assert\Email()],
                          'team_id'     => [new Assert\NotBlank(), new Validator\Users\TeamChoice()],
                          'username'    => new Assert\Optional([new Assert\NotBlank(), new Validator\Users\Username()]),
                          'description' => new Assert\Optional([new Validator\Text()])],
            ]
        );
        parent::__construct($options);
    }
}
