<?php

namespace Provider\Api;

use StdClass;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Silex\Application;
use Silex\ServiceProviderInterface;

use HttpKernel\Exception\HttpExceptionInterface as MyHttpExceptionInterface;
use Validator;

/**
 * This service provider is used for declared api routing rules
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class RoutesServiceProvider implements ServiceProviderInterface
{
    /**
     * Registration for api workflow
     *
     * "register" method for class interface Silex\ServiceProviderInterface
     *
     * nothing as added to containner
     *
     * @param Silex\Application $app silex application container instance
     *
     * @return void
     */
    public function register(Application $app)
    {
        $app['routes.middleware.validoffsetor404'] = $app->protect(
            function (Request $request) use ($app) {
                $offset = $app['request']->get('offset');
                $isInt = (is_string($offset))
                    ? ctype_digit($offset)
                    : is_int($offset);
                if (!$isInt) {
                    if ($app['debug']) {
                        return $app->abort(
                            400,
                            'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo(),
                            ['[offset] ' . $offset . ' invalid ']
                        );
                    }
                    return $app->abort(
                        404,
                        'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo()
                    );
                }
            }
        );

        $app['routes.middleware.validlocationor404'] = $app->protect(
            function (Request $request) use ($app) {
                $location = [
                    'latitude'  => $app['request']->get('latitude'),
                    'longitude' => $app['request']->get('longitude')
                ];
                $errors = $app['validator']->validateValue(
                    $location,
                    new Validator\Location()
                );
                if (count($errors) > 0) {
                    if ($app['debug']) {
                        $data = [];
                        foreach ($errors as $error) {
                            $data[] = $error->getPropertyPath() . ' ' . $error->getMessage();
                        }
                        return $app->abort(
                            400,
                            'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo(),
                            $data
                        );
                    }
                    return $app->abort(
                        404,
                        'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo()
                    );
                }
            }
        );

        $app->get(
            '/',
            'api.controllers.index:indexAction'
        )->bind(
            'api_index_index'
        );

        /* Users */
        $app->get(
            '/users/{userId}',
            'api.controllers.users:showAction'
        );
        $app->put(
            '/users',
            'api.controllers.users:createAction'
        );
        $app->post(
            '/users/{userId}',
            'api.controllers.users:editAction'
        )->before(
            $app['api.auth.authenticationowneronly']
        );
        $app->delete(
            '/users/{userId}',
            'api.controllers.users:removeAction'
        )->before(
            $app['api.auth.authenticationowneronly']
        );

        /* Events */
        $app->put(
            '/users/{userId}/events',
            'api.controllers.events:createAction'
        )->before(
            $app['api.auth.authenticationowneronly']
        );
        $app->post(
            '/users/{userId}/events/{eventId}',
            'api.controllers.events:editAction'
        )->before(
            $app['api.auth.authenticationowneronly']
        );
        $app->get(
            '/users/{userId}/events/mine/{offset}',
            'api.controllers.events:listAction'
        )->value(
            'offset',
            1
        )->before(
            $app['routes.middleware.validoffsetor404']
        );
        $app->get(
            '/users/{userId}/events/{eventId}',
            'api.controllers.events:showAction'
        );
        $app->delete(
            '/users/{userId}/events/{eventId}',
            'api.controllers.events:removeAction'
        )->before(
            $app['api.auth.authenticationowneronly']
        );
        $app->post(
            '/users/{userId}/events/{eventId}/go',
            'api.controllers.eventsvotes:goAction'
        )->before(
            $app['api.auth.credentialsonly']
        );
        $app->delete(
            '/users/{userId}/events/{eventId}/go',
            'api.controllers.eventsvotes:ungoAction'
        )->before(
            $app['api.auth.credentialsonly']
        );

        /* User activity */
        $app->get(
            '/users/{userId}/activity/events/go/incoming/{offset}',
            'api.controllers.eventsvotes:goIncomingListAction'
        )->value(
            'offset',
            1
        )->before(
            $app['api.auth.authenticationowneronly']
        );
        $app->get(
            '/users/{userId}/activity/events/go/past/{offset}',
            'api.controllers.eventsvotes:goPastListAction'
        )->value(
            'offset',
            1
        );

        /* Event Search */
        $app->get(
            '/events/near/{latitude}/{longitude}/{offset}',
            'api.controllers.eventssearch:nearAction'
        )->before(
            $app['routes.middleware.validlocationor404']
        )->value(
            'offset',
            1
        )->before(
            $app['routes.middleware.validoffsetor404']
        );
        $app->get(
            '/events/recent/{latitude}/{longitude}/{offset}',
            'api.controllers.eventssearch:recentAction'
        )->value(
            'offset',
            1
        )->before(
            $app['routes.middleware.validoffsetor404']
        );
        $app->get(
            '/events/popular/{latitude}/{longitude}/{offset}',
            'api.controllers.eventssearch:popularAction'
        )->value(
            'offset',
            1
        )->before(
            $app['routes.middleware.validoffsetor404']
        );
    }

    /**
     * "boot" method for class interface Silex\ServiceProviderInterface
     *
     * do nothing is't only for interface
     *
     * @param Silex\Application $app silex application container instance
     *
     * @return void
     */
    public function boot(Application $app)
    {
        //get json from request
        $app->before(
            function (Request $request) use ($app) {
                if ($request->headers->has('Content-Type') &&
                    (strpos($request->headers->get('Content-Type'), 'application/json') === false &&
                     strpos($request->headers->get('Content-Type'), 'application/x-www-form-urlencoded') === false &&
                     strpos($request->headers->get('Content-Type'), 'multipart/form-data') === false)
                ) {
                    return $app->abort(
                        406,
                        sprintf(
                            'Content-Type: %s is not allowed for %s',
                            $request->headers->get('Content-Type'),
                            $request->getPathInfo()
                        )
                    );
                }
                if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
                    $data = $request->getContent();
                    if (!is_array($data)) {
                        try {
                            $data = json_decode($request->getContent(), true);
                        } catch (Exception $e) {
                            $data = null;
                        }
                    }
                    $request->request->replace(is_array($data) ? $data : []);
                }
            }
        );

        $app->error(
            function (Exception $e) use ($app) {
                $res = [
                    'code'    => 500,
                    'message' => 'Internal Server Error'
                ];
                $headers = [];

                if ($e instanceof HttpExceptionInterface && $e->getStatusCode() != 500) {
                    try {
                        $res = [
                            'code'    => $e->getStatusCode(),
                            'message' => $e->getMessage()
                        ];
                        if ($e instanceof MyHttpExceptionInterface) {
                            $data = $e->getData() ?: [];
                            $rawData = $e->getRawData() ?: [];
                            $headers = $e->getHeaders();
                            if (!empty($data)) {
                                $res['data'] = $data;
                            } elseif ($res['code'] === 400) {
                                //empty and 400 set an empty object
                                $res['data'] = new StdClass();
                            }
                            if (!empty($rawData) && $res['code'] === 400) {
                                $res['rawData'] = $rawData;
                            }
                        }

                    } catch (Exception $ee) {
                        // TODO: log this error somewhere
                    }
                } else {
                    // TODO: something went wrong, better notify someone
                }

                return $app->json(
                    $res,
                    $res['code'],
                    $headers
                );
            },
            -1
        );
    }
}
