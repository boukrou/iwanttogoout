<?php

namespace Provider\Api;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Controller\Api as Controller;

/**
 * This service provider is used for declared api ation controllers
 *
 * This access keys are added to silex application container "Silex\Application"
 *   - api.controllers.index    with an instance of Controller\Api\IndexController
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class ControllersServiceProvider implements ServiceProviderInterface
{
    /**
     * "register" method for class interface Silex\ServiceProviderInterface
     *
     * Added to containner :
     *   - api.controllers.index    with an instance of Controller\Api\IndexController
     *
     * @return void
     */
    public function register(Application $app)
    {
        $app['api.controllers.index'] = $app->share(
            function () use ($app) {
                return new Controller\IndexController($app);
            }
        );

        $app['api.controllers.users'] = $app->share(
            function () use ($app) {
                return new Controller\UsersController($app);
            }
        );

        $app['api.controllers.events'] = $app->share(
            function () use ($app) {
                return new Controller\EventsController($app);
            }
        );
        $app['api.controllers.eventsvotes'] = $app->share(
            function () use ($app) {
                return new Controller\EventsVotesController($app);
            }
        );
        $app['api.controllers.eventssearch'] = $app->share(
            function () use ($app) {
                return new Controller\EventsSearchController($app);
            }
        );
    }

    /**
     * "boot" method for class interface Silex\ServiceProviderInterface
     *
     * do nothing is't only for interface
     *
     * @param Silex\Application $app silex application container instance
     */
    public function boot(Application $app)
    {
        //do nothing
    }
}
