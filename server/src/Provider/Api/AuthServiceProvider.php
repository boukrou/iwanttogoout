<?php

namespace Provider\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Repository\Api\Auth as RepositoryApiAuth;
use Validator;

/**
 *
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class AuthServiceProvider implements ServiceProviderInterface
{
    use \Repository\SecurityRepositoryTrait;

    /**
     * Checks HTTP Basic authentication
     *
     * The "username:password" authentication credentials that are expected
     * in the base64-encoded Authorization header are:
     *   - username: the UID of the device and its type joined by a slash (device_uid/device_type)
     *   - password: the user password
     *
     * @param Silex\Application $app Silex application
     *
     * @return string User ID (or false if the client is not authenticated)
     */
    protected function checkAuthentication(
        Application $app
    ) {
        $authUserId   = $app['request']->server->get('PHP_AUTH_USER');
        $authPassword = $app['request']->server->get('PHP_AUTH_PW');

        try {
            if (empty($authUserId) || empty($authPassword)) {
                throw new \Exception(
                    sprintf(
                        '%s (%s) - empty authUsername or empty authPassword',
                        __METHOD__,
                        __LINE__
                    )
                );
            }

            $errors = $app['validator']->validateValue(
                $authPassword,
                new Validator\Users\Password()
            );

            if (count($errors) > 0) {
                throw new \Exception(
                    sprintf(
                        '%s (%s) - validation error',
                        __METHOD__,
                        __LINE__
                    )
                );
            }
        } catch (\Exception $e) {
            // TODO : log this
            return false;
        }

        try {
            $user = $app['repositories.users']->find(
                ['where' => [['id' => $authUserId, 'password' => $authPassword]]],
                ['id', 'username']
            )->first();

            if (empty($user)) {
                return false;
            }

            $app['authUser'] = $user;

            return $user->id;
        } catch (\Exception $e) {
            // TODO : log this
            return false;
        }

        return false;
    }

    /**
     * Registration for Yax api workflow
     *
     * "register" method for class interface Silex\ServiceProviderInterface
     *
     * nothing as added to containner
     *
     * @param Silex\Application $app silex application container instance
     *
     * @return void
     */
    public function register(
        Application $app
    ) {
        $app['api.auth.authenticationowneronly'] = $app->protect(
            function (Request $request) use ($app) {
                $currentUserId = $this->checkAuthentication($app);

                $userId = $app['request']->get('userId');

                if (empty($currentUserId) || $currentUserId != $userId) {
                    return $app->abort(
                        401,
                        'User not authenticated',
                        [],
                        ['WWW-Authenticate' => 'Basic realm="Auth API"']
                    );
                }

            }
        );

        $app['api.auth.credentialsonly'] = $app->protect(
            function (Request $request) use ($app) {
                $currentUserId = $this->checkAuthentication($app);

                if (empty($currentUserId)) {
                    return $app->abort(
                        401,
                        'User not authenticated',
                        [],
                        ['WWW-Authenticate' => 'Basic realm="Auth API"']
                    );
                }

            }
        );
    }

    /**
     * "boot" method for class interface Silex\ServiceProviderInterface
     *
     * @param Silex\Application $app silex application container instance
     *
     * @return void
     */
    public function boot(
        Application $app
    ) {

    }
}
