<?php

namespace Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;

use Repository;

/**
 * Repositories Service Provider is a service provider that inject all repositories access
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class RepositoriesServiceProvider implements ServiceProviderInterface
{
    /**
     * "register" method for class interface Silex\ServiceProviderInterface
     *
     * Added to containner :
     *
     * @param Silex\Application $app silex application container instance
     *
     * @return void
     */
    public function register(Application $app)
    {
        $app['repositories.users'] = $app->share(
            function () use ($app) {
                return new Repository\UsersRepository($app, new \Entity\Users());
            }
        );

        $app['repositories.events'] = $app->share(
            function () use ($app) {
                return new Repository\EventsRepository($app, new \Entity\Events());
            }
        );
        $app['repositories.eventsvotes'] = $app->share(
            function () use ($app) {
                return new Repository\EventsVotesRepository($app, new \Entity\EventsVotes());
            }
        );
    }

    /**
     * "boot" method for class interface Silex\ServiceProviderInterface
     *
     * do nothing is't only for interface
     *
     * @param Silex\Application $app silex application container instance
     */
    public function boot(Application $app)
    {
        //do nothing
    }
}
