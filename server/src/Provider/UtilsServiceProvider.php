<?php

namespace Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;

use Utils;

/**
 * Repositories Service Provider is a service provider that inject all repositories access
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class UtilsServiceProvider implements ServiceProviderInterface
{
    /**
     * "register" method for class interface Silex\ServiceProviderInterface
     *
     * Added to containner :
     *
     * @param Silex\Application $app silex application container instance
     *
     * @return void
     */
    public function register(Application $app)
    {
        $app['utils.images'] = $app->share(
            function () use ($app) {
               return new Utils\ImagesUtils($app);
            }
        );

        $app['utils.formErrorsSerializer'] = $app->share(
            function () use ($app) {
               return new Utils\FormErrorsSerializerUtils($app);
            }
        );
    }

    /**
     * "boot" method for class interface Silex\ServiceProviderInterface
     *
     * do nothing is't only for interface
     *
     * @param Silex\Application $app silex application container instance
     */
    public function boot(Application $app)
    {
        //do nothing
    }
}
