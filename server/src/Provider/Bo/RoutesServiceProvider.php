<?php

namespace Provider\Bo;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

use HttpKernel\Exception\HttpExceptionInterface as MyHttpExceptionInterface;

/**
 * This service provider is used for declared bo routing rules
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class RoutesServiceProvider implements ServiceProviderInterface
{
    /**
     * Registration for bo workflow
     *
     * "register" method for class interface Silex\ServiceProviderInterface
     *
     * nothing as added to containner
     *
     * @param Silex\Application $app silex application container instance
     *
     * @return void
     */
    public function register(Application $app)
    {
        //Allow fake PUT, DELETE, PATCH with hidden _method in form
        Request::enableHttpMethodParameterOverride();

        //ACL middleware
        $app['routes.middleware.acl'] = $app->protect(
            function ($checkAcl = \Validator\Users\BoAclTypeChoice::MODO) use ($app) {
                return function (Request $request) use ($app, $checkAcl) {
                    if (empty($checkAcl)) {
                        return;
                    }

                    $acl = $app['session']->get('user')->acl;
                    if ((int) $acl > (int) $checkAcl) {
                        return $app->abort(
                            403,
                            'Not allowed'
                        );
                    }
                };
            }
        );

        // authentication
        $app->get(
            '/login',
            'bo.controllers.auth:loginAction'
        )->bind(
            'bo_login'
        );
        $app->post(
            '/login',
            'bo.controllers.auth:loginAction'
        )->bind(
            'bo_login_post'
        );

        $app->get(
            '/logout',
            'bo.controllers.auth:logoutAction'
        )->bind(
            'bo_logout'
        );

        // dashboard
        $app->get(
            '/',
            'bo.controllers.dashboard:indexAction'
        )->bind(
            'bo_dashboard_index'
        );

        $crudNames = [
            'users',
            'events'
        ];

        foreach ($crudNames as $crudName) {
            $app->get(
                '/' . $crudName,
                'bo.controllers.' . $crudName . ':listAction'
            )->bind(
                'bo_'.$crudName . '_list'
            )->before(
                $app['routes.middleware.acl'](\Validator\Users\BoAclTypeChoice::ADMIN)
            );
            $app->put(
                '/' . $crudName,
                'bo.controllers.' . $crudName . ':createAction'
            )->bind(
                'bo_'.$crudName . '_create'
            )->before(
                $app['routes.middleware.acl'](\Validator\Users\BoAclTypeChoice::ADMIN)
            );
            $app->get(
                '/' . $crudName . '/new',
                'bo.controllers.' . $crudName . ':createAction'
            )->bind(
                'bo_'.$crudName . '_new'
            )->before(
                $app['routes.middleware.acl'](\Validator\Users\BoAclTypeChoice::ADMIN)
            );
            $app->get(
                '/' . $crudName . '/{resourceId}',
                'bo.controllers.' . $crudName . ':getAction'
            )->bind(
                'bo_'.$crudName . '_get'
            )->before(
                $app['routes.middleware.acl'](\Validator\Users\BoAclTypeChoice::ADMIN)
            );
            $app->post(
                '/' . $crudName . '/{resourceId}',
                'bo.controllers.' . $crudName . ':updateAction'
            )->bind(
                'bo_'.$crudName . '_update'
            )->before(
                $app['routes.middleware.acl'](\Validator\Users\BoAclTypeChoice::ADMIN)
            );
            $app->post(
                '/' . $crudName . '/{resourceId}/status',
                'bo.controllers.' . $crudName . ':changeStatusAction'
            )->bind(
                'bo_'.$crudName . '_update_status'
            )->before(
                $app['routes.middleware.acl'](\Validator\Users\BoAclTypeChoice::ADMIN)
            );
            $app->post(
                '/' . $crudName . '/{resourceId}/acl',
                'bo.controllers.' . $crudName . ':changeAclAction'
            )->bind(
                'bo_'.$crudName . '_update_acl'
            )->before(
                $app['routes.middleware.acl'](\Validator\Users\BoAclTypeChoice::ADMIN)
            );
            $app->get(
                '/' . $crudName . '/{resourceId}/edit',
                'bo.controllers.' . $crudName . ':updateAction'
            )->bind(
                'bo_'.$crudName . '_edit'
            )->before(
                $app['routes.middleware.acl'](\Validator\Users\BoAclTypeChoice::ADMIN)
            );
            $app->delete(
                '/' . $crudName . '/{resourceId}',
                'bo.controllers.' . $crudName . ':deleteAction'
            )->bind(
                'bo_'.$crudName . '_delete'
            )->before(
                $app['routes.middleware.acl'](\Validator\Users\BoAclTypeChoice::ADMIN)
            );
        }
    }

    /**
     * "boot" method for class interface Silex\ServiceProviderInterface
     *
     * do nothing is't only for interface
     *
     * @param Silex\Application $app silex application container instance
     *
     * @return void
     */
    public function boot(Application $app)
    {
        $app->before(
            function (Request $request) use ($app) {
                /* TODO : redirection after login
                if (!$request->isSecure()) {
                    return $app->redirect(
                        $app->url('bo_login')
                    );
                }
                */

                if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
                    $data = json_decode($request->getContent(), true);
                    $request->request->replace(is_array($data) ? $data : array());
                }

                // check authemtification
                return $app['bo.controllers.auth']->isAuthenticated($request);
            }
        );

        $app->error(
            function (\Exception $e) use ($app) {

                $res = [
                    'code'    => 500,
                    'message' => 'Internal Server Error'
                ];
                if ($e instanceof HttpExceptionInterface) {
                    try {
                        $res = [
                            'code'    => $e->getStatusCode(),
                            'message' => $e->getMessage()
                        ];
                        if ($e instanceof MyHttpExceptionInterface) {
                            $data = $e->getData() ?: [];
                            $rawData = $e->getRawData() ?: [];

                            if (!empty($data)) {
                                $res['data'] = $data;
                            } elseif ($res['code'] === 400) {
                                //empty and 400 set an empty object
                                $res['data'] = new StdClass();
                            } elseif ($res['code'] > 400 && $res['code'] < 500) {
                                $res = [
                                    'code'    => 404,
                                    'message' => 'Page Not Found'
                                ];
                            }

                            if (!empty($rawData) && $res['code'] === 400) {
                                $res['rawData'] = $rawData;
                            }
                        } else {
                            if ($res['code'] > 400 && $res['code'] < 500) {
                                $res = [
                                    'code'    => 404,
                                    'message' => 'Page Not Found'
                                ];
                            }
                        }
                    } catch (\Exception $ee) {
                        // TODO: log this error somewhere
                    }
                } else {
                    $res['exception'] = $e;
                }

                $res['is_error_page'] = true;

                try {
                    return $app->render(
                        'common/errors.twig',
                        $res
                    );
                } catch (\Exception $e) {
                    return new Response(
                        '<html><h1>Une erreur est survenue.</h1><html>',
                        500
                    );
                }
            },
            -1
        );
    }
}
