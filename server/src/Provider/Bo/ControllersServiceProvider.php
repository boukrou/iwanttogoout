<?php

namespace Provider\Bo;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Controller\Bo as Controller;

/**
 * This service provider is used for declared bo ation controllers
 *
 * This access keys are added to silex application container "Silex\Application"
 *   - bo.controllers.index    with an instance of Controller\Bo\IndexController
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class ControllersServiceProvider implements ServiceProviderInterface
{
    /**
     * "register" method for class interface Silex\ServiceProviderInterface
     *
     * Added to containner :
     *   - bo.controllers.index    with an instance of Controller\Bo\IndexController
     *
     * @return void
     */
    public function register(Application $app)
    {
        $app['bo.controllers.auth'] = $app->share(
            function () use ($app) {
                return new Controller\AuthController($app);
            }
        );
        $app['bo.controllers.dashboard'] = $app->share(
            function () use ($app) {
                return new Controller\DashboardController($app);
            }
        );
        $app['bo.controllers.users'] = $app->share(
            function () use ($app) {
                return new Controller\UsersController($app);
            }
        );
        $app['bo.controllers.events'] = $app->share(
            function () use ($app) {
                return new Controller\EventsController($app);
            }
        );
    }

    /**
     * "boot" method for class interface Silex\ServiceProviderInterface
     *
     * do nothing is't only for interface
     *
     * @param Silex\Application $app silex application container instance
     */
    public function boot(Application $app)
    {
        //do nothing
    }
}
