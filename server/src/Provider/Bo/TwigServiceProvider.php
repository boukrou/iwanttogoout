<?php

namespace Provider\Bo;

use DateTime;
use Twig_SimpleFilter;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Silex\Provider\TwigServiceProvider as SilexTwigServiceProvider;

class TwigServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app->register(
            new SilexTwigServiceProvider(),
            ['twig.path' => realpath(__DIR__.'/../../../views/bo'),
             'twig.form.templates' => [
                'common/form_div_layout.twig'
            ]]
        );

        $app['twig'] = $app->share(
            $app->extend(
                'twig',
                function ($twig, $app) {
                    $twig->addGlobal(
                        'ACL_LEVEL_ADMIN',
                        \Validator\Users\BoAclTypeChoice::ADMIN
                    );
                    $twig->addGlobal(
                        'ACL_LEVEL_EDITO',
                        \Validator\Users\BoAclTypeChoice::EDITO
                    );
                    $twig->addGlobal(
                        'ACL_LEVEL_MODO',
                        \Validator\Users\BoAclTypeChoice::MODO
                    );

                    return $twig;
                }
            )
        );
    }

    /**
     * "boot" method for class interface Silex\ServiceProviderInterface
     *
     * do nothing is't only for interface
     *
     * @param Silex\Application $app silex application container instance
     */
    public function boot(Application $app)
    {
        //do nothing
    }
}
