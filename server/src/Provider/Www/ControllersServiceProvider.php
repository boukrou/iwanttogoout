<?php

namespace Provider\Www;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Controller\Www as Controller;

/**
 * This service provider is used for declared public ation controllers
 *
 * This access keys are added to silex application container "Silex\Application"
 *   - public.controllers.index    with an instance of Controller\Www\IndexController
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class ControllersServiceProvider implements ServiceProviderInterface
{
    /**
     * "register" method for class interface Silex\ServiceProviderInterface
     *
     * Added to containner :
     *   - public.controllers.index    with an instance of Controller\Www\IndexController
     *
     * @return void
     */
    public function register(Application $app)
    {
        $app['www.controllers.home'] = $app->share(
            function () use ($app) {
                return new Controller\HomeController($app);
            }
        );

        $app['www.controllers.events'] = $app->share(
            function () use ($app) {
                return new Controller\EventsController($app);
            }
        );

        $app['www.controllers.eventsvotes'] = $app->share(
            function () use ($app) {
                return new Controller\EventsVotesController($app);
            }
        );

        $app['www.controllers.eventssearch'] = $app->share(
            function () use ($app) {
                return new Controller\EventsSearchController($app);
            }
        );

        $app['www.controllers.users'] = $app->share(
            function () use ($app) {
                return new Controller\UsersController($app);
            }
        );

        $app['www.controllers.auth'] = $app->share(
            function () use ($app) {
                return new Controller\AuthController($app);
            }
        );

        $app['www.controllers.pages'] = $app->share(
            function () use ($app) {
                return new Controller\PagesController($app);
            }
        );
    }

    /**
     * "boot" method for class interface Silex\ServiceProviderInterface
     *
     * do nothing is't only for interface
     *
     * @param Silex\Application $app silex application container instance
     */
    public function boot(Application $app)
    {
        //do nothing
    }
}
