<?php

namespace Provider\Www;

use DateTime;
use Twig_SimpleFilter;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Silex\Provider\TwigServiceProvider as SilexTwigServiceProvider;

class TwigServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app->register(
            new SilexTwigServiceProvider(),
            array(
                'twig.path' => realpath(__DIR__.'/../../../views/www'),
                'twig.form.templates' => [
                    'common/form_div_layout.twig'
                ]
            )
        );

        $app['twig'] = $app->share(
            $app->extend(
                'twig',
                function ($twig, $app) {

                    $twig->addExtension(new \Twig_Extensions_Extension_Intl());

                    $twig->addFunction(new \Twig_SimpleFunction(
                        'images_service', function($type, $format, $file) use ($app) {
                            return $app['images_service'].'/'.$type.'/'.$format.'/'.$file;
                        }
                    ));

                    return $twig;
                }
            )
        );
    }

    /**
     * "boot" method for class interface Silex\ServiceProviderInterface
     *
     * do nothing is't only for interface
     *
     * @param Silex\Application $app silex application container instance
     */
    public function boot(Application $app)
    {
        //do nothing
    }
}
