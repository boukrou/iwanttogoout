<?php

namespace Provider\Www;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

use HttpKernel\Exception\HttpExceptionInterface as MyHttpExceptionInterface;
/**
 * This service provider is used for declared public routing rules
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class RoutesServiceProvider implements ServiceProviderInterface
{
    /**
     * Registration for public workflow
     *
     * "register" method for class interface Silex\ServiceProviderInterface
     *
     * nothing as added to containner
     *
     * @param Silex\Application $app silex application container instance
     *
     * @return void
     */
    public function register(Application $app)
    {
        $app['routes.middleware.validoffsetor404'] = $app->protect(
            function (Request $request) use ($app) {
                $offset = $app['request']->get('offset');
                $isInt = (is_string($offset))
                    ? ctype_digit($offset)
                    : is_int($offset);
                if (!$isInt) {
                    if ($app['debug']) {
                        return $app->abort(
                            400,
                            'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo(),
                            ['[offset] ' . $offset . ' invalid ']
                        );
                    }
                    return $app->abort(
                        404,
                        'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo()
                    );
                }
            }
        );

        $app['routes.middleware.credentialsonly'] = $app->protect(
            function (Request $request) use ($app) {
                if (empty($app['session']->get('user')) || empty($app['session']->get('user')['id'])) {
                    return $app->abort(
                        401,
                        'Create account'
                    );
                }

            }
        );

        $app['routes.middleware.authenticationowneronly'] = $app->protect(
            function (Request $request) use ($app) {
                if (empty($app['session']->get('user')) || empty($app['session']->get('user')['id']) || $app['request']->get('userId') != $app['session']->get('user')['id']) {
                    return $app->abort(
                        403,
                        'Forbidden'
                    );
                }

            }
        );


        $app['routes.middleware.valideventor404'] = $app->protect(
            function (Request $request) use ($app) {
                $event = $app['repositories.events']->findById(
                    $app['request']->get('eventId'),
                    ['id', 'status']
                );

                $invalidStatuses = [
                    \Validator\Events\StatusChoice::MODERATED,
                    \Validator\Events\StatusChoice::DELETED,
                    \Validator\Events\StatusChoice::DELETED_PENDING
                ];

                if (empty($event->status) || in_array($event->status, $invalidStatuses)) {
                    return $app->abort(
                        404,
                        'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo()
                    );
                }
            }
        );

        $app['routes.middleware.validlocationor404'] = $app->protect(
            function (Request $request) use ($app) {
                $location = [
                    'latitude'  => $app['request']->get('latitude'),
                    'longitude' => $app['request']->get('longitude')
                ];
                $errors = $app['validator']->validateValue(
                    $location,
                    new \Validator\Location()
                );
                if (count($errors) > 0) {
                    if ($app['debug']) {
                        $data = [];
                        foreach ($errors as $error) {
                            $data[] = $error->getPropertyPath() . ' ' . $error->getMessage();
                        }
                        return $app->abort(
                            400,
                            'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo(),
                            $data
                        );
                    }
                    return $app->abort(
                        404,
                        'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo()
                    );
                }
            }
        );

        $app->get(
            '/',
            'www.controllers.home:indexAction'
        )->bind(
            'www_home'
        );

        /* Events */
        $app->get(
            '/events',
            'www.controllers.events:indexAction'
        )->bind(
            'www_events_index'
        );
        $app->get(
            '/events/add',
            'www.controllers.events:createAction'
        )->bind(
            'www_events_create'
        )->before(
            $app['routes.middleware.credentialsonly']
        );
        $app->post(
            '/events/add',
            'www.controllers.events:createAction'
        )->bind(
            'www_events_create_post'
        )->before(
            $app['routes.middleware.credentialsonly']
        );

        /* Events votes */
        $app->post(
            '/events/{eventId}/go',
            'www.controllers.eventsvotes:goAction'
        )->bind(
            'www_events_votes_go'
        )->before(
            $app['routes.middleware.valideventor404']
        )->before(
            $app['routes.middleware.credentialsonly']
        );
        $app->post(
            '/events/{eventId}/ungo',
            'www.controllers.eventsvotes:ungoAction'
        )->bind(
            'www_events_votes_ungo'
        )->before(
            $app['routes.middleware.valideventor404']
        )->before(
            $app['routes.middleware.credentialsonly']
        );

        /* Profiles */
        $app->get(
            '/users/{userId}',
            'www.controllers.users:showAction'
        )->bind(
            'www_users_show'
        );
        $app->get(
            '/users/{userId}/events/{eventId}/edit',
            'www.controllers.events:editAction'
        )->bind(
            'www_events_edit'
        )->before(
            $app['routes.middleware.authenticationowneronly']
        );
        $app->post(
            '/users/{userId}/events/{eventId}/edit',
            'www.controllers.events:editAction'
        )->bind(
            'www_events_edit_post'
        )->before(
            $app['routes.middleware.authenticationowneronly']
        );

        /* Pages */
        $app->get(
            '/contact',
            'www.controllers.pages:contactAction'
        )->bind(
            'www_contact'
        );
        $app->post(
            '/contact',
            'www.controllers.pages:contactAction'
        )->bind(
            'www_contact_post'
        );
        $app->get(
            '/about-us',
            'www.controllers.pages:aboutUsAction'
        )->bind(
            'www_aboutus'
        );
        $app->get(
            '/terms',
            'www.controllers.pages:termsAction'
        )->bind(
            'www_terms'
        );
        $app->get(
            '/privacy',
            'www.controllers.pages:privacyAction'
        )->bind(
            'www_privacy'
        );

        /* Auth */
        $app->get(
            '/signin',
            'www.controllers.auth:signinAction'
        )->bind(
            'www_auth_signin'
        );
        $app->post(
            '/signin',
            'www.controllers.auth:signinAction'
        )->bind(
            'www_auth_signin_post'
        );
        $app->get(
            '/signup',
            'www.controllers.auth:signupAction'
        )->bind(
            'www_auth_signup'
        );
        $app->post(
            '/signup',
            'www.controllers.auth:signupAction'
        )->bind(
            'www_auth_signup_post'
        );
        $app->get(
            '/logout',
            'www.controllers.auth:logoutAction'
        )->bind(
            'www_auth_logout'
        );

        /* Event Search */
        $app->get(
            '/events/near/{latitude}/{longitude}/{offset}',
            'www.controllers.eventssearch:nearAction'
        )->before(
            $app['routes.middleware.validlocationor404']
        )->value(
            'offset',
            1
        )->before(
            $app['routes.middleware.validoffsetor404']
        );
        $app->get(
            '/events/recent/{latitude}/{longitude}/{offset}',
            'www.controllers.eventssearch:recentAction'
        )->value(
            'offset',
            1
        )->before(
            $app['routes.middleware.validoffsetor404']
        );
        $app->get(
            '/events/popular/{latitude}/{longitude}/{offset}',
            'www.controllers.eventssearch:popularAction'
        )->value(
            'offset',
            1
        )->before(
            $app['routes.middleware.validoffsetor404']
        );
    }

    /**
     * "boot" method for class interface Silex\ServiceProviderInterface
     *
     * @param Silex\Application $app silex application container instance
     *
     * @return void
     */
    public function boot(Application $app)
    {
        $app->before(
            function (Request $request) use ($app) {
                $lang = $default_locale = $app['www.default_locale'];

                $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

                if (!empty($app['session']->get('lang'))) {
                    $lang = $app['session']->get('lang');
                }

                if (!empty($request->get('l'))) {
                    $lang = $request->get('l');
                    $request->query->remove('l');
                }

                if (!in_array($lang, $app['www.supported_locales'])) {
                    $lang = $default_locale;
                }

                $app['session']->set('lang', $lang);

                \Locale::setDefault($lang);

                $app['translator']->setLocale($lang);
            }
        );

        $app->error(
            function (\Exception $e) use ($app) {
                $res = [
                    'code'    => 500,
                    'message' => 'Internal Server Error'
                ];
                if ($e instanceof HttpExceptionInterface) {
                    try {
                        $res = [
                            'code'    => $e->getStatusCode(),
                            'message' => $e->getMessage()
                        ];
                        if ($e instanceof MyHttpExceptionInterface) {
                            $data = $e->getData() ?: [];

                            if (!empty($data)) {
                                $res['data'] = $data;
                            } elseif ($res['code'] === 400) {
                                //empty and 400 set an empty object
                                $res['data'] = new StdClass();
                            } elseif ($res['code'] > 400 && $res['code'] < 500) {
                                $res = [
                                    'code'    => 404,
                                    'message' => 'Page Not Found'
                                ];
                            }
                        } else {
                            if ($res['code'] > 400 && $res['code'] < 500) {
                                $res = [
                                    'code'    => 404,
                                    'message' => 'Page Not Found'
                                ];
                            }
                        }
                    } catch (\Exception $ee) {
                        // TODO: log this error somewhere
                    }
                } else {
                    $res['exception'] = $e;
                }

                $res['is_error_page'] = true;

                try {
                    return $app->render(
                        'common/errors.twig',
                        $res
                    );
                } catch (\Exception $e) {
                    return new Response(
                        '<html><h1>Une erreur est survenue.</h1><html>',
                        500
                    );
                }
            },
            -1
        );
    }
}
