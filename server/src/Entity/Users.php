<?php

namespace Entity;

class Users extends \Illuminate\Database\Eloquent\Model 
{
    protected $table = "users";

    protected $fillable = [
        'username',
        'email',
        'password',
        'salt',
        'team_id',
        'status',
        'description',
        'picture',
        'activity_at',
        'acl'
    ];

    public function getDates()
    {
        return [
            'activity_at',
        ];
    }
}
