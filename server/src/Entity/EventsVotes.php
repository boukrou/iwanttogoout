<?php

namespace Entity;

use \Illuminate\Database\Query\Expression as Expression;

class EventsVotes extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "events_votes";

    protected $fillable = [
        'event_id',
        'user_id',
        'vote_type',
        'status',
    ];

    /**
     * Get the related user
     */
    public function user()
    {
        return $this->belongsTo('\Entity\Users', 'user_id');
    }

    /**
     * Get the related event
     */
    public function event()
    {
        return $this->belongsTo('\Entity\Events', 'event_id');
    }
}
