<?php

namespace Entity;

use \Illuminate\Database\Query\Expression as Expression;

class Events extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "events";

    protected $fillable = [
        'title',
        'user_id',
        'description',
        'start_at',
        'end_at',
        'event_type',
        'team_id',
        'location',
        'location_data',
        'is_sponsored',
        'status',
    ];

    public function setLocationAttribute($value)
    {
        $this->attributes['location'] = new Expression('POINT(' . $value . ')');
    }

    public function getLocationAttribute($value)
    {
        $loc = substr($value, 6);
        $loc = preg_replace('/[ ,]+/', ',', $loc, 1);
        $loc = substr($loc, 0, -1);

        if (!empty($loc)) {
            $loc = explode(',', $loc);

            if (!empty($loc[0]) && !empty($loc[1])) {
                return [
                    'latitude'  => $loc[0],
                    'longitude' => $loc[1]
                ];
            }
        }

        return [];
    }

    /**
     * Custom date fields
     */
    public function getDates()
    {
        return [
            'start_at',
            'end_at',
        ];
    }

    public function getStartAtAttribute($date)
    {
        return !empty($date) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('U') : $date;
    }

    public function getEndAtAttribute($date)
    {
        return !empty($date) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('U') : $date;
    }


    public function getIsSponsoredAttribute($isSponsored)
    {
        return !empty($isSponsored);
    }

    public function getLocationDataAttribute($json)
    {
        return json_decode($json);
    }

    /**
     * Get the user of an event.
     */
    public function user()
    {
        return $this->belongsTo('\Entity\Users', 'user_id');
    }

    /**
     * Get the user of an event.
     */
    public function eventsvotes()
    {
        return $this->hasMany('\Entity\EventsVotes', 'event_id');
    }
}
