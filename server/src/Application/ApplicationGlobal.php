<?php

namespace Application;

use UnexpectedValueException;
use Igorw\Silex\ConfigServiceProvider;
use Silex\Application;
use Silex\Provider\ValidatorServiceProvider;

use Provider;

/**
 * Main Application Class
 * This clas must be extended by a specific application ex: Application\Api
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class ApplicationGlobal extends Application
{
    /**
     * @const string ENV_NAME environement variable name
     */
    const ENV_NAME = 'IWTGO_ENV';

    /**
     * @const string DEFAULT_ENV default environement
     */
    const DEFAULT_ENV = 'development';

    /**
     * @const string CONFIG_FILE_PATTERN the configuration pattern for retreive configuration file
     */
    const CONFIG_FILE_PATTERN = '/../../config/%s/config.yml';

    protected $extendedValues = array();

    /**
     * Constructor
     *
     * @param array $values Injections values
     */
    public function __construct(array $values = array())
    {
        if (!empty($values)) {
            $this->extendedValues = $values;
        }
        parent::__construct($values);

        //on set l'environement dans le container
        if (!isset($this['env'])) {
            $this['env'] = getenv(self::ENV_NAME) ?: self::DEFAULT_ENV;
            unset($this->extendedValues['env']);
        }

        $this['base_path'] = realpath(__DIR__ . '/../../');

        $this['time']      = time();
        $this['microtime'] = microtime(true);

        //on enregistre les services providers génériques
        $this
            ->registerConfiguration()
            ->registerBackendsDrivers()
            ->registerRepositories();

        if ($this['debug']) {
            error_reporting(E_ALL);
            ini_set("display_errors", 1);
        }
    }

    /**
     * Register configuration loader cf. https://github.com/igorw/ConfigServiceProvider
     * and load configuration file with it the configuration file loaded deends of IWTGO_ENV environement variable
     *
     * @return $this for method chaining
     **/
    protected function registerConfiguration()
    {
        $configFileName = __DIR__ . sprintf(self::CONFIG_FILE_PATTERN, $this['env']);
        $this['config.file'] = realpath($configFileName);
        if (empty($this['config.file']) || !is_readable($this['config.file'])) {
            throw new UnexpectedValueException('Config File "' . $configFileName . '" not found or not readable');
        }

        $this['config.path'] = dirname($this['config.file']);
        $configParams =  array(
            'env'         => $this['env']         ,
            'base_path'   => $this['base_path']   ,
            'config_path' => $this['config.path'] ,
            'config_file' => $this['config.file'] ,
        );

        $this->register(
            new ConfigServiceProvider(
                $this['config.file'],
                $configParams
            )
        );
        //exclude config.file and config.path
        $configParams = array_merge(
            $configParams,
            array(
                'config.file' => $this['config.file'],
                'config.path' => $this['config.path'],
            )
        );

        foreach ($this->extendedValues as $key => $value) {
            if (empty($configParams[$key])) {
                $this[$key] = $value;
            }
        }

        return $this;
    }

    /**
     * Register all backends drivers
     *
     * @return $this for method chaining
     **/
    protected function registerBackendsDrivers()
    {
        $mysqlDefaults = [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'iwtgodev',
            'username'  => 'iwtgodev',
            'password'  => 'iwtgodev',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'logging'   => true,
        ];

        $this->register(
            new \BitolaCo\Silex\CapsuleServiceProvider(), [
                'capsule.connection' => array_merge($mysqlDefaults, $this['mysql'])
            ]
        );

        return $this;
    }

    /**
     * Register repositories for DAO (data access object)
     *
     * @return $this for method chaining
     **/
    public function registerRepositories()
    {
        $this->register(
            new ValidatorServiceProvider()
        );
        $this->register(
            new Provider\RepositoriesServiceProvider()
        );
        $this->register(
            new Provider\UtilsServiceProvider()
        );

        return $this;
    }

}
