<?php

namespace Application;

use Silex\Application as SilexApplication;
use Silex\Provider as SilexProvider;

/**
 * Http Application Class for http application api, bo
 * This an extension of GlobalApplication a generic silex application
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class ApplicationHttp extends ApplicationGlobal
{
    use SilexApplication\UrlGeneratorTrait;

    /**
     * Constructor
     *
     * @param array $values Injections values
     */
    public function __construct(array $values = array())
    {
        parent::__construct($values);

        $this->register(new SilexProvider\ServiceControllerServiceProvider());
        $this->register(new SilexProvider\UrlGeneratorServiceProvider());
    }
}
