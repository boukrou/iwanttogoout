<?php

namespace Application;

use Silex\Application as SilexApplication;
use Silex\Provider as SilexProvider;
use Provider\Api as ApiProvider;

use HttpKernel\Exception as HttpKernelException;

/**
 * Application Class for www
 * This an extension of silex application
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class Api extends ApplicationHttp
{
    use SilexApplication\UrlGeneratorTrait;

    /**
     * Constructor
     *
     * @param array $values Injections values
     */
    public function __construct(array $values = array())
    {
        parent::__construct($values);

        $this->register(new SilexProvider\UrlGeneratorServiceProvider());
        $this->register(new ApiProvider\AuthServiceProvider());
        $this->register(new ApiProvider\ControllersServiceProvider());
        $this->register(new ApiProvider\RoutesServiceProvider());
    }

    /**
     * Aborts the current request by sending a proper HTTP error.
     *
     * @param integer $statusCode The HTTP status code
     * @param string  $message    The status message
     * @param array   $headers    An array of HTTP headers
     */
    public function abort($statusCode, $message = '', array $data = [], array $headers = [])
    {
        $rawData = [];
        if ($statusCode === 400) {
            $rawData = $this['request']->request->all();
        }

        throw new HttpKernelException\HttpException(
            $statusCode,
            $message,
            $data,
            $rawData,
            null,
            $headers
        );
    }
}