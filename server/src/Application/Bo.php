<?php

namespace Application;

use Silex\Application as SilexApplication;
use Silex\Provider as SilexProvider;
use Provider\Bo as BoProvider;

use HttpKernel\Exception as HttpKernelException;

/**
 * Application Class for www
 * This an extension of silex application
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class Bo extends ApplicationHttp
{
    use SilexApplication\FormTrait, SilexApplication\TwigTrait;

    /**
     * Constructor
     *
     * @param array $values Injections values
     */
    public function __construct(array $values = array())
    {
        parent::__construct($values);

        $this->register(
            new SilexProvider\SessionServiceProvider(),
            [
                'session.storage.handler' => null,
                'session.storage.options' => [
                    'gc_maxlifetime' => $this['bo.session.gc_maxlifetime']
                ]
            ]
        );

        $this->register(
            new SilexProvider\TranslationServiceProvider(),
            ['locale_fallbacks' => ['en']]
        );

        $this->register(new SilexProvider\FormServiceProvider());
        $this->register(new BoProvider\TwigServiceProvider());
        $this->register(new BoProvider\ControllersServiceProvider());
        $this->register(new BoProvider\RoutesServiceProvider());
    }
}