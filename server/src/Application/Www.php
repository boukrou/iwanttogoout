<?php

namespace Application;

use Silex\Application as SilexApplication;
use Silex\Provider as SilexProvider;
use Provider\Www as WwwProvider;

use HttpKernel\Exception as HttpKernelException;

/**
 * Application Class for www
 * This an extension of silex application
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class Www extends ApplicationHttp
{
    use SilexApplication\FormTrait, SilexApplication\TwigTrait, SilexApplication\SwiftmailerTrait;

    /**
     * Constructor
     *
     * @param array $values Injections values
     */
    public function __construct(array $values = array())
    {
        parent::__construct($values);

        $this->register(
            new SilexProvider\SessionServiceProvider(),
            [
                'session.storage.handler' => null,
                'session.storage.options' => [
                    'gc_maxlifetime' => $this['www.session.gc_maxlifetime']
                ]
            ]
        );

        $this->register(
            new SilexProvider\TranslationServiceProvider(),
            ['locale_fallbacks' => ['en']]
        );

        $this->extend('translator', function($translator, $app) {
            $translator->addLoader('yaml', new \Symfony\Component\Translation\Loader\YamlFileLoader());

            $translator->addResource('yaml', realpath(__DIR__.'/../../resources/locales').'/en.yml', 'en');
            $translator->addResource('yaml', realpath(__DIR__.'/../../resources/locales').'/fr.yml', 'fr');

            return $translator;
        });

        $this->register(new SilexProvider\SwiftmailerServiceProvider(), [
            'swiftmailer.options' => [
                'host'       => $this['mail_host'],
                'username'   => $this['mail_username'],
                'password'   => $this['mail_password'],
                'port'       => $this['mail_port'],
                'encryption' => $this['mail_encryption'],
                'auth_mode'  => $this['mail_auth_mode']
            ]
        ]);

        $this->register(new SilexProvider\FormServiceProvider());
        $this->register(new WwwProvider\TwigServiceProvider());
        $this->register(new WwwProvider\ControllersServiceProvider());
        $this->register(new WwwProvider\RoutesServiceProvider());
    }
}