<?php

namespace Controller\Www;

use Symfony\Component\HttpFoundation\Request;

use Controller\ControllerActionAbstract;
use Validator;

/**
 * EventsVotes action controller for votes management
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class EventsVotesController extends ControllerActionAbstract
{
    /**
     * Returns .....
     *
     * POST /users/{userId}/events/{eventId}/go
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic API information
     */
    public function goAction(
        Request $request,
        $eventId
    ) {
        list($responseData, $statusCode) = $this->performVoteToJson(
            $request,
            ['vote_type' => Validator\EventsVotes\EventsVotesTypeChoice::GO,
             'user_id'   => $this->app['session']->get('user')['id'],
             'event_id'  => $eventId]
        );

        $votesTotal = $this->app['repositories.eventsvotes']->find(
            ['where' => [[
                'event_id'  => $eventId,
                'vote_type' => Validator\EventsVotes\EventsVotesTypeChoice::GO,
                'status'    => Validator\EventsVotes\StatusChoice::VALID
            ]]],
            [
                new \Illuminate\Database\Query\Expression('COUNT(events_votes.id) as votes_total')
            ]
        )->first()->toArray();

        return $this->app->json(
            [
                'state' => $statusCode == 200 ? 'success' : 'error',
                'data'  => $votesTotal
            ],
            $statusCode
        );
    }

    /**
     * Returns .....
     *
     * DELETE /users/{userId}/events/{eventId}/go
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic API information
     */
    public function ungoAction(
        Request $request,
        $eventId
    ) {
        list($responseData, $statusCode) = $this->performRemoveVoteToJson(
            $request,
            ['vote_type' => Validator\EventsVotes\EventsVotesTypeChoice::GO,
             'user_id'   => $this->app['session']->get('user')['id'],
             'event_id'  => $eventId]
        );

        $votesTotal = $this->app['repositories.eventsvotes']->find(
            ['where' => [[
                'event_id'  => $eventId,
                'vote_type' => Validator\EventsVotes\EventsVotesTypeChoice::GO,
                'status'    => Validator\EventsVotes\StatusChoice::VALID
            ]]],
            [
                new \Illuminate\Database\Query\Expression('COUNT(events_votes.id) as votes_total')
            ]
        )->first()->toArray();

        return $this->app->json(
            [
                'state' => $statusCode == 200 ? 'success' : 'error',
                'data'  => $votesTotal
            ],
            $statusCode
        );
    }

    protected function performVoteToJson(
        Request $request,
        $voteParams
    ) {
        return $this->execOrAbort(
            $request,
            function ($params) use ($request, $voteParams) {
                $vote = $this->app['repositories.eventsvotes']->create(
                    $voteParams
                );

                return [
                    !empty($vote),
                    !empty($vote) ? 200 : 403
                ];
            }
        );
    }

    protected function performRemoveVoteToJson(
        Request $request,
        $voteParams
    ) {
        return $this->execOrAbort(
            $request,
            function ($params) use ($request, $voteParams) {
                $vote = $this->app['repositories.eventsvotes']->remove(
                    $voteParams
                );

                return [
                    !empty($vote),
                    !empty($vote) ? 200 : 403
                ];

            }
        );
    }
}
