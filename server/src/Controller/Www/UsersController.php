<?php

namespace Controller\Www;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Controller\ControllerActionAbstract;

/**
 * HomeController main action controller
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class UsersController extends ControllerActionAbstract
{
    public static $PUBLIC_FIELDS = [
        'id',
        'username',
        'team_id',
        'email',
        'picture',
        'description',
    ];

    protected static $RELATIONSHIP_PUBLIC_FIELDS = [
        'user' => [
            'id',
            'team_id',
            'username',
            'description',
        ],
        'event' => [
            'id',
            'title',
            'user_id',
            'description',
            'location_data',
            'start_at',
            'end_at',
            'event_type',
            'team_id',
        ]
    ];
    protected static $RELATIONSHIP_PUBLIC_RAW_FIELDS = [
        'AsText(location) as location'
    ];

    /**
     * Display user
     *
     * GET /users/{$userId}
     *
     * @param Request $request Incoming request object
     *
     * @return object Template rendering
     */
    public function showAction(
        Request $request,
        $userId
    ) {
        $params = [];

        $user = $this->app['repositories.users']->findById(
            $userId,
            $this->getPublicFields(['status'])
        );

        if (empty($user) || $user->status != \Validator\Users\StatusChoice::VALID) {
            return $this->app->abort(
                404,
                'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo()
            );
        }
        $params['user'] = $user;

        $params['myEvents'] = $this->app['repositories.events']->runPaginatedSearch(
            [
                'with'  => ['eventsvotes'],
                'where' => [[
                    'user_id' => $userId,
                    'status'  => \Validator\Events\StatusChoice::VALID
                ]],
                'orderBy' => ['start_at', 'desc']
            ],
            [
                'limit'  => 10,
                'offset' => 1
            ],
            ['*', new \Illuminate\Database\Query\Expression('AsText(location) as location') ]
        )['data']->toArray();

        $where = [];
        $datas = [
            'events_votes.user_id' => $userId,
            'events_votes.status'  => \Validator\Events\StatusChoice::VALID,
            'events.status'        => \Validator\Events\StatusChoice::VALID,
        ];
        $where[] = function ($query) use ($datas) {
            foreach ($datas as $data => $dataValue) {
                $query->whereIn($data, explode(',', $dataValue));
            }
            $query->where('start_at', '<', date('Y-m-d H:i:s', time() - 1800));
        };

        $params['pastEvents'] = $this->app['repositories.eventsvotes']->runPaginatedSearch(
            [
                'leftJoin' => ['events', 'events.id', '=', 'events_votes.event_id'],
                'with'     => [$this->getRelationshipPublicFields('event')],
                'where'    => $where,
                'orderBy' => ['events_votes.created_at', 'desc']
            ],
            [
                'limit'  => 10,
                'offset' => 1
            ]
        )['data']->toArray();

        if ($userId == $this->app['session']->get('user')['id']) {
            $where   = [];
            $where[] = function ($query) use ($datas) {
                foreach ($datas as $data => $dataValue) {
                    $query->whereIn($data, explode(',', $dataValue));
                }
                $query->where('start_at', '>=', date('Y-m-d H:i:s', time() - 1800));
            };

            $params['incomingEvents'] = $this->app['repositories.eventsvotes']->runPaginatedSearch(
                [
                    'leftJoin' => ['events', 'events.id', '=', 'events_votes.event_id'],
                    'with'     => [$this->getRelationshipPublicFields('event')],
                    'where'    => $where,
                    'orderBy' => ['events_votes.created_at', 'desc']
                ],
                [
                    'limit'  => 10,
                    'offset' => 1
                ]
            )['data']->toArray();
        }

        return $this->app->render(
            'users/get.twig',
            $params
        );
    }
}
