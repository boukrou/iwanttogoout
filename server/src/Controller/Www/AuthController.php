<?php

namespace Controller\Www;

use Exception;
use Controller\ControllerActionAbstract;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

use Repository\Exception as RepositoryException;

/**
 * AuthController main action controller
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class AuthController extends ControllerActionAbstract
{
    use \Repository\SecurityRepositoryTrait;

    /**
     *
     *
     *
     */
    public function signupAction(
        Request $request
    ) {
        $signinForm = $this->signinForm();
        $signupForm = $this->signupForm();
        $errors     = [];

        if ('POST' === $request->getMethod()) {

            $signupForm->bind($request);
            if ($signupForm->isValid()) {
                $data  = $signupForm->getData();
                $token = '';
                $ok    = false;

                try {
                    $user = $this->app['repositories.users']->create(
                        $data
                    );

                    if (!empty($user)) {
                        $token = base64_encode($user->id.':'.$user->hashed_password);
                        $ok = true;
                    }
                } catch (RepositoryException\InvalidDataException $e) {
                    $errors = $e->getErrors();

                    $signupForm->addError(
                        new FormError($e->getMessage())
                    );
                    foreach ($errors as $k => $v) {
                        try {
                            $formField = str_replace(
                                ']',
                                '',
                                str_replace(
                                    '[',
                                    '',
                                    $k
                                )
                            );
                            $formEl = $signupForm->get($formField);
                            $formEl->addError(new FormError($v));

                            $errors = $this->app['utils.formErrorsSerializer']->serializeFormErrors($signupForm, true, true);

                        } catch (\Exception $ee) {
                            //ingnored exception
                        }
                    }
                } catch (\Exception $e) {
                    $errors['global'][] = $e->getMessage();
                }

                if ($ok) {
                    $user = $user->toArray();
                    foreach ($user as $k => $v) {
                        if (!in_array($k, ['id', 'username', 'team_id'])) {
                            unset($user[$k]);
                        }
                    }
                    $this->setSession(true, $user, $token);

                    return $this->app->json(
                        [
                            'state' => 'success',
                            'data'  => $user
                        ],
                        200
                    );

                } else {
                    if (empty($e)) {
                        $errors['global'][] = $this->app['translator']->trans('Signup failed');
                    }
                }
            } else {
                $errors = $this->app['utils.formErrorsSerializer']->serializeFormErrors($signupForm, true, true);
            }

            if (!empty($errors)) {
                return $this->app->json(
                    [
                        'state' => 'error',
                        'data'  => $errors
                    ],
                    200
                );
            }
        }

        return $this->app->render(
            'auth/connection.twig',
            [
                'signinForm' => [
                    'view' => $signinForm->createView(),
                    'action' => $this->app->url(
                        'www_auth_signin_post'
                    ),
                    'method' => 'POST',
                ],
                'signupForm' => [
                    'view'   => $signupForm->createView(),
                    'action' => $this->app->url(
                        'www_auth_signup_post'
                    ),
                    'method' => 'POST',
                ],
            ]
        );
    }

    /**
     *
     *
     *
     */
    public function signinAction(
        Request $request
    ) {
        $signinForm = $this->signinForm();
        $signupForm = $this->signupForm();
        $errors     = [];

        if ('POST' === $request->getMethod()) {
            $signinForm->bind($request);
            if ($signinForm->isValid()) {
                $data     = $signinForm->getData();

                $email    = $data['email'];
                $password = $data['password'];
                $token    = '';
                $ok       = false;

                try {
                    $hashedPassword = $this->generateHashedPasswordField($password);
                    $user           = $this->app['repositories.users']->find(
                        ['where' => [['email' => $email, 'password' => $hashedPassword['hashed_password']]]],
                        ['id', 'username', 'team_id']
                    )->first();

                    if (!empty($user)) {
                        $token = base64_encode($user->id.':'.$hashedPassword['hashed_password']);
                        $ok = true;
                    }
                } catch (\Exception $e) {
                    $errors['global'][] = $e->getMessage();
                }

                if ($ok) {
                    $this->setSession(true, $user->toArray(), $token);

                    return $this->app->json(
                        [
                            'state' => 'success',
                            'data'  => $user
                        ],
                        200
                    );

                } else {
                    if (empty($e)) {
                        $errors['global'][] = $this->app['translator']->trans('Your mail or password is not correct');
                    }
                }
            } else {
                $errors = $this->app['utils.formErrorsSerializer']->serializeFormErrors($signinForm, true, true);
            }

            if (!empty($errors)) {
                return $this->app->json(
                    [
                        'state' => 'error',
                        'data'  => $errors
                    ],
                    200
                );
            }
        }

        return $this->app->render(
            'auth/connection.twig',
            [
                'signinForm' => [
                    'view'   => $signinForm->createView(),
                    'action' => $this->app->url(
                        'www_auth_signin_post'
                    ),
                    'method' => 'POST',
                ],
                'signupForm' => [
                    'view'   => $signupForm->createView(),
                    'action' => $this->app->url(
                        'www_auth_signup_post'
                    ),
                    'method' => 'POST',
                ],
            ]
        );
    }

    /**
     *
     *
     *
     */
    public function logoutAction(
        Request $request
    ) {
        $this->setSession();
        $this->app['session']->clear();

        return $this->app->redirect(
            $this->app->url('www_home')
        );
    }

    /**
     *
     *
     *
     */
    protected function setSession(
        $isAuthenticated = false,
        $user            = null,
        $token           = null
    ) {
        $this->app['session']->set('isAuthenticated', $isAuthenticated);
        $this->app['session']->set('user', $user);
        $this->app['session']->set('token', $token);
    }

    /**
     *
     *
     *
     */
    public function isAuthenticated(Request $request)
    {
        //default return
        $ret = null;

        switch ($request->getRequestUri()) {
            case $this->app->path('www_auth_signin'):

                if ($this->app['session']->get('isAuthenticated')) {
                    $ret = $this->app->redirect($this->app->url['www_events_index']);
                }
                break;

            case $this->app->path('www_auth_logout'):
                if (!$this->app['session']->get('isAuthenticated')) {
                    $ret = $this->app->redirect($this->app->url('www_auth_signin'));
                }
                break;

            default:
                if (!$this->app['session']->get('isAuthenticated')) {
                    $ret = $this->app->redirect($this->app->url('www_auth_signin'));
                }
                break;
        }

        return $ret;
    }

    protected function signinForm(
        array $data = [],
        array $options = []
    ) {
        return $this->app->form(
            $data,
            $options
        )->add(
            'email',
            'text',
            [
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => $this->app['translator']->trans("This field is required"),
                    ]),
                    new Assert\Email([
                        'message' => $this->app['translator']->trans("Your mail is not valid"),
                    ]),
                ],
            ]
        )->add(
            'password',
            'password',
            [
                'label' => "Password",
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => $this->app['translator']->trans("This field is required"),
                    ]),
                    new \Validator\Users\Password([
                        'minMessage' => $this->app['translator']->trans("Your password has to be {{ limit }} characters at least"),
                        'maxMessage' => $this->app['translator']->trans("Your password has to be {{ limit }} characters maximum"),
                    ]),
                ],
            ]
        )->getForm();
    }

    protected function signupForm(
        array $data = [],
        array $options = []
    ) {
        return $this->app->form(
            $data,
            $options
        )->add(
            'email',
            'text',
            [
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => $this->app['translator']->trans("This field is required"),
                    ]),
                    new Assert\Email([
                        'message' => $this->app['translator']->trans("Your mail is not valid"),
                    ]),
                ],
            ]
        )->add(
            'password',
            'repeated',
            [
                'type'            => 'password',
                'invalid_message' => $this->app['translator']->trans("Password are not identical"),
                'first_options'   => [
                    'label'       => "Password",
                    'constraints' => [
                        new Assert\NotBlank([
                            'message' => $this->app['translator']->trans("This field is required"),
                        ]),
                        new \Validator\Users\Password([
                            'minMessage' => $this->app['translator']->trans("Your password has to be {{ limit }} characters at least"),
                            'maxMessage' => $this->app['translator']->trans("Your password has to be {{ limit }} characters maximum"),
                        ]),
                    ]
                ],
                'second_options'  => [
                    'label' => $this->app['translator']->trans("Confirm password")
                ]
            ]
        )->add(
            'team_id',
            'choice',
            [
                'choices'    => \Validator\Users\TeamChoice::getChoicesAndLabels(),
                'constraints' => [
                    new \Validator\Users\TeamChoice()
                ],
                'label'       => $this->app['translator']->trans("Team")
            ]
        )->getForm();
    }
}