<?php

namespace Controller\Www;

use Exception;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

use Controller\ControllerActionAbstract;
use Repository\Exception as RepositoryException;
use Validator;

/**
 * HomeController main action controller
 *
 * @author Valérian Brie <thethrower@hotmail.com>
 */
class EventsController extends ControllerActionAbstract
{
    protected static $STATUS_CHOICES_404 = [
        Validator\Events\StatusChoice::DELETED_PENDING,
        Validator\Events\StatusChoice::DELETED,
        Validator\Events\StatusChoice::MODERATED
    ];

    protected static $PUBLIC_RAW_FIELDS = [
        'AsText(location) as location'
    ];

    /**
     * Display events home
     *
     * GET /
     *
     * @param Request $request Incoming request object
     *
     * @return object Template rendering
     */
    public function indexAction(Request $request)
    {
        return $this->app->render('events/list.twig', array(
            'page_name' => 'list'
        ));
    }

    public function createAction(
        Request $request
    ) {
        $form   = $this->form();
        $errors = [];

        if ('POST' === $request->getMethod()) {
            $form->bind($request);

            return $this->formPost($form);
        }

        return $this->app->render(
            'events/add.twig',
            array(
                'form' => array(
                    'view' => $form->createView(),
                    'action' => $this->app->url(
                        'www_events_create_post'
                    ),
                    'method' => 'POST'
                )
            )
        );
    }

    public function editAction(
        $userId,
        $eventId,
        Request $request
    ) {
        $event = $this->app['repositories.events']->findById(
            $eventId,
            ['*', new \Illuminate\Database\Query\Expression('AsText(location) as location') ]
        );

        if (empty($event) || $event->user_id != $userId || (!empty($event->status) && in_array($event->status, static::$STATUS_CHOICES_404))) {
            return $this->app->abort(
                404,
                'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo()
            );
        }

        $form   = $this->form();
        $errors = [];

        if ('POST' === $request->getMethod()) {
            $form->bind($request);

            return $this->formPost($form, $eventId);
        } else {
            $data = [];
            $data['start_at_date'] = new \DateTime(date('Y/m/d', $event->start_at));
            $data['start_at_time'] = new \DateTime(date('H:i', $event->start_at));
            $data['coordinates']   = implode(',', $event->location);

            $form   = $this->form(array_merge($data, $event->toArray()));
        }

        return $this->app->render(
            'events/add.twig',
            array(
                'event' => $event,
                'form' => array(
                    'view' => $form->createView(),
                    'action' => $this->app->url(
                        'www_events_edit_post',
                        [
                            'userId'  => $userId,
                            'eventId' => $eventId,
                        ]
                    ),
                    'method' => 'POST'
                )
            )
        );
    }

    protected function formPost(
        $form,
        $eventId = null
    ) {
        if ($form->isValid()) {
            $data               = $form->getData();
            $coordinates        = $data['coordinates'];
            $data['location']   = [];
            $data['user_id']    = $this->app['session']->get('user')['id'];
            $data['event_type'] = (int)$data['event_type'];
            $data['start_at']   = strtotime($data['start_at_date']->format('Y/m/d')." ".$data['start_at_time']->format('H:i'));

            unset($data['coordinates']);
            unset($data['start_at_date']);
            unset($data['start_at_time']);

            if (!empty($coordinates)) {
                $l = explode(',', $coordinates);
                $data['location']['latitude'] = $l[0];
                if (isset($l[1])) {
                    $data['location']['longitude'] = $l[1];
                }
            }

            try {
                $event = !empty($eventId) ? $this->app['repositories.events']->save($eventId, $data) : $this->app['repositories.events']->create($data);

                return $this->app->json(
                    [
                        'state' => 'success',
                        'data'  => $event
                    ],
                    200
                );

            } catch (RepositoryException\InvalidDataException $e) {
                $errors = $e->getErrors();

                $form->addError(
                    new FormError($e->getMessage())
                );

                foreach ($errors as $k => $v) {
                    try {
                        switch ($k) {
                            case '[location]':
                            case '[location][latitude]':
                            case '[location][longitude]':
                                $form->get('coordinates')
                                    ->addError(
                                        new FormError($k.' '.$v)
                                    );
                                break;
                            default:
                                $formField = str_replace(
                                    ']',
                                    '',
                                    str_replace(
                                        '[',
                                        '',
                                        $k
                                    )
                                );
                                $formEl = $form->get($formField);
                                $formEl->addError(new FormError($v));
                                break;
                        }

                        $errors = $this->app['utils.formErrorsSerializer']->serializeFormErrors($form, true, true);
                    } catch (\Exception $ee) {
                        //ingnored exception
                    }
                }
            } catch (\Exception $e) {
                $errors['global'][] = $this->app['translator']->trans('An unknown error occured'. $e->getMessage());
            }
        } else {
            $errors = $this->app['utils.formErrorsSerializer']->serializeFormErrors($form, true, true);
        }

        if (!empty($errors)) {
            return $this->app->json(
                [
                    'state' => 'error',
                    'data'  => $errors
                ],
                200
            );
        }
    }

    protected function form(
        array $data = [],
        array $options = []
    ) {
        if (!empty($data['location'])) {
            $data['coordinates'] = implode(',', $data['location']);
        }

        return $this->app->form(
            $data,
            $options
        )->add(
            'start_at_date',
            'date',
            [
                'attr' => [
                    'class' => '',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ]
        )->add(
            'start_at_time',
            'time',
            [
                'attr' => [
                    'class' => '',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ]
        )->add(
            'event_type',
            'choice',
            [
                'choices' => Validator\Events\EventTypeChoice::getChoicesAndLabels(),
                'constraints' => [
                    new Validator\Events\EventTypeChoice()
                ]
            ]
        )->add(
            'coordinates',
            'hidden',
            [
                'constraints' => [
                    new Assert\NotBlank()
                ]
            ]
        )->add(
            'location_data',
            'hidden',
            [
                'constraints' => [
                    new Assert\NotBlank()
                ]
            ]
        )->getForm();
    }
}
