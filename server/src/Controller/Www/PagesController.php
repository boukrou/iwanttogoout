<?php

namespace Controller\Www;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Symfony\Component\Validator\Constraints as Assert;

use Controller\ControllerActionAbstract;

/**
 * HomeController main action controller
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class PagesController extends ControllerActionAbstract
{
    /**
     * Display contact form
     *
     * GET /
     *
     * @param Request $request Incoming request object
     *
     * @return object Template rendering
     */
    public function contactAction(Request $request)
    {
        $form = $this->form();

        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();
                try {
                    $data['fromIP'] = $request->getClientIp();
                    $data['fromUserAgent'] = $request->headers->get('User-Agent');

                    $message = $this->app->render(
                        'mails/contact.twig',
                        [
                            'data' => $data,
                        ]
                    );

                    $this->app->mail(
                        \Swift_Message::newInstance()
                            ->setSubject('[Pokeasy] Contact')
                            ->setFrom([$this->app['mail_contact']])
                            ->setTo([$this->app['mail_contact']])
                            ->setBody($message)
                            ->addPart($message, 'text/html')
                    );
                    $this->app['session']
                        ->getFlashBag()
                        ->add(
                            'success',
                            'mail_success'
                        );
                } catch (Exception $e) {
                    $this->app['session']
                        ->getFlashBag()
                        ->add(
                            'errors',
                            'Action failed '.$e->getMessage()
                        );
                }
            }
        }

        return $this->app->render(
            'pages/contact.twig',
            [
                'form' => [
                    'view' => $form->createView(),
                    'action' => $this->app->url(
                        'www_contact_post'
                    ),
                    'method' => 'POST',
                ],
            ]
        );
    }

    /**
     * Display about us
     *
     * GET /about-us
     *
     * @param Request $request Incoming request object
     *
     * @return object Template rendering
     */
    public function aboutUsAction(Request $request)
    {
        return $this->app->render('pages/aboutus_'.$this->app['locale'].'.twig');
    }

    /**
     * Display about us
     *
     * GET /about-us
     *
     * @param Request $request Incoming request object
     *
     * @return object Template rendering
     */
    public function termsAction(Request $request)
    {
        return $this->app->render('pages/terms_'.$this->app['locale'].'.twig');
    }

    /**
     * Display about us
     *
     * GET /about-us
     *
     * @param Request $request Incoming request object
     *
     * @return object Template rendering
     */
    public function privacyAction(Request $request)
    {
        return $this->app->render('pages/privacy_'.$this->app['locale'].'.twig');
    }

    protected function form(
        array $data = array(),
        array $options = array()
    ) {
        return $this->app->form(
            $data,
            $options
        )->add(
            'lastname',
            'text',
            [
                'label' => 'contact_lastname',
                'constraints' => [
                    new Assert\NotBlank(
                        [
                            'message' => $this->app['translator']->trans('This field is required'),
                        ]
                    ),
                    new Assert\Length(
                        [
                            'min' => 2,
                            'max' => 50,
                            'minMessage' => $this->app['translator']->trans('Your lastname has to be at least {{ limit }} characters'),
                            'maxMessage' => $this->app['translator']->trans('Your lastname has to be {{ limit }} characters maximum'),
                        ]
                    ),
                ],
            ]
        )->add(
            'firstname',
            'text',
            [
                'label' => 'contact_firstname',
                'constraints' => [
                    new Assert\NotBlank(
                        [
                            'message' => $this->app['translator']->trans('This field is required'),
                        ]
                    ),
                    new Assert\Length(
                        [
                            'min' => 2,
                            'max' => 50,
                            'minMessage' => $this->app['translator']->trans('Your firstname has to be at least {{ limit }} characters'),
                            'maxMessage' => $this->app['translator']->trans('Your firstname has to be {{ limit }} characters maximum'),
                        ]
                    ),
                ],
            ]
        )->add(
            'email',
            'text',
            [
                'label' => 'contact_email',
                'constraints' => [
                    new Assert\NotBlank(
                        [
                            'message' => $this->app['translator']->trans('This field is required'),
                        ]
                    ),
                    new Assert\Email(
                        [
                            'message' => $this->app['translator']->trans('Email {{ value }} is not a valid email.'),
                            'checkMX' => true,
                        ]
                    ),
                ],
            ]
        )->add(
            'comment',
            'textarea',
            [
                'label' => 'contact_comment',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'rows' => '7',
                ],
            ]
        )->getForm();
    }
}
