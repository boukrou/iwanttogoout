<?php

namespace Controller\Www;

use Controller\ControllerActionAbstract;
use Symfony\Component\HttpFoundation\Request;

use Validator;

/**
 * EventSearchController action controller for event searching
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class EventsSearchController extends ControllerActionAbstract
{
    protected static $PUBLIC_FIELDS = [
        'events.id',
        'events.title',
        'events.user_id',
        'events.description',
        'events.start_at',
        'events.end_at',
        'events.event_type',
        'events.team_id',
        'events.is_sponsored',
    ];

    protected static $RELATIONSHIP_PUBLIC_FIELDS = [
        'user' => [
            'id',
            'team_id',
            'username',
            'picture',
            'description',
        ]
    ];

    protected static $PUBLIC_RAW_FIELDS = [
        'AsText(location) as location'
    ];

    protected static $STATUS_CHOICES_404 = [
        Validator\Events\StatusChoice::DELETED_PENDING,
        Validator\Events\StatusChoice::DELETED,
        Validator\Events\StatusChoice::MODERATED
    ];

    /**
     * @const DEFAULT_RADIUS_METERS Default radius used in the near search (meters)
     */
    const DEFAULT_RADIUS_METERS = 3000;

    /**
     * @const DEFAULT_TARGET Default target number of results in the near search
     */
    const DEFAULT_TARGET = 100;

    /**
     * @const DEFAULT_MAX_DEPTH Default maximum depth of the near search
     */
    const DEFAULT_MAX_DEPTH = 5;

    /**
     * Fetch paginated list of events near a location
     *
     * GET /events/near/{latitude}/{longitude}/{offset}
     *
     * @param Request $request  Incoming request object
     * @param float $latitude   Latitude
     * @param float $longitude  Longitude
     * @param int $offset       Offset
     *
     * @return array
     */
    public function nearAction(
        Request $request,
        $latitude,
        $longitude,
        $offset
    ) {
        return $this->execToJsonOrAbort(
            $request,
            function ($params) use ($request, $latitude, $longitude, $offset) {
                $results             = [];
                $enoughResults       = false;
                $distanceInKm        = self::DEFAULT_RADIUS_METERS / 1000;
                $target              = self::DEFAULT_TARGET;
                $maxDepth            = self::DEFAULT_MAX_DEPTH;
                $currentRadiusFactor = 1;

                if ($this->app->offsetExists('eventssearch.near.radiusmeters')) {
                    $distanceInKm = $this->app['eventssearch.near.radiusmeters'] / 1000;
                }
                if ($this->app->offsetExists('eventssearch.near.target')) {
                    $target = $this->app['eventssearch.near.target'];
                }
                if ($this->app->offsetExists('eventssearch.near.max_depth')) {
                    $maxDepth = $this->app['eventssearch.near.max_depth'];
                }

                $params['events.status'] = \Validator\Events\StatusChoice::VALID;
                $params['events_votes.status'] = \Validator\StatusChoice::VALID;
                if (!empty($params)) {
                    $where[] = function ($query) use ($params) {
                        foreach ($params as $param => $paramValue) {
                            $query->whereIn($param, explode(',', $paramValue));
                        }
                        $query->where('start_at', '>=', date('Y-m-d H:i:s', time() - 1800));
                    };
                }

                while (!$enoughResults) {
                    $distance  = $distanceInKm * $currentRadiusFactor;
                    $lon1      = $longitude + ($distance/abs(cos(deg2rad($latitude))*111));
                    $lat1      = $latitude + ($distance/111);
                    $lon2      = $longitude - ($distance/abs(cos(deg2rad($latitude))*111));
                    $lat2      = $latitude - ($distance/111);

                    $params    = [
                        'leftJoin'   => ['events_votes', 'events.id', '=', 'events_votes.event_id'],
                        'with'       => [$this->getRelationshipPublicFields('user')],
                        'whereRaw'   => ['MBRContains(envelope(linestring(point(('.$lat1.'), ('.$lon1.')), point(('.$lat2.'), ('.$lon2.')))), location)'],
                        'orderByRaw' => ['is_sponsored desc, distance_in_meters asc'],
                        'groupBy'    => ['events_votes.event_id'],
                    ];

                    if (!empty($where)) {
                        $params['where'] = $where;
                    }

                    $results = $this->app['repositories.events']->runPaginatedSearch(
                        $params,
                        [
                            'limit'    => $target,
                            'offset'   => $offset
                        ],
                        $this->getPublicFields([
                            new \Illuminate\Database\Query\Expression('COUNT(events_votes.id) as votes_total'),
                            new \Illuminate\Database\Query\Expression('slc( '.$latitude.', '.$longitude.', x(location), y(location))*1000 as distance_in_meters'),
                            new \Illuminate\Database\Query\Expression('start_at BETWEEN DATE(DATE_SUB(NOW(), INTERVAL 30 MINUTE)) AND timestamp(NOW()) as has_started'),
                            new \Illuminate\Database\Query\Expression(
                                '
                                (SELECT COUNT(*) FROM events_votes 
                                WHERE
                                    events_votes.event_id = events.id AND
                                    events_votes.user_id = '.(!empty($this->app['session']->get('user')) ? $this->app['session']->get('user')['id'] : '-1').' AND
                                    events_votes.status = '.\Validator\StatusChoice::VALID.'
                                ) as is_going')
                        ])
                    );

                    if (count($results['data']) >= $target) {
                        $enoughResults = true;
                    } else {
                        $currentRadiusFactor++;

                        if ($currentRadiusFactor > $maxDepth) {
                            $enoughResults = true;
                        }
                    }
                }

                return $results;
            }
        );
    }

    /**
     * Fetch paginated list of events sorted by start_at
     *
     * GET /events/recent/{offset}
     *
     * @param Request $request  Incoming request object
     * @param int $offset       Offset
     *
     * @return array
     */
    public function recentAction(
        Request $request,
        $latitude,
        $longitude,
        $offset
    ) {
        return $this->execToJsonOrAbort(
            $request,
            function ($params) use ($request, $latitude, $longitude, $offset) {
                $results             = [];
                $enoughResults       = false;
                $currentRadiusFactor = 1;
                $distanceInKm        = self::DEFAULT_RADIUS_METERS / 1000;
                $target              = self::DEFAULT_TARGET;
                $maxDepth            = self::DEFAULT_MAX_DEPTH;

                if ($this->app->offsetExists('eventssearch.near.radiusmeters')) {
                    $distanceInKm = $this->app['eventssearch.near.radiusmeters'] / 1000;
                }
                if ($this->app->offsetExists('eventssearch.near.target')) {
                    $target = $this->app['eventssearch.near.target'];
                }
                if ($this->app->offsetExists('eventssearch.near.max_depth')) {
                    $maxDepth = $this->app['eventssearch.near.max_depth'];
                }

                $params['events.status'] = \Validator\Events\StatusChoice::VALID;
                $params['events_votes.status'] = \Validator\StatusChoice::VALID;
                if (!empty($params)) {
                    $where[] = function ($query) use ($params) {
                        foreach ($params as $param => $paramValue) {
                            $query->whereIn($param, explode(',', $paramValue));
                        }
                        $query->where('events.start_at', '>=', date('Y-m-d H:i:s', time() - 1800));
                    };
                }

                while (!$enoughResults) {
                    $distance  = $distanceInKm * $currentRadiusFactor;
                    $lon1      = $longitude + ($distance/abs(cos(deg2rad($latitude))*111));
                    $lat1      = $latitude + ($distance/111);
                    $lon2      = $longitude - ($distance/abs(cos(deg2rad($latitude))*111));
                    $lat2      = $latitude - ($distance/111);

                    $params    = [
                        'leftJoin'   => ['events_votes', 'events.id', '=', 'events_votes.event_id'],
                        'with'       => [$this->getRelationshipPublicFields('user')],
                        'whereRaw'   => ['MBRContains(envelope(linestring(point(('.$lat1.'), ('.$lon1.')), point(('.$lat2.'), ('.$lon2.')))), location)'],
                        'groupBy'    => ['events_votes.event_id'],
                        'orderByRaw' => ['is_sponsored desc, events.start_at asc'],
                    ];

                    if (!empty($where)) {
                        $params['where'] = $where;
                    }

                    $results = $this->app['repositories.events']->runPaginatedSearch(
                        $params,
                        [
                            'limit'    => $target,
                            'offset'   => $offset
                        ],
                        $this->getPublicFields([
                            new \Illuminate\Database\Query\Expression('COUNT(events_votes.id) as votes_total'),
                            new \Illuminate\Database\Query\Expression('slc( '.$latitude.', '.$longitude.', x(location), y(location))*1000 as distance_in_meters'),
                            new \Illuminate\Database\Query\Expression('start_at BETWEEN DATE(DATE_SUB(NOW(), INTERVAL 30 MINUTE)) AND timestamp(NOW()) as has_started'),
                            new \Illuminate\Database\Query\Expression('
                                (SELECT COUNT(*) FROM events_votes 
                                WHERE
                                    events_votes.event_id = events.id AND
                                    events_votes.user_id = '.(!empty($this->app['session']->get('user')) ? $this->app['session']->get('user')['id'] : '-1').' AND
                                    events_votes.status = '.\Validator\StatusChoice::VALID.'
                                ) as is_going')
                        ])
                    );

                    if (count($results['data']) >= $target) {
                        $enoughResults = true;
                    } else {
                        $currentRadiusFactor++;

                        if ($currentRadiusFactor > $maxDepth) {
                            $enoughResults = true;
                        }
                    }
                }

                return $results;
            }
        );
    }

    /**
     * Fetch paginated list of events sorted by votes
     *
     * GET /events/popular/{offset}
     *
     * @param Request $request  Incoming request object
     * @param int $offset       Offset
     *
     * @return array
     */
    public function popularAction(
        Request $request,
        $latitude,
        $longitude,
        $offset
    ) {
        return $this->execToJsonOrAbort(
            $request,
            function ($params) use ($request, $latitude, $longitude, $offset) {
                $results             = [];
                $enoughResults       = false;
                $currentRadiusFactor = 1;
                $distanceInKm        = self::DEFAULT_RADIUS_METERS / 1000;
                $target              = self::DEFAULT_TARGET;
                $maxDepth            = self::DEFAULT_MAX_DEPTH;

                if ($this->app->offsetExists('eventssearch.near.radiusmeters')) {
                    $distanceInKm = $this->app['eventssearch.near.radiusmeters'] / 1000;
                }
                if ($this->app->offsetExists('eventssearch.near.target')) {
                    $target = $this->app['eventssearch.near.target'];
                }
                if ($this->app->offsetExists('eventssearch.near.max_depth')) {
                    $maxDepth = $this->app['eventssearch.near.max_depth'];
                }

                $params['events.status'] = \Validator\Events\StatusChoice::VALID;
                $params['events_votes.status'] = \Validator\Events\StatusChoice::VALID;
                if (!empty($params)) {
                    $where[] = function ($query) use ($params) {
                        foreach ($params as $param => $paramValue) {
                            $query->whereIn($param, explode(',', $paramValue));
                        }
                        $query->where('start_at', '>=', date('Y-m-d H:i:s', time() - 1800));
                    };
                }

                while (!$enoughResults) {
                    $distance  = $distanceInKm * $currentRadiusFactor;
                    $lon1      = $longitude + ($distance/abs(cos(deg2rad($latitude))*111));
                    $lat1      = $latitude + ($distance/111);
                    $lon2      = $longitude - ($distance/abs(cos(deg2rad($latitude))*111));
                    $lat2      = $latitude - ($distance/111);

                    $params    = [
                        'leftJoin'   => ['events_votes', 'events.id', '=', 'events_votes.event_id'],
                        'with'       => [$this->getRelationshipPublicFields('user')],
                        'whereRaw'   => ['MBRContains(envelope(linestring(point(('.$lat1.'), ('.$lon1.')), point(('.$lat2.'), ('.$lon2.')))), location)'],
                        'groupBy'    => ['events_votes.event_id'],
                        'orderByRaw' => ['is_sponsored desc, votes_total desc'],
                    ];

                    if (!empty($where)) {
                        $params['where'] = $where;
                    }

                    $results = $this->app['repositories.events']->runPaginatedSearch(
                        $params,
                        [
                            'limit'    => $target,
                            'offset'   => $offset
                        ],
                        $this->getPublicFields([
                            new \Illuminate\Database\Query\Expression('COUNT(events_votes.id) as votes_total'),
                            new \Illuminate\Database\Query\Expression('slc( '.$latitude.', '.$longitude.', x(location), y(location))*1000 as distance_in_meters'),
                            new \Illuminate\Database\Query\Expression('start_at BETWEEN DATE(DATE_SUB(NOW(), INTERVAL 30 MINUTE)) AND timestamp(NOW()) as has_started'),
                            new \Illuminate\Database\Query\Expression('
                                (SELECT COUNT(*) FROM events_votes 
                                WHERE
                                    events_votes.event_id = events.id AND
                                    events_votes.user_id = '.(!empty($this->app['session']->get('user')) ? $this->app['session']->get('user')['id'] : '-1').' AND
                                    events_votes.status = '.\Validator\StatusChoice::VALID.'
                                ) as is_going')
                        ])
                    );

                    if (count($results['data']) >= $target) {
                        $enoughResults = true;
                    } else {
                        $currentRadiusFactor++;

                        if ($currentRadiusFactor > $maxDepth) {
                            $enoughResults = true;
                        }
                    }
                }

                return $results;
            }
        );
    }
}
