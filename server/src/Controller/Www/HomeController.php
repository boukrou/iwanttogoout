<?php

namespace Controller\Www;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Controller\ControllerActionAbstract;

/**
 * HomeController main action controller
 *
 * @author Valérian Brie <thethrower@hotmail.com>
 */
class HomeController extends ControllerActionAbstract
{
    /**
     * Display home
     *
     * GET /
     *
     * @param Request $request Incoming request object
     *
     * @return object Template rendering
     */
    public function indexAction(Request $request)
    {
        return $this->app->render('home/index.twig');
    }
}
