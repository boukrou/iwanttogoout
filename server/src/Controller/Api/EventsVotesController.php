<?php

namespace Controller\Api;

use Symfony\Component\HttpFoundation\Request;

use Controller\ControllerActionAbstract;
use Validator;

/**
 * EventsVotes action controller for votes management
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class EventsVotesController extends ControllerActionAbstract
{
    protected static $PUBLIC_FIELDS = [
        'id',
        'user_id',
        'event_id',
        'vote_type',
    ];

    protected static $RELATIONSHIP_PUBLIC_FIELDS = [
        'user' => [
            'id',
            'team_id',
            'username',
            'description',
        ],
        'event' => [
            'id',
            'title',
            'user_id',
            'description',
            'start_at',
            'end_at',
            'event_type',
            'team_id',
        ]
    ];

    /**
     * Returns events where an user went
     *
     * GET /users/{userId}/activity/events/past/{offset}
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic API information
     */
    public function goPastListAction(
        Request $request,
        $userId,
        $offset
    ) {
        return $this->execToJsonOrAbort(
            $request,
            function ($params) use ($request, $userId, $offset) {
                $events = $this->app['repositories.eventsvotes']->runPaginatedSearch(
                    [
                        'with'    => [$this->getRelationshipPublicFields('event')],
                        'whereDate' => ['start_at', '<', date('Y-m-d H:i:s')],
                        'where'   => [
                            ['user_id' => $userId],
                            ['status'  => \Validator\Events\StatusChoice::VALID]
                        ],
                        'orderBy' => ['created_at', 'desc']
                    ],
                    [
                        'limit'    => 20,
                        'offset'   => $offset
                    ],
                    $this->getPublicFields()
                );
                return $events;
            }
        );
    }

    /**
     * Returns events where an user planned to go
     *
     * GET /users/{userId}/activity/events/incoming{offset}
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic API information
     */
    public function goIncomingListAction(
        Request $request,
        $userId,
        $offset
    ) {
        return $this->execToJsonOrAbort(
            $request,
            function ($params) use ($request, $userId, $offset) {
                $events = $this->app['repositories.eventsvotes']->runPaginatedSearch(
                    [
                        'with'    => [$this->getRelationshipPublicFields('event')],
                        'whereDate' => ['start_at', '>=', date('Y-m-d H:i:s')],
                        'where'   => [
                            ['user_id' => $userId],
                            ['status'  => \Validator\Events\StatusChoice::VALID]
                        ],
                        'orderBy' => ['created_at', 'desc']
                    ],
                    [
                        'limit'    => 20,
                        'offset'   => $offset
                    ],
                    $this->getPublicFields()
                );
                return $events;
            }
        );
    }

    /**
     * Returns .....
     *
     * POST /users/{userId}/events/{eventId}/go
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic API information
     */
    public function goAction(
        Request $request,
        $eventId
    ) {
        list($responseData, $statusCode) = $this->performVoteToJson(
            $request,
            ['vote_type' => Validator\EventsVotes\EventsVotesTypeChoice::GO,
             'user_id'   => $this->app['authUser']->id,
             'event_id'  => $eventId]
        );

        return $this->app->json(
            $responseData,
            $statusCode
        );
    }

    /**
     * Returns .....
     *
     * DELETE /users/{userId}/events/{eventId}/go
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic API information
     */
    public function ungoAction(
        Request $request,
        $eventId
    ) {
        list($responseData, $statusCode) = $this->performRemoveVoteToJson(
            $request,
            ['vote_type' => Validator\EventsVotes\EventsVotesTypeChoice::GO,
             'user_id'   => $this->app['authUser']->id,
             'event_id'  => $eventId]
        );

        return $this->app->json(
            $responseData,
            $statusCode
        );
    }

    protected function performVoteToJson(
        Request $request,
        $voteParams
    ) {
        return $this->execOrAbort(
            $request,
            function ($params) use ($request, $voteParams) {
                $vote = $this->app['repositories.eventsvotes']->create(
                    $voteParams
                );

                return [
                    !empty($vote),
                    !empty($vote) ? 200 : 403
                ];
            }
        );
    }

    protected function performRemoveVoteToJson(
        Request $request,
        $voteParams
    ) {
        return $this->execOrAbort(
            $request,
            function ($params) use ($request, $voteParams) {
                $vote = $this->app['repositories.eventsvotes']->remove(
                    $voteParams
                );

                return [
                    !empty($vote),
                    !empty($vote) ? 200 : 403
                ];

            }
        );
    }
}
