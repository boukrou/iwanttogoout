<?php

namespace Controller\Api;

use Symfony\Component\HttpFoundation\Request;

/**
 * EventController action controller for event management
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class EventsController extends EventsControllerAbstract
{
    const DEFAULT_USER_LIST_LIMIT = 20;

    /**
     * Returns .....
     *
     * PUT /users/{userId}/events/
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic API information
     */
    public function createAction(
        Request $request
    ) {
        $event = $this->execOrAbort(
            $request,
            function ($params) use ($request) {
                return $this->app['repositories.events']->create($params);
            }
        );

        return $this->app->json(
            $event,
            201
        );
    }

    /**
     * Returns .....
     *
     * POST /users/{userId}/events/{eventId}
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic API information
     */
    public function editAction(
        Request $request,
        $eventId
    ) {
        return $this->execToJsonOrAbort(
            $request,
            function ($params) use ($request, $eventId) {
                $event = $this->app['repositories.events']->save($eventId, $params);

                if (empty($event) || (!empty($event['status']) && in_array($event['status'], static::$STATUS_CHOICES_404))) {
                    return $this->app->abort(
                        404,
                        'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo()
                    );
                }

                return $event;
            }
        );
    }

    /**
     * Returns .....
     *
     * GET /users/{userId}/events/{eventId}
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic API information
     */
    public function showAction(
        Request $request,
        $eventId
    ) {
        return $this->execToJsonOrAbort(
            $request,
            function ($params) use ($request, $eventId) {
                $event = $this->app['repositories.events']->findByIdWith(
                    $eventId,
                    $this->getRelationshipPublicFields('user'),
                    $this->getPublicFields(['status'])
                );

                if (empty($event) || empty($event['status']) || (!empty($event['status']) && in_array($event['status'], static::$STATUS_CHOICES_404))) {
                    return $this->app->abort(
                        404,
                        'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo()
                    );
                }
                unset($event['status']);

                return $event;
            }
        );
    }

    /**
     * Returns every events of an user
     *
     * GET /users/{userId}/events/{eventId}
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic API information
     */
    public function listAction(
        Request $request,
        $userId,
        $offset
    ) {
        return $this->execToJsonOrAbort(
            $request,
            function ($params) use ($request, $userId, $offset) {
                $events = $this->app['repositories.events']->runPaginatedSearch(
                    [
                        'where'   => [
                            ['user_id' => $userId],
                            ['status'  => \Validator\Events\StatusChoice::VALID]
                        ],
                        'orderBy' => ['start_at', 'desc']
                    ],
                    [
                        'limit'    => self::DEFAULT_USER_LIST_LIMIT,
                        'offset'   => $offset
                    ],
                    $this->getPublicFields()
                );

                return $events;
            }
        );
    }

    /**
     * Returns .....
     *
     * DELETE /users/{userId}/events/{eventId}
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic API information
     */
    public function removeAction(
        Request $request,
        $eventId
    ) {
        $res = $this->execOrAbort(
            $request,
            function ($params) use ($request, $eventId) {
                $res = $this->app['repositories.events']->changeStatus(
                    $eventId,
                    ['new_status' => \Validator\Events\StatusChoice::DELETED]
                );

                return [
                    'ok'     => $res['ok'],
                    'from'   => $eventId,
                    'ts'     => $res['ts'],
                ];
            }
        );

        return $this->app->json(
            $res,
            202
        );
    }
}
