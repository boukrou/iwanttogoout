<?php

namespace Controller\Api;

use Symfony\Component\HttpFoundation\Request;

use Controller\ControllerActionAbstract;
use Validator;

/**
 * UserController action controller for user management
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class UsersController extends ControllerActionAbstract
{
    protected static $PUBLIC_FIELDS = [
        'id',
        'username',
        'email',
        'description',
        'picture'
    ];

    protected static $STATUS_CHOICES_404 = [
        Validator\Users\StatusChoice::DELETED_PENDING,
        Validator\Users\StatusChoice::DELETED,
        Validator\Users\StatusChoice::MODERATED
    ];

    /**
     * Returns .....
     *
     * PUT /api/users/
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic API information
     */
    public function createAction(
        Request $request
    ) {
        $user = $this->execOrAbort(
            $request,
            function ($params) use ($request) {
                return $this->app['repositories.users']->create($params);
            }
        );

        return $this->app->json(
            $user,
            201
        );
    }

    /**
     * Returns .....
     *
     * POST /api/users/{userId}
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic API information
     */
    public function editAction(
        Request $request,
        $userId
    ) {
        return $this->execToJsonOrAbort(
            $request,
            function ($params) use ($request, $userId) {
                $user = $this->app['repositories.users']->save($userId, $params);

                if (empty($user) || (!empty($user['status']) && in_array($user['status'], static::$STATUS_CHOICES_404))) {
                    return $this->app->abort(
                        404,
                        'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo()
                    );
                }

                return $user;
            }
        );
    }

    /**
     * Returns .....
     *
     * GET /api/users/{userId}
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic API information
     */
    public function showAction(
        Request $request,
        $userId
    ) {
        return $this->execToJsonOrAbort(
            $request,
            function ($params) use ($request, $userId) {
                $user = $this->app['repositories.users']->findById(
                    $userId,
                    $this->getPublicFields(['status'])
                );

                if (empty($user) || (!empty($user['status']) && in_array($user['status'], static::$STATUS_CHOICES_404))) {
                    return $this->app->abort(
                        404,
                        'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo()
                    );
                }
                unset($user['status']);

                return $user;
            }
        );
    }

    /**
     * Returns .....
     *
     * DELETE /api/users/{userId}
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic API information
     */
    public function removeAction(
        Request $request,
        $userId
    ) {
        $res = $this->execOrAbort(
            $request,
            function ($params) use ($request, $userId) {
                $res = $this->app['repositories.users']->changeStatus(
                    $userId,
                    ['new_status' => \Validator\Users\StatusChoice::DELETED]
                );

                return [
                    'ok'     => $res['ok'],
                    'from'   => $userId,
                    'ts'     => $res['ts'],
                ];
            }
        );

        return $this->app->json(
            $res,
            202
        );
    }
}
