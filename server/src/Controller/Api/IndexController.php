<?php

namespace Controller\Api;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Controller\ControllerActionAbstract;

/**
 * IndexController main action controller
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class IndexController extends ControllerActionAbstract
{
    /**
     * Returns basic information about the Api
     *
     * GET /
     *
     * @param Request $request Incoming request object
     *
     * @return array Basic Api information
     */
    public function indexAction(Request $request)
    {
        return $this->app->json(
            'Welcome',
            200
        );
    }
}
