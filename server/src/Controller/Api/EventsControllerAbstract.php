<?php

namespace Controller\Api;

use Controller\ControllerActionAbstract;
use Validator;

/**
 * Abstract Event controller
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class EventsControllerAbstract extends ControllerActionAbstract
{
    protected static $PUBLIC_FIELDS = [
        'events.id',
        'events.title',
        'events.user_id',
        'events.description',
        'events.start_at',
        'events.end_at',
        'events.event_type',
        'events.team_id',
    ];

    protected static $RELATIONSHIP_PUBLIC_FIELDS = [
        'user' => [
            'id',
            'team_id',
            'username',
            'picture',
            'description',
        ]
    ];

    protected static $PUBLIC_RAW_FIELDS = [
        'AsText(location) as location'
    ];

    protected static $STATUS_CHOICES_404 = [
        Validator\Events\StatusChoice::DELETED_PENDING,
        Validator\Events\StatusChoice::DELETED,
        Validator\Events\StatusChoice::MODERATED
    ];
}
