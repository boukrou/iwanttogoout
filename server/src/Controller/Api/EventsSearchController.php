<?php

namespace Controller\Api;

use Controller\ControllerActionAbstract;
use Symfony\Component\HttpFoundation\Request;

/**
 * EventSearchController action controller for event searching
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class EventsSearchController extends EventsControllerAbstract
{
    /**
     * @const DEFAULT_NEAR_RADIUS_METERS Default radius used in the near search (meters)
     */
    const DEFAULT_NEAR_RADIUS_METERS = 3000;

    /**
     * @const DEFAULT_NEAR_TARGET Default target number of results in the near search
     */
    const DEFAULT_NEAR_TARGET = 100;

    /**
     * @const DEFAULT_NEAR_MAX_DEPTH Default maximum depth of the near search
     */
    const DEFAULT_NEAR_MAX_DEPTH = 5;

    /**
     * Fetch paginated list of events near a location
     *
     * GET /events/near/{latitude}/{longitude}/{offset}
     *
     * @param Request $request  Incoming request object
     * @param float $latitude   Latitude
     * @param float $longitude  Longitude
     * @param int $offset       Offset
     *
     * @return array
     */
    public function nearAction(
        Request $request,
        $latitude,
        $longitude,
        $offset
    ) {
        return $this->execToJsonOrAbort(
            $request,
            function ($params) use ($request, $latitude, $longitude, $offset) {
                $results             = [];
                $enoughResults       = false;
                $distanceInKm        = self::DEFAULT_NEAR_RADIUS_METERS / 1000;
                $currentRadiusFactor = 1;

                $params['events.status'] = \Validator\Events\StatusChoice::VALID;
                if (!empty($params)) {
                    $where[] = function ($query) use ($params) {
                        foreach ($params as $param => $paramValue) {
                            $query->whereIn($param, explode(',', $paramValue));
                        }
                        $query->where('start_at', '>=', date('Y-m-d H:i:s', time() - 1800));
                    };
                }

                while (!$enoughResults) {
                    $distance  = $distanceInKm * $currentRadiusFactor;
                    $lon1      = $longitude + ($distance/abs(cos(deg2rad($latitude))*111));
                    $lat1      = $latitude + ($distance/111);
                    $lon2      = $longitude - ($distance/abs(cos(deg2rad($latitude))*111));
                    $lat2      = $latitude - ($distance/111);

                    $params    = [
                        'leftJoin'  => ['events_votes', 'events.id', '=', 'events_votes.event_id'],
                        'with'      => [$this->getRelationshipPublicFields('user')],
                        'whereRaw'  => ['MBRContains(envelope(linestring(point(('.$lat1.'), ('.$lon1.')), point(('.$lat2.'), ('.$lon2.')))), location)'],
                        'orderBy'   => ['distance_in_meters', 'asc'],
                        'groupBy'   => ['events_votes.event_id'],
                    ];

                    if (!empty($where)) {
                        $params['where'] = $where;
                    }

                    $results = $this->app['repositories.events']->runPaginatedSearch(
                        $params,
                        [
                            'limit'    => self::DEFAULT_NEAR_TARGET,
                            'offset'   => $offset
                        ],
                        $this->getPublicFields([
                            new \Illuminate\Database\Query\Expression('COUNT(events_votes.id) as votes_total'),
                            new \Illuminate\Database\Query\Expression('slc( '.$latitude.', '.$longitude.', x(location), y(location))*1000 as distance_in_meters'),
                            new \Illuminate\Database\Query\Expression('start_at BETWEEN DATE(DATE_SUB(NOW(), INTERVAL 30 MINUTE)) AND timestamp(NOW()) as has_started')
                        ])
                    );

                    if (count($results['data']) >= self::DEFAULT_NEAR_TARGET) {
                        $enoughResults = true;
                    } else {
                        $currentRadiusFactor++;

                        if ($currentRadiusFactor > self::DEFAULT_NEAR_MAX_DEPTH) {
                            $enoughResults = true;
                        }
                    }
                }

                return $results;
            }
        );
    }

    /**
     * Fetch paginated list of events sorted by start_at
     *
     * GET /events/recent/{offset}
     *
     * @param Request $request  Incoming request object
     * @param int $offset       Offset
     *
     * @return array
     */
    public function recentAction(
        Request $request,
        $latitude,
        $longitude,
        $offset
    ) {
        return $this->execToJsonOrAbort(
            $request,
            function ($params) use ($request, $latitude, $longitude, $offset) {
                $results             = [];
                $enoughResults       = false;
                $distanceInKm        = self::DEFAULT_NEAR_RADIUS_METERS / 1000;
                $currentRadiusFactor = 1;

                $params['events.status'] = \Validator\Events\StatusChoice::VALID;
                if (!empty($params)) {
                    $where[] = function ($query) use ($params) {
                        foreach ($params as $param => $paramValue) {
                            $query->whereIn($param, explode(',', $paramValue));
                        }
                        $query->where('events.start_at', '>=', date('Y-m-d H:i:s', time() - 1800));
                    };
                }

                while (!$enoughResults) {
                    $distance  = $distanceInKm * $currentRadiusFactor;
                    $lon1      = $longitude + ($distance/abs(cos(deg2rad($latitude))*111));
                    $lat1      = $latitude + ($distance/111);
                    $lon2      = $longitude - ($distance/abs(cos(deg2rad($latitude))*111));
                    $lat2      = $latitude - ($distance/111);

                    $params    = [
                        'leftJoin'  => ['events_votes', 'events.id', '=', 'events_votes.event_id'],
                        'with'      => [$this->getRelationshipPublicFields('user')],
                        'whereRaw'  => ['MBRContains(envelope(linestring(point(('.$lat1.'), ('.$lon1.')), point(('.$lat2.'), ('.$lon2.')))), location)'],
                        'groupBy'   => ['events_votes.event_id'],
                        'orderBy'   => ['events.start_at', 'asc'],
                    ];

                    if (!empty($where)) {
                        $params['where'] = $where;
                    }

                    $results = $this->app['repositories.events']->runPaginatedSearch(
                        $params,
                        [
                            'limit'    => self::DEFAULT_NEAR_TARGET,
                            'offset'   => $offset
                        ],
                        $this->getPublicFields([
                            new \Illuminate\Database\Query\Expression('COUNT(events_votes.id) as votes_total'),
                            new \Illuminate\Database\Query\Expression('slc( '.$latitude.', '.$longitude.', x(location), y(location))*1000 as distance_in_meters'),
                            new \Illuminate\Database\Query\Expression('start_at BETWEEN DATE(DATE_SUB(NOW(), INTERVAL 30 MINUTE)) AND timestamp(NOW()) as has_started')
                        ])
                    );

                    if (count($results['data']) >= self::DEFAULT_NEAR_TARGET) {
                        $enoughResults = true;
                    } else {
                        $currentRadiusFactor++;

                        if ($currentRadiusFactor > self::DEFAULT_NEAR_MAX_DEPTH) {
                            $enoughResults = true;
                        }
                    }
                }

                return $results;
            }
        );
    }

    /**
     * Fetch paginated list of events sorted by votes
     *
     * GET /events/popular/{offset}
     *
     * @param Request $request  Incoming request object
     * @param int $offset       Offset
     *
     * @return array
     */
    public function popularAction(
        Request $request,
        $latitude,
        $longitude,
        $offset
    ) {
        return $this->execToJsonOrAbort(
            $request,
            function ($params) use ($request, $latitude, $longitude, $offset) {
                $results             = [];
                $enoughResults       = false;
                $distanceInKm        = self::DEFAULT_NEAR_RADIUS_METERS / 1000;
                $currentRadiusFactor = 1;

                $params['events.status'] = \Validator\Events\StatusChoice::VALID;
                $params['events_votes.status'] = \Validator\Events\StatusChoice::VALID;
                if (!empty($params)) {
                    $where[] = function ($query) use ($params) {
                        foreach ($params as $param => $paramValue) {
                            $query->whereIn($param, explode(',', $paramValue));
                        }
                        $query->where('start_at', '>=', date('Y-m-d H:i:s', time() - 1800));
                    };
                }

                while (!$enoughResults) {
                    $distance  = $distanceInKm * $currentRadiusFactor;
                    $lon1      = $longitude + ($distance/abs(cos(deg2rad($latitude))*111));
                    $lat1      = $latitude + ($distance/111);
                    $lon2      = $longitude - ($distance/abs(cos(deg2rad($latitude))*111));
                    $lat2      = $latitude - ($distance/111);

                    $params    = [
                        'leftJoin'  => ['events_votes', 'events.id', '=', 'events_votes.event_id'],
                        'with'      => [$this->getRelationshipPublicFields('user')],
                        'whereRaw'  => ['MBRContains(envelope(linestring(point(('.$lat1.'), ('.$lon1.')), point(('.$lat2.'), ('.$lon2.')))), location)'],
                        'groupBy'   => ['events_votes.event_id'],
                        'orderBy'   => ['votes_total', 'desc'],
                    ];

                    if (!empty($where)) {
                        $params['where'] = $where;
                    }

                    $results = $this->app['repositories.events']->runPaginatedSearch(
                        $params,
                        [
                            'limit'    => self::DEFAULT_NEAR_TARGET,
                            'offset'   => $offset
                        ],
                        $this->getPublicFields([
                            new \Illuminate\Database\Query\Expression('COUNT(events_votes.id) as votes_total'),
                            new \Illuminate\Database\Query\Expression('slc( '.$latitude.', '.$longitude.', x(location), y(location))*1000 as distance_in_meters'),
                            new \Illuminate\Database\Query\Expression('start_at BETWEEN DATE(DATE_SUB(NOW(), INTERVAL 30 MINUTE)) AND timestamp(NOW()) as has_started')
                        ])
                    );

                    if (count($results['data']) >= self::DEFAULT_NEAR_TARGET) {
                        $enoughResults = true;
                    } else {
                        $currentRadiusFactor++;

                        if ($currentRadiusFactor > self::DEFAULT_NEAR_MAX_DEPTH) {
                            $enoughResults = true;
                        }
                    }
                }

                return $results;
            }
        );
    }
}
