<?php

namespace Controller;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use HttpKernel\Exception as HttpException;
use Repository\Exception as RepositoryException;

/**
 * This a main abstract class for action controllers
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
abstract class ControllerActionAbstract
{
    /**
     * @var $app Silex application container instance
     **/
    protected $app;

    /**
     * Constructor
     *
     * @param Silex\Application $app silex application container instance instance
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public static function getPublicFields(array $extraFields = [], array $extraRawFields = [])
    {
        $f = static::$PUBLIC_FIELDS;

        if (!empty(static::$PUBLIC_RAW_FIELDS)) {
            foreach (static::$PUBLIC_RAW_FIELDS as $raw) {
                $f[] = new \Illuminate\Database\Query\Expression($raw);
            }
        }

        foreach ($extraFields as $extra) {
            $f[] = $extra;
        }

        foreach ($extraRawFields as $extraRaw) {
            $f[] = new \Illuminate\Database\Query\Expression($extraRaw);
        }

        return $f;
    }

    public static function getRelationshipPublicFields($relationship, array $extraFields = [], array $extraRawFields = [])
    {
        if (empty(static::$RELATIONSHIP_PUBLIC_FIELDS[$relationship])) {
            return [];
        }

        $f = static::$RELATIONSHIP_PUBLIC_FIELDS[$relationship];

        if (!empty(static::$RELATIONSHIP_PUBLIC_RAW_FIELDS)) {
            foreach (static::$RELATIONSHIP_PUBLIC_RAW_FIELDS as $raw) {
                $f[] = new \Illuminate\Database\Query\Expression($raw);
            }
        }

        foreach ($extraFields as $extra) {
            $f[] = $extra;
        }

        foreach ($extraRawFields as $extraRaw) {
            $f[] = new \Illuminate\Database\Query\Expression($extraRaw);
        }

        return [
            $relationship => function($query) use ($f) {
                $query->select($f);
            }
        ];
    }

    public function execToJsonOrAbort(
        Request $request,
        $callable
    ) {
        return $this->app->json(
            $this->execOrAbort(
                $request,
                $callable
            )
        );
    }

    public function execOrAbort(
        Request $request,
        $callable
    ) {
        try {
            if (!is_callable($callable)) {
                throw new Exception(
                    sprintf(
                        '%s:%s called with no callable parameters',
                        __CLASS__,
                        __METHOD__
                    )
                );
            }

            return $callable(
                $request->getMethod() == 'GET' ? $request->query->all() : $request->request->all()
            );
        } catch (RepositoryException\InvalidDataException $e) {
            return $this->app->abort(
                400,
                $e->getMessage(),
                $e->getErrors()
            );
        } catch (RepositoryException\NotFoundException $e) {
            return $this->app->abort(
                404,
                'Not found ' . $request->getMethod() . ' ' . $request->getPathInfo()
            );
        } catch (RepositoryException\NotYetImplementedException $e) {
            return $this->app->abort(
                501,
                'Not Yet Implemented'
            );
        } catch (RepositoryException\ServerInternalException $e) {
            return $this->app->abort(
                500,
                $e->getMessage()
            );
        } catch (RepositoryException\DuplicateKeyException $e) {
            return $this->app->abort(
                403,
                $e->getMessage()
            );
        } catch (HttpException\HttpException $e) {
            throw $e;
        } catch (Exception $e) {
            return $this->app->abort(
                500,
                $e->getMessage()
            );
        }
    }
}
