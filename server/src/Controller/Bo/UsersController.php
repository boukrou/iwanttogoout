<?php

namespace Controller\Bo;

use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

use Controller\ControllerActionAbstract;
use Repository\Exception as RepositoryException;
use Validator;

/**
 * UserController main action controller
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class UsersController extends ControllerActionAbstract
{
    protected static $COUNT_PER_LIST = 50;

    public static $PUBLIC_FIELDS = [
        'id',
        'username',
        'team_id',
        'email',
        'description',
    ];

    /**
     * Display users list
     *
     * GET /users
     *
     * @param Request $request Incoming request object
     *
     * @return object Template rendering
     */
    public function listAction(
        Request $request
    ) {
        $offset = $request->get('offset') ?: 1;

        $users = $this->app['repositories.users']->find(
            [
                'take' => [static::$COUNT_PER_LIST + 1],
                'offset' => [($offset - 1) * static::$COUNT_PER_LIST],
                'orderBy' => ['id', 'asc']
            ],
            $this->getPublicFields(['status'])
        );

        if ($offset > 1 && empty($users)) {
            return $this->app->redirect(
                $this->app->url('bo_users_list', ['offset' => 1])
            );
        }

        $next = false;
        if (count($users) > static::$COUNT_PER_LIST) {
            $next = true;
            $users->pop();
        }

        $pager = [
            'next'     => $next,
            'previous' => $offset > 1,
            'offset'   => $offset
        ];

        return $this->app->render(
            'users/list.twig', [
                'teams'   => \Validator\Users\TeamChoice::getChoicesAndLabels(),
                'acl'     => \Validator\Users\BoAclTypeChoice::getChoicesAndLabels(),
                'reasons' => \Validator\Events\StatusChoice::getChoicesAndLabels(),
                'users'   => $users,
                'pager'   => $pager
            ]
        );
    }

    /**
     * Create user
     *
     * GET /users/new
     * PUT /users -> redirect list
     **/
    public function createAction(
        Request $request
    ) {
        $form = $this->formCreate();

        if ('PUT' === $request->getMethod()) {

            $form->bind(
                $request
            );

            if ($form->isValid()) {
                $data = $form->getData();
                try {
                    $user = $this->app['repositories.users']->create(
                        $data
                    );

                    $this->app['session']
                        ->getFlashBag()
                        ->add(
                            'success',
                            sprintf(
                                'User <a href="%s" class="alert-link">%s</a> is created.',
                                $this->app->url(
                                    'bo_users_get', [
                                        'resourceId' => $user->id
                                    ]
                                ),
                                $user['username']
                            )
                        );

                    return $this->app->redirect(
                        $this->app->url('bo_users_list')
                    );

                } catch (RepositoryException\InvalidDataException $e) {
                    $errors = $e->getErrors();
                    $form->addError(
                        new FormError($e->getMessage())
                    );
                    foreach ($errors as $k => $v) {
                        try {
                            switch ($k) {
                                case '[password]':
                                    $form->get('password')->get('first')
                                        ->addError(
                                            new FormError($v)
                                        );
                                    break;
                                default:
                                    $formField = str_replace(
                                        ']',
                                        '',
                                        str_replace(
                                            '[',
                                            '',
                                            $k
                                        )
                                    );
                                    $formEl = $form->get($formField);
                                    $formEl->addError(new FormError($v));
                                    break;
                            }
                        } catch (\Exception $ee) {
                            //ingnored exception
                        }
                    }
                } catch (RepositoryException\DuplicateKeyException $e) {
                    $form
                        ->get('email')
                        ->addError(
                            new FormError('An user with this email already exist.')
                        );
                } catch (\Exception $e) {
                    $form->addError(
                        new FormError('An unknow error occured'. $e->getMessage())
                    );
                }
            }
        }

        return $this->app->render(
            'users/form.twig', [
                'form' => [
                    'view' => $form->createView(),
                    'action' => $this->app->url(
                        'bo_users_create'
                    ),
                    'method' => 'PUT'
                ]
            ]
        );
    }

    /**
     * Get user
     *
     * GET /users/{resourceId}
     *
     * @param Request $request      Incoming request object
     * @param Int     $resourceId   User Id
     *
     * @return object Template rendering
     */
    public function getAction(
        Request $request,
        $resourceId
    ) {
        $user = $this->app['repositories.users']->findById(
            $resourceId,
            $this->getPublicFields(['status', 'created_at', 'updated_at', 'activity_at', 'acl'])
        );

        if (empty($user)) {
            $this->app['session']
                ->getFlashBag()
                ->add(
                    'error',
                    'User not found.'
                );

            return $this->app->redirect(
                $this->app->url('bo_users_list')
            );
        }

        return $this->app->render(
            'users/get.twig', [
                'teams'   => \Validator\Users\TeamChoice::getChoicesAndLabels(),
                'acl'     => \Validator\Users\BoAclTypeChoice::getChoicesAndLabels(),
                'reasons' => \Validator\Users\StatusChoice::getChoicesAndLabels(),
                'user'    => $user
            ]
        );
    }

    /**
     * Update user
     *
     * GET  /users/{resourceId}
     * POST /users/{resourceId}
     *
     * @param Request $request      Incoming request object
     * @param Int     $resourceId   User Id
     *
     * @return redirect user listing page
     */
    public function updateAction(
        Request $request,
        $resourceId
    ) {
        $user = $this->app['repositories.users']->findById(
            $resourceId,
            $this->getPublicFields(['status', 'acl'])
        );
        if (empty($user)) {
            $this->app['session']
                ->getFlashBag()
                ->add(
                    'error',
                    'User not found.'
                );
            return $this->app->redirect($this->app->url('bo_users_list'));
        }

        $formStatus = $this->formStatus(
            $user->toArray()
        );
        unset($user->status);

        $formAcl = $this->formAcl(
            $user->toArray()
        );
        unset($user->acl);

        $form = $this->formUpdate(
            $user->toArray()
        );

        if ('POST' === $request->getMethod()) {
            $form->bind($request);

            if ($form->isValid()) {
                $data = $form->getData();

                unset($data['id']);
                if (empty($data['password'])) {
                    unset($data['password']);
                }

                $picture = $data['picture'];
                if (!empty($picture)) {
                    $data['picture'] = $picture->getPathname();
                }

                try {
                    $user = $this->app['repositories.users']->save(
                        $resourceId,
                        $data
                    );

                    if (!empty($user)) {
                        $this->app['session']
                            ->getFlashBag()
                            ->add(
                                'success',
                                sprintf(
                                    'User <a href="%s" class="alert-link">#%s %s</a> has been updated.',
                                    $this->app->url(
                                        'bo_users_get', [
                                            'resourceId' => $user->id
                                        ]
                                    ),
                                    $user->id,
                                    $user->username
                                )
                            );

                        return $this->app->redirect(
                            $this->app->url('bo_users_list')
                        );
                    }
                } catch (RepositoryException\InvalidDataException $e) {
                    $errors = $e->getErrors();
                    $form->addError(
                        new FormError($e->getMessage())
                    );
                    foreach ($errors as $k => $v) {
                        try {
                            switch ($k) {
                                case '[password]':
                                    $form->get('password')->get('first')
                                        ->addError(
                                            new FormError($v)
                                        );
                                    break;
                                default:
                                    $formField = str_replace(
                                        ']',
                                        '',
                                        str_replace(
                                            '[',
                                            '',
                                            $k
                                        )
                                    );
                                    $formEl = $form->get($formField);
                                    $formEl->addError(new FormError($v));
                                    break;
                            }
                        } catch (\Exception $ee) {
                            //ingnored exception
                        }
                    }
                } catch (RepositoryException\DuplicateKeyException $e) {
                    $form
                        ->get('email')
                        ->addError(
                            new FormError('An user with this email already exist.')
                        );
                } catch (\Exception $e) {
                    $form->addError(
                        new FormError('An unknow error occured')
                    );
                }
            }
        }

        return $this->app->render(
            'users/form.twig', [
                'user' => $user,
                'form' => [
                    'view'   => $form->createView(),
                    'action' => $this->app->url(
                        'bo_users_update', [
                            'resourceId' => $resourceId
                        ]
                    ),
                    'method' => 'POST'
                ],
                'formStatus' => [
                    'view'   => $formStatus->createView(),
                    'action' => $this->app->url(
                        'bo_users_update_status', [
                            'resourceId' => $resourceId
                        ]
                    ),
                    'method' => 'POST'
                ],
                'formAcl' => [
                    'view'   => $formAcl->createView(),
                    'action' => $this->app->url(
                        'bo_users_update_acl', [
                            'resourceId' => $resourceId
                        ]
                    ),
                    'method' => 'POST'
                ],
            ]
        );
    }

    /**
     * Change an user status
     *
     * POST /users/{resourceId}/status
     *
     * @param Request $request      Incoming request object
     * @param Int     $resourceId   User Id
     *
     * @return redirect user listing page
     */
    public function changeStatusAction(
        Request $request,
        $resourceId
    ) {
        $user = $this->app['repositories.users']->findById(
            $resourceId
        )->toArray();

        if (empty($user)) {
            $this->app['session']
                ->getFlashBag()
                ->add(
                    'error',
                    'User not found.'
                );
        } else {
            $formStatus = $this->formStatus($user);
            $formStatus->bind($request);

            if ($formStatus->isValid()) {
                $data = $formStatus->getData();
                unset($data['id']);

                try {
                    $result = $this->app['repositories.users']->changeStatus(
                        $resourceId,
                        ['new_status' => $data['status']]
                    );

                    if (!empty($result) && !empty($result['ok'])) {
                        $this->app['session']
                            ->getFlashBag()
                            ->add(
                                'success',
                                sprintf(
                                    'User <a href="%s" class="alert-link">#%s %s</a> has been updated.',
                                    $this->app->url(
                                        'bo_users_get', [
                                            'resourceId' => $user['id']
                                        ]
                                    ),
                                    $user['id'],
                                    $user['username']
                                )
                            );

                        return $this->app->redirect(
                            $this->app->url('bo_users_list')
                        );
                    }
                } catch (RepositoryException\InvalidDataException $e) {
                    $errors = $e->getErrors();
                    $formStatus->addError(
                        new FormError($e->getMessage())
                    );
                    foreach ($errors as $k => $v) {
                        try {
                            switch ($k) {
                                default:
                                    $formField = str_replace(
                                        ']',
                                        '',
                                        str_replace(
                                            '[',
                                            '',
                                            $k
                                        )
                                    );
                                    $formEl = $form->get($formField);
                                    $formEl->addError(new FormError($v));
                                    break;
                            }
                        } catch (\Exception $ee) {
                            //ingnored exception
                        }
                    }
                } catch (\Exception $e) {
                    $form->addError(
                        new FormError('An unknow error occured')
                    );
                }
            }
        }

        return $this->app->redirect($this->app->url('bo_users_list'));
    }

    /**
     * Change an user status
     *
     * POST /users/{resourceId}/status
     *
     * @param Request $request      Incoming request object
     * @param Int     $resourceId   User Id
     *
     * @return redirect user listing page
     */
    public function changeAclAction(
        Request $request,
        $resourceId
    ) {
        $user = $this->app['repositories.users']->findById(
            $resourceId
        )->toArray();

        if (empty($user)) {
            $this->app['session']
                ->getFlashBag()
                ->add(
                    'error',
                    'User not found.'
                );
        } else {
            $formAcl = $this->formAcl($user);
            $formAcl->bind($request);

            if ($formAcl->isValid()) {
                $data = $formAcl->getData();
                unset($data['id']);

                try {
                    $result = $this->app['repositories.users']->changeAcl(
                        $resourceId,
                        ['new_acl' => $data['acl']]
                    );

                    if (!empty($result) && !empty($result['ok'])) {
                        $this->app['session']
                            ->getFlashBag()
                            ->add(
                                'success',
                                sprintf(
                                    'User <a href="%s" class="alert-link">#%s %s</a> has been updated.',
                                    $this->app->url(
                                        'bo_users_get', [
                                            'resourceId' => $user['id']
                                        ]
                                    ),
                                    $user['id'],
                                    $user['username']
                                )
                            );

                        return $this->app->redirect(
                            $this->app->url('bo_users_list')
                        );
                    }
                } catch (RepositoryException\InvalidDataException $e) {
                    $errors = $e->getErrors();
                    $formAcl->addError(
                        new FormError($e->getMessage())
                    );
                    foreach ($errors as $k => $v) {
                        try {
                            switch ($k) {
                                default:
                                    $formField = str_replace(
                                        ']',
                                        '',
                                        str_replace(
                                            '[',
                                            '',
                                            $k
                                        )
                                    );
                                    $formEl = $form->get($formField);
                                    $formEl->addError(new FormError($v));
                                    break;
                            }
                        } catch (\Exception $ee) {
                            //ingnored exception
                        }
                    }
                } catch (\Exception $e) {
                    $form->addError(
                        new FormError('An unknow error occured')
                    );
                }
            }
        }

        return $this->app->redirect($this->app->url('bo_users_list'));
    }

    /**
     * Delete an user
     *
     * DELETE /users/{resourceId}
     *
     * @param Request $request      Incoming request object
     * @param Int     $resourceId   User Id
     *
     * @return redirect user listing page
     */
    public function deleteAction(
        Request $request,
        $resourceId
    ) {
        $user = $this->app['repositories.users']->findById(
            $resourceId
        );

        if (empty($user)) {
            $this->app['session']
                ->getFlashBag()
                ->add(
                    'error',
                    'User not found.'
                );
        } else {
            $result = $this->app['repositories.users']->changeStatus(
                $resourceId,
                ['new_status' => \Validator\Users\StatusChoice::DELETED]
            );

            if (!empty($result) && !empty($result['ok'])) {
                $this->app['session']
                    ->getFlashBag()
                    ->add(
                        'success',
                        sprintf(
                            'User #%s %s has been removed',
                            $resourceId,
                            $user['username']
                        )
                    );
            } else {
                $this->app['session']
                    ->getFlashBag()
                    ->add(
                        'error',
                        sprintf(
                            'User #%s %s can\'t be removed',
                            $resourceId,
                            $user['username']
                        )
                    );
            }
        }

        return $this->app->redirect($this->app->url('bo_users_list'));
    }

    protected function formCreate(
        array $data = [],
        array $options = []
    ) {
        return $this->app->form(
            $data,
            $options
        )->add(
            'email',
            'text',
            [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Email()
                ]
            ]
        )->add(
            'password',
            'repeated',
            [
                'required'        => false,
                'type'            => 'password',
                'invalid_message' => 'Password are not identical',
                'first_options'   => [
                    'label'       => 'Password',
                    'constraints' => [
                        new Validator\Users\Password()
                    ]
                ],
                'second_options'  => [
                    'label' => 'Confirm Password'
                ],
            ]
        )->add(
            'team_id',
            'choice',
            [
                'choices'    => Validator\Users\TeamChoice::getChoicesAndLabels(),
                'constraints' => [
                    new Validator\Users\TeamChoice()
                ]
            ]
        )->getForm();
    }

    protected function formUpdate(
        array $data = [],
        array $options = []
    ) {
        return $this->app->form(
            $data,
            $options
        )->add(
            'picture',
            'file',
            [
                'attr'  => [
                    'accept' => 'image/jpeg'
                ],
                'required' => false,
            ]
        )->add(
            'username',
            'text',
            [
                'required'    => false,
                'constraints' => [
                    new Validator\Users\Username()
                ]
            ]
        )->add(
            'email',
            'text',
            [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Email()
                ]
            ]
        )->add(
            'password',
            'repeated',
            [
                'required'        => false,
                'type'            => 'password',
                'invalid_message' => 'Password are not identical',
                'first_options'   => [
                    'label'       => 'Password',
                    'constraints' => [
                        new Validator\Users\Password()
                    ]
                ],
                'second_options'  => [
                    'label' => 'Confirm Password'
                ],
            ]
        )->add(
            'description',
            'textarea',
            [
                'required'    => false,
                'constraints' => [
                    new Validator\Text()
                ]
            ]
        )->add(
            'team_id',
            'choice',
            [
                'choices'    => Validator\Users\TeamChoice::getChoicesAndLabels(),
                'constraints' => [
                    new Validator\Users\TeamChoice()
                ]
            ]
        )->getForm();
    }

    protected function formStatus(
        array $data = [],
        array $options = []
    ) {
        return $this->app->form(
            $data,
            $options
        )->add(
            'status',
            'choice',
            [
                'choices' => Validator\Users\StatusChoice::getChoicesAndLabels(),
                'constraints' => [
                    new Validator\Users\StatusChoice()
                ]
            ]
        )->getForm();
    }

    protected function formAcl(
        array $data = [],
        array $options = []
    ) {
        return $this->app->form(
            $data,
            $options
        )->add(
            'acl',
            'choice',
            [
                'choices' => Validator\Users\BoAclTypeChoice::getChoicesAndLabels(),
                'constraints' => [
                    new Validator\Users\BoAclTypeChoice()
                ]
            ]
        )->getForm();
    }
}
