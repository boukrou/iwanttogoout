<?php

namespace Controller\Bo;

use Illuminate\Database\Query\Expression;
use Symfony\Component\HttpFoundation\Request;

use Controller\ControllerActionAbstract;

/**
 * DashboardController main action controller
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class DashboardController extends ControllerActionAbstract
{
    const DATE_FORMAT = '%Y-%m';
    /**
     * Display bo dashboard
     *
     * GET /
     *
     * @param Request $request Incoming request object
     *
     * @return object Template rendering
     */
    public function indexAction(Request $request)
    {
        $teamsLabels = \Validator\Users\TeamChoice::getChoicesAndLabels();

        /* Teams Donut chart */
        $usersByTeam = [];
        $users       = $this->app['repositories.users']->all(['team_id', new Expression('count(id) as total')], 'team_id');
        foreach ($users as $u) {
            $usersByTeam[$u->team_id] = [
                'label' => $teamsLabels[$u->team_id],
                'count' => $u->total
            ];
        }

        /* Events / users bar chart */
        $eventsByTeam = [];
        $events       = $this->app['repositories.events']->all(['team_id', new Expression('count(id) as total')], 'team_id');
        foreach ($events as $e) {
            $eventsByTeam[$e->team_id] = [
                'label' => $teamsLabels[$e->team_id],
                'usersCount'  => $usersByTeam[$e->team_id]['count'],
                'eventsCount' => $e->total
            ];
        }


        /* events / users / validated creations graph chart */
        $events = $this->app['repositories.events']->find(
            [
                'groupBy' => [
                    new Expression('DATE_FORMAT(created_at, \''.self::DATE_FORMAT.'\')'),
                    'team_id',
                    'status'
                ],
            ],
            [
                'team_id',
                'status',
                new Expression('DATE_FORMAT(created_at, \''.self::DATE_FORMAT.'\') as created_at'),
                new Expression('count(id) as total')
            ]
        );
        $users = $this->app['repositories.users']->find(
            [
                'groupBy' => [
                    new Expression('DATE_FORMAT(created_at, \''.self::DATE_FORMAT.'\')'),
                    'team_id',
                    'status'
                ],
            ],
            [
                'team_id',
                'status',
                new Expression('DATE_FORMAT(created_at, \''.self::DATE_FORMAT.'\') as created_at'),
                new Expression('count(id) as total')
            ]
        );

        $creations = [];
        foreach ($events as $event) {
            if (empty($creations[$event->created_at])) {
                foreach (\Validator\Users\TeamChoice::getChoices() as $team) {
                    $creations[$event->created_at]['events_team_'.$team] = 0;
                    $creations[$event->created_at]['users_team_'.$team] = 0;
                }
                $creations[$event->created_at]['events_total'] = 0;
                $creations[$event->created_at]['users_total'] = 0;
                $creations[$event->created_at]['events_validated'] = 0;
                $creations[$event->created_at]['users_validated'] = 0;
            }

            $creations[$event->created_at]['events_team_'.$event->team_id] += $event->total;
            $creations[$event->created_at]['events_total'] += $event->total;
            if ($event->status == \Validator\Events\StatusChoice::VALID) {
                $creations[$event->created_at]['events_validated'] += $event->total;
            }
        }
        foreach ($users as $user) {
            if (empty($creations[$user->created_at])) {
                foreach (\Validator\Users\TeamChoice::getChoices() as $team) {
                    $creations[$user->created_at]['users_team_'.$team] = 0;
                    $creations[$user->created_at]['events_team_'.$team] = 0;
                }
                $creations[$user->created_at]['users_total'] = 0;
                $creations[$user->created_at]['events_total'] = 0;
                $creations[$user->created_at]['users_validated'] = 0;
                $creations[$user->created_at]['events_validated'] = 0;
            }

            $creations[$user->created_at]['users_team_'.$user->team_id] += $user->total;
            $creations[$user->created_at]['users_total'] += $user->total;
            if ($user->status == \Validator\Users\StatusChoice::VALID) {
                $creations[$user->created_at]['users_validated'] += $user->total;
            }
        }

        $eventsToday = $this->app['repositories.events']->find(
            ['whereRaw' => ['DATE(created_at)=CURDATE()']],
            [new Expression('count(id) as total')]
        )->first()->total;
        $usersToday = $this->app['repositories.users']->find(
            ['whereRaw' => ['DATE(created_at)=CURDATE()']],
            [new Expression('count(id) as total')]
        )->first()->total;
        $activityToday = $this->app['repositories.users']->find(
            ['whereRaw' => ['DATE(activity_at)=CURDATE()']],
            [new Expression('count(id) as total')]
        )->first()->total;

        return $this->app->render(
            'dashboard/index.twig', [
                'eventsToday'   => $eventsToday,
                'usersToday'    => $usersToday,
                'activityToday' => $activityToday,
                'creations'     => $creations,
                'usersByTeam'   => $usersByTeam,
                'eventsByTeam'  => $eventsByTeam
            ]
        );
    }
}