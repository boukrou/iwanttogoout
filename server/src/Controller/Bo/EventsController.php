<?php

namespace Controller\Bo;

use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

use Controller\ControllerActionAbstract;
use Repository\Exception as RepositoryException;
use Validator;

/**
 * EventController main action controller
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class EventsController extends ControllerActionAbstract
{
    protected static $COUNT_PER_LIST = 50;

    protected static $PUBLIC_FIELDS = [
        'id',
        'title',
        'user_id',
        'description',
        'start_at',
        'end_at',
        'event_type',
        'team_id',
        'is_sponsored'
    ];

    protected static $PUBLIC_RAW_FIELDS = [
        'AsText(location) as location'
    ];

    /**
     * Display events list
     *
     * GET /events
     *
     * @param Request $request Incoming request object
     *
     * @return object Template rendering
     */
    public function listAction(
        Request $request
    ) {
        $offset = $request->get('offset') ?: 1;

        $events = $this->app['repositories.events']->find(
            [
                'with' => [['user']],
                'take' => [static::$COUNT_PER_LIST + 1],
                'offset' => [($offset - 1) * static::$COUNT_PER_LIST],
                'orderBy' => ['id', 'asc']
            ],
            $this->getPublicFields(['status'])
        );

        if ($offset > 1 && empty($events)) {
            return $this->app->redirect(
                $this->app->url('events_list', ['offset' => 1])
            );
        }

        $next = false;
        if (count($events) > static::$COUNT_PER_LIST) {
            $next = true;
            $events->pop();
        }

        $pager = [
            'next'     => $next,
            'previous' => $offset > 1,
            'offset'   => $offset
        ];

        return $this->app->render(
            'events/list.twig', [
                'eventtypes' => \Validator\Events\EventTypeChoice::getChoicesAndLabels(),
                'reasons'    => \Validator\Events\StatusChoice::getChoicesAndLabels(),
                'teams'      => \Validator\Users\TeamChoice::getChoicesAndLabels(),
                'events'     => $events->toArray(),
                'pager'      => $pager
            ]
        );
    }

    /**
     * Create event
     *
     * GET  /events/new
     * POST /events -> redirect list
     **/
    public function createAction(
        Request $request
    ) {
        $form = $this->form();

        if ('PUT' === $request->getMethod()) {

            $form->bind(
                $request
            );

            if ($form->isValid()) {
                $data               = $form->getData();
                $location           = $data['location'];
                $data['location']   = [];
                $data['user_id']    = (int)$data['user_id'];
                $data['event_type'] = (int)$data['event_type'];
                $data['start_at']   = strtotime($data['start_at']);
                $data['end_at']     = $data['end_at'] ? strtotime($data['end_at']) : null;

                if (!empty($location)) {
                    $l = explode(',', $location);
                    $data['location']['latitude'] = $l[0];
                    if (isset($l[1])) {
                        $data['location']['longitude'] = $l[1];
                    }
                }

                try {
                    $event = $this->app['repositories.events']->create(
                        $data
                    );

                    $this->app['session']
                        ->getFlashBag()
                        ->add(
                            'success',
                            sprintf(
                                'Event <a href="%s" class="alert-link"># %s %s</a> is created.',
                                $this->app->url(
                                    'bo_events_get', [
                                        'resourceId' => $event->id
                                    ]
                                ),
                                $event->id,
                                $event->title
                            )
                        );

                    return $this->app->redirect(
                        $this->app->url('bo_events_list')
                    );

                } catch (RepositoryException\InvalidDataException $e) {
                    $errors = $e->getErrors();

                    $form->addError(
                        new FormError($e->getMessage())
                    );
                    foreach ($errors as $k => $v) {
                        try {
                            switch ($k) {
                                case '[location]':
                                case '[location][latitude]':
                                case '[location][longitude]':
                                    $form->get('location')
                                        ->addError(
                                            new FormError($k.' '.$v)
                                        );
                                    break;
                                default:
                                    $formField = str_replace(
                                        ']',
                                        '',
                                        str_replace(
                                            '[',
                                            '',
                                            $k
                                        )
                                    );
                                    $formEl = $form->get($formField);
                                    $formEl->addError(new FormError($v));
                                    break;
                            }
                        } catch (\Exception $ee) {
                            //ingnored exception
                        }
                    }
                } catch (\Exception $e) {
                    $form->addError(
                        new FormError('An unknown error occured'. $e->getMessage())
                    );
                }
            }
        }

        return $this->app->render(
            'events/form.twig',
            array(
                'form' => array(
                    'view' => $form->createView(),
                    'action' => $this->app->url(
                        'bo_events_create'
                    ),
                    'method' => 'PUT'
                )
            )
        );
    }

    /**
     * Get event
     *
     * GET /events/{resourceId}
     *
     * @param Request $request      Incoming request object
     * @param Int     $resourceId   Event Id
     *
     * @return object Template rendering
     */
    public function getAction(
        Request $request,
        $resourceId
    ) {
        $event = $this->app['repositories.events']->findByIdWith(
            $resourceId,
            ['user'],
            $this->getPublicFields(['status', 'created_at', 'updated_at'])
        );

        if (empty($event)) {
            $this->app['session']
                ->getFlashBag()
                ->add(
                    'error',
                    'Event not found.'
                );

            return $this->app->redirect(
                $this->app->url('bo_events_list')
            );
        }

        return $this->app->render(
            'events/get.twig', [
                'eventtypes' => \Validator\Events\EventTypeChoice::getChoicesAndLabels(),
                'reasons'    => \Validator\Events\StatusChoice::getChoicesAndLabels(),
                'teams'      => \Validator\Users\TeamChoice::getChoicesAndLabels(),
                'event'      => $event->toArray()
            ]
        );
    }

    /**
     * Update event
     *
     * GET  /events/{resourceId}
     * POST /events/{resourceId}
     *
     * @param Request $request      Incoming request object
     * @param Int     $resourceId   Event Id
     *
     * @return redirect user listing page
     */
    public function updateAction(
        Request $request,
        $resourceId
    ) {
        $event = $this->app['repositories.events']->findById(
            $resourceId,
            $this->getPublicFields(['status'])
        );

        if (empty($event)) {
            $this->app['session']
                ->getFlashBag()
                ->add(
                    'error',
                    'Event not found.'
                );
            return $this->app->redirect($this->app->url('bo_events_list'));
        }

        $formStatus = $this->formStatus(
            $event->toArray()
        );
        unset($event->status);

        $form = $this->form(
            $event->toArray()
        );

        if ('POST' === $request->getMethod()) {

            $form->bind($request);

            if ($form->isValid()) {
                $data = $form->getData();

                unset($data['id']);
                $location           = $data['location'];
                $data['location']   = [];
                $data['user_id']    = (int)$data['user_id'];
                $data['event_type'] = (int)$data['event_type'];
                $data['start_at']   = strtotime($data['start_at']);
                $data['end_at']     = $data['end_at'] ? strtotime($data['end_at']) : null;

                if (!empty($location)) {
                    $l = explode(',', $location);
                    $data['location']['latitude'] = $l[0];
                    if (isset($l[1])) {
                        $data['location']['longitude'] = $l[1];
                    }
                }

                try {
                    $event = $this->app['repositories.events']->save(
                        $resourceId,
                        $data,
                        [
                            'fields' => ['is_sponsored' => new Assert\Optional([new Assert\Type('bool')])]
                        ]
                    );

                    if (!empty($event)) {
                        $this->app['session']
                            ->getFlashBag()
                            ->add(
                                'success',
                                sprintf(
                                    'Event <a href="%s" class="alert-link">#%s %s</a> has been updated.',
                                    $this->app->url(
                                        'bo_events_get',
                                        array(
                                            'resourceId' => $event->id
                                        )
                                    ),
                                    $event->id,
                                    $event->title
                                )
                            );

                        return $this->app->redirect(
                            $this->app->url('bo_events_list')
                        );
                    }
                } catch (RepositoryException\InvalidDataException $e) {
                    $errors = $e->getErrors();
                    $form->addError(
                        new FormError($e->getMessage())
                    );

                    foreach ($errors as $k => $v) {
                        try {
                            switch ($k) {
                                case '[location]':
                                case '[location][latitude]':
                                case '[location][longitude]':
                                    $form->get('location')
                                        ->addError(
                                            new FormError($k.' '.$v)
                                        );
                                    break;
                                default:
                                    $formField = str_replace(
                                        ']',
                                        '',
                                        str_replace(
                                            '[',
                                            '',
                                            $k
                                        )
                                    );
                                    $formEl = $form->get($formField);
                                    $formEl->addError(new FormError($v));
                                    break;
                            }
                        } catch (\Exception $ee) {
                            //ingnored exception
                        }
                    }
                } catch (\Exception $e) {
                    $form->addError(
                        new FormError('An unknow error occured')
                    );
                }
            }
        }

        return $this->app->render(
            'events/form.twig', [
                'event' => $event,
                'form' => [
                    'view'   => $form->createView(),
                    'action' => $this->app->url(
                        'bo_events_update', [
                            'resourceId' => $resourceId
                        ]
                    ),
                    'method' => 'POST'
                ],
                'formStatus' => [
                    'view'   => $formStatus->createView(),
                    'action' => $this->app->url(
                        'bo_events_update_status', [
                            'resourceId' => $resourceId
                        ]
                    ),
                    'method' => 'POST'
                ]
            ]

        );
    }

    /**
     * Change an event status
     *
     * POST /events/{resourceId}/status
     *
     * @param Request $request      Incoming request object
     * @param Int     $resourceId   Event Id
     *
     * @return redirect event listing page
     */
    public function changeStatusAction(
        Request $request,
        $resourceId
    ) {
        $event = $this->app['repositories.events']->findById(
            $resourceId
        )->toArray();

        if (empty($event)) {
            $this->app['session']
                ->getFlashBag()
                ->add(
                    'error',
                    'Event not found.'
                );
        } else {

            $formStatus = $this->formStatus($event);
            $formStatus->bind($request);
            if ($formStatus->isValid()) {
                $data = $formStatus->getData();
                unset($data['id']);

                try {
                    $result = $this->app['repositories.events']->changeStatus(
                        $resourceId,
                        ['new_status' => $data['status']]
                    );

                    if (!empty($result) && !empty($result['ok'])) {
                        $this->app['session']
                            ->getFlashBag()
                            ->add(
                                'success',
                                sprintf(
                                    'Event <a href="%s" class="alert-link">#%s %s</a> has been updated.',
                                    $this->app->url(
                                        'bo_events_get', [
                                            'resourceId' => $event['id']
                                        ]
                                    ),
                                    $event['id'],
                                    $event['title']
                                )
                            );

                        return $this->app->redirect(
                            $this->app->url('bo_events_list')
                        );
                    }
                } catch (RepositoryException\InvalidDataException $e) {
                    $errors = $e->getErrors();
                    $formStatus->addError(
                        new FormError($e->getMessage())
                    );
                    foreach ($errors as $k => $v) {
                        try {
                            switch ($k) {
                                default:
                                    $formField = str_replace(
                                        ']',
                                        '',
                                        str_replace(
                                            '[',
                                            '',
                                            $k
                                        )
                                    );
                                    $formEl = $form->get($formField);
                                    $formEl->addError(new FormError($v));
                                    break;
                            }
                        } catch (\Exception $ee) {
                            //ingnored exception
                        }
                    }
                } catch (\Exception $e) {
                    $form->addError(
                        new FormError('An unknow error occured')
                    );
                }
            }
        }

        return $this->app->redirect($this->app->url('bo_events_list'));
    }

    /**
     * Delete an event
     *
     * DELETE /events/{eventId}
     *
     * @param Request $request Incoming request object
     * @param Int     $eventId Event Id
     *
     * @return redirect event listing page
     */
    public function deleteAction(
        Request $request,
        $resourceId
    ) {
        $res = $this->app['repositories.events']->changeStatus(
            $resourceId,
            ['new_status' => \Validator\Events\StatusChoice::DELETED]
        );

        return $this->app->redirect($this->app->url('bo_events_list'));
    }

    protected function form(
        array $data = [],
        array $options = []
    ) {
        if (!empty($data['location'])) {
            $data['location'] = implode(',', $data['location']);
        }

        return $this->app->form(
            $data,
            $options
        )->add(
            'title',
            'text',
            [
                'required'    => false,
                'constraints' => [
                    new Validator\Events\Title()
                ]
            ]
        )->add(
            'description',
            'textarea',
            [
                'required'    => false,
                'constraints' => [
                    new Validator\Text()
                ]
            ]
        )->add(
            'user_id',
            'text',
            [
                'constraints' => [
                    new Assert\NotBlank()
                ]
            ]
        )->add(
            'start_at',
            'text',
            [
                'attr' => [
                    'class' => 'datepicker',
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                ]
            ]
        )->add(
            'end_at',
            'text',
            [
                'required' => false,
                'attr'     => [
                    'class' => 'datepicker',
                ]
            ]
        )->add(
            'event_type',
            'choice',
            [
                'choices' => Validator\Events\EventTypeChoice::getChoicesAndLabels(),
                'constraints' => [
                    new Validator\Events\EventTypeChoice()
                ]
            ]
        )->add(
            'location',
            'text',
            [
                'constraints' => [
                    new Assert\NotBlank()
                ]
            ]
        )->add(
            'is_sponsored',
            'checkbox',
            [
                'required' => false,
            ]
        )->getForm();
    }

    protected function formStatus(
        array $data = [],
        array $options = []
    ) {
        return $this->app->form(
            $data,
            $options
        )->add(
            'status',
            'choice',
            [
                'choices' => Validator\Events\StatusChoice::getChoicesAndLabels(),
                'constraints' => [
                    new Validator\Events\StatusChoice()
                ]
            ]
        )->getForm();
    }
}
