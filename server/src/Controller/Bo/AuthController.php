<?php

namespace Controller\Bo;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

use Controller\ControllerActionAbstract;

/**
 * AuthController main action controller
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class AuthController extends ControllerActionAbstract
{
    use \Repository\SecurityRepositoryTrait;

    protected function setSession($isAuthenticated = false, $user = null)
    {
        $this->app['session']->set('isAuthenticated', $isAuthenticated);
        $this->app['session']->set('user', $user);
    }

    public function isAuthenticated(Request $request)
    {
        $ret = null;

        switch ($request->getRequestUri()) {
            case $this->app->path('bo_login'):
                if ($this->app['session']->get('isAuthenticated')) {
                    $ret = $this->app->redirect(
                        $this->app->url('bo_dashboard_index')
                    );
                }
                break;

            case $this->app->path('bo_logout'):
                if (!$this->app['session']->get('isAuthenticated')) {
                    $ret = $this->app->redirect(
                        $this->app->url('bo_login')
                    );
                }
                break;

            default:
                if (!$this->app['session']->get('isAuthenticated')) {
                    $ret = $this->app->redirect(
                        $this->app->url('bo_login')
                    );
                }
                break;
        }

        return $ret;
    }

    public function loginAction(Request $request)
    {
        $form = $this->form();

        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $data = $form->getData();

                $acl      = false;
                $ok       = false;

                try {
                    $hashedPassword = $this->generateHashedPasswordField($data['password']);
                    $user           = $this->app['repositories.users']->find(
                        ['where' => [['email' => $data['email'], 'password' => $hashedPassword['hashed_password']]]],
                        ['id', 'username', 'acl']
                    )->first();

                    if (!empty($user) && !empty($user->acl) && in_array($user->acl, \Validator\Users\BoAclTypeChoice::getChoices())) {
                        $ok = true;
                    }
                } catch (Exception $e) {
                    if ($this->app['debug'] == true) {
                        $this->app['session']
                            ->getFlashBag()
                            ->add(
                                'errors',
                                $e->getMessage()
                            );
                    }
                }

                if ($ok) {
                    $this->setSession(true, $user);

                    return $this->app->redirect(
                        $this->app->url('bo_dashboard_index')
                    );
                } else {
                    $this->app['session']
                        ->getFlashBag()
                        ->add(
                            'errors',
                            'Authentification failed'
                        );
                }
            }
        }

        return $this->app->render(
            'auth/signin.twig', [
                'display_contact_restaurant' => true,
                'form' => [
                    'view'   => $form->createView(),
                    'action' => $this->app->url(
                        'bo_login_post'
                    ),
                    'method' => 'POST'
                ]
            ]
        );
    }

    public function logoutAction(Request $request)
    {
        $this->setSession();
        $this->app['session']->clear();
        return $this->app->redirect(
            $this->app->url('bo_login')
        );
    }

    protected function form(array $data = array(), array $options = array())
    {
        return $this->app->form(
            $data,
            $options
        )->add(
            'email',
            'text',
            [
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Ce champ ne peut pas être vide',
                    ]),
                    new Assert\Email([
                        'message' => 'Addresse email invalide',
                    ]),
                ],
            ]
        )->add(
            'password',
            'password',
            [
                'label' => 'Mot de passe',
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Ce champ ne peut pas être vide',
                    ]),
                    new \Validator\Users\Password([
                        'minMessage' => 'Votre mot de passe doit faire au moins {{ limit }} caractères',
                        'maxMessage' => 'Votre mot de passe ne peut pas être plus long que {{ limit }} caractères',
                    ]),
                ],
            ]
        )->getForm();
    }

}
