<?php

namespace Utils;

use Repository\Exception as RepositoryException;

/**
 * Repository for manipulating images
 *
 * @author Jean-Sébastien Eude <jeude@gmail.com>
 */
class ImagesUtils extends UtilsAbstract
{
    const DEFAULT_QUALITY = 70;
    const DEFAULT_IMAGE_EXTENSION = 'png';

    /**
     * Upload picture from path.
     *
     * @param string $file Path from which upload will be made (URL accepted).
     * @param array $options Optional parameters such as destination, min_width/height allowed.
     * @return array
     */
    public function upload($file = null, $options = array()) {
        if (empty($file)) {
            throw new RepositoryException\InvalidDataException('Empty image source');
        }

        list($p_width, $p_height, $p_type) = getimagesize($file);

        if ($p_type != IMAGETYPE_JPEG && $p_type != IMAGETYPE_PNG) {
            throw new RepositoryException\InvalidDataException($p_type.' is not a supported imagetype.');
        }
        if (isset($options['min_width']) && !empty($options['min_width']) && $p_width < $options['min_width']) {
            throw new RepositoryException\InvalidDataException('Width lower than '.$options['min_width'].'.');
        }
        if (isset($options['min_height']) && !empty($options['min_height']) && $p_height < $options['min_height']) {
            throw new RepositoryException\InvalidDataException('Height lower than '.$options['min_height'].'.');
        }

        $options['saveAs'] = image_type_to_extension($p_type, false);

        return $this->save($file, $options);
    }

    /**
     * Call method to retrieve images depending on the provider passed
     *
     * @param string $file   String image path or url
     * @param array $options Array of options to be used
     *
     * @return array $images Array of images
     */
    public function resizeAndCrop(
        $file,
        array $options = []
    ) {
        list($source_width, $source_height, $source_type) = getimagesize($file);

        switch($source_type){
            case IMAGETYPE_WBMP:
                $source_gdim = imagecreatefromwbmp($file);
                break;
            case IMAGETYPE_GIF:
                $source_gdim = imagecreatefromgif($file);
                break;
            case IMAGETYPE_JPEG:
                $source_gdim = imagecreatefromjpeg($file);
                break;
            case IMAGETYPE_PNG:
                $source_gdim = imagecreatefrompng($file);
                break;
            default :
                throw new RepositoryException\InvalidDataException('Insupported image type.');
        }

        $width  = !empty($options['width']) ? $options['width'] : 640;
        $height = !empty($options['height']) ? $options['height'] : 960;

        if ($width && !$height) {
            $ratio       = $width / $source_width;
            $temp_width  = $width;
            $temp_height = $source_height * $ratio;

            $desired_gdim = imagecreatetruecolor($temp_width, $temp_height);
            imagecopyresampled($desired_gdim, $source_gdim, 0, 0, 0, 0, $temp_width, $temp_height, $source_width, $source_height);
        } else {
            $source_aspect_ratio  = $source_width / $source_height;
            $desired_aspect_ratio = $width / $height;

            if ($source_aspect_ratio > $desired_aspect_ratio) {
                $temp_height = $height;
                $temp_width  = (int)($height * $source_aspect_ratio);
            } else {
                $temp_width  = $width;
                $temp_height = (int)($width / $source_aspect_ratio);
            }

            $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
            imagecopyresampled($temp_gdim, $source_gdim, 0, 0, 0, 0, $temp_width, $temp_height, $source_width, $source_height);


            $x0 = ($temp_width - $width) / 2;
            $y0 = ($temp_height - $height) / 2;
            $desired_gdim = imagecreatetruecolor($width, $height);
            imagecopy($desired_gdim, $temp_gdim, 0, 0, $x0, $y0, $width, $height);
        }

        if (!isset($options['save']) || $options['save'] === true) {
            $temp_file = '/tmp/'.sha1(microtime().rand(1, 10000));
            $quality = !empty($options['quality']) ? $options['quality'] : self::DEFAULT_QUALITY;
            imagepng($desired_gdim, $temp_file, $quality);

            return $this->save($temp_file, $options);
        }

        return $this->display($desired_gdim);
    }

    /**
     * Display file
     *
     * @param string $resource  Resource to display
     * @param array $options    Options such as quality
     *
     * @return array
     */
    protected function display(
        $resource,
        $options = []
    ) {
        ob_start();
        $quality = !empty($options['quality']) ? $options['quality'] : self::DEFAULT_QUALITY;
        imagejpeg($resource, null, $quality);
        $image = ob_get_contents();
        ob_end_clean();
        imagedestroy($resource);

        return $image;
    }

    /**
     * Save file
     *
     * @param string $file      File path to process
     * @param array $options    Options such as saveAs, subdir.
     *
     * @return array
     */
    protected function save(
        $file,
        $options = []
    ) {
        $upload_hash = sha1(microtime().rand(1, 10000));
        $filename    = $upload_hash.'.'.(isset($options['saveAs']) && !empty($options['saveAs']) ? $options['saveAs'] : self::DEFAULT_IMAGE_EXTENSION);
        $filedir     = (isset($options['subdir']) && !empty($options['subdir']) ? $options['subdir'].'/' : '');
        $path        = '/'.$this->app['upload_path'].$filedir.$filename;
        $url         = 'http://'.$_SERVER['HTTP_HOST'].$path;
        $full_path   = dirname(__FILE__).'/../../public'.$path;

        list($p_width, $p_height, $p_type) = getimagesize($file);

        if (!copy($file, $full_path)) {
            throw new RepositoryException\RuntimeException('Unable to copy file in '.$path.'.');
        }

        if (file_exists($file)) {
            @unlink($file);
        }

        return [
            'url'        => $url,
            'filename'   => $filename,
            'path'       => $path,
            'dimensions' => [
                'width'  => $p_width,
                'height' => $p_height
            ],
        ];
    }
}