<?php

namespace Utils;

use Silex\Application;

abstract class UtilsAbstract
{
    /**
     * @var $app Silex application container instance
     **/
    protected $app;

    /**
     * Constructor
     *
     * @param Silex\Application $app silex application container instance
     */
    public function __construct(
        Application $app
    ) {
        $this->app = $app;
    }
}
