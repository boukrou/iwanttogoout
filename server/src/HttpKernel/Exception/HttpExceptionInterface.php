<?php

namespace HttpKernel\Exception;

/**
 * Interface for HTTP error exceptions.
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
interface HttpExceptionInterface
{
    /**
     * Returns the status code.
     * [cf Symfony\Component\HttpKernel\Exception\HttpExceptionInterface]
     *
     * @return integer An HTTP response status code
     */
    public function getStatusCode();

    /**
     * Returns response headers.
     * [cf Symfony\Component\HttpKernel\Exception\HttpExceptionInterface]
     *
     * @return array Response headers
     */
    public function getHeaders();

    /**
     * Extra data are included in response.
     *
     * @return array Response headers
     */
    public function getData();

    /**
     * Extra rawdata are included in response.
     *
     * @return array Response headers
     */
    public function getRawData();

    /**
     * Extends exception message
     *
     * @return $this for chaining
     */
    public function setMessage($message);
}
