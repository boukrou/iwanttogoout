<?php

namespace HttpKernel\Exception;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException as BaseHttpException;

/**
 * HttpException.
 *
 * @author Jean-Sébastien Eude <jseude@gmail.com>
 */
class HttpException extends BaseHttpException implements HttpExceptionInterface
{
    private $data;
    private $rawData;

    public function __construct(
        $statusCode,
        $message = null,
        array $data = array(),
        array $rawData = array(),
        Exception $previous = null,
        array $headers = array(),
        $code = 0
    ) {
        $this->data = $data;
        $this->rawData = $rawData;

        parent::__construct($statusCode, $message, $previous = null, $headers, $code);
    }

    public function getData()
    {
        return $this->data;
    }

    public function getRawData()
    {
        return $this->rawData;
    }

    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }
}
