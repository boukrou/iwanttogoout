#!/bin/bash

domain="api.iwanttogoout"

# clean db
host="localhost"
database="iwtgodev"
username="iwtgodev"
password="iwtgodev"

mysql -h $host -u $username -p$password $database < dump.sql;

# init admins
curl -sS -H 'Content-Type: application/json' -X PUT -d '{"email":"jseude@gmail.com","password":"changeme","team_id":'`shuf -i 1-3 -n 1`'}' $domain/users;
curl -sS -H 'Content-Type: application/json' -X PUT -d '{"email":"thethrower@hotmail.com","password":"changeme","team_id":'`shuf -i 1-3 -n 1`'}' $domain/users;
echo 'UPDATE users set `acl`=100' | mysql -h $host -u $username -p$password $database

# random location 5 kms around Paris
loc[1]="\"latitude\":48.82950082,\"longitude\":2.32686202"
loc[2]="\"latitude\":48.87736743,\"longitude\":2.3194758"
loc[3]="\"latitude\":48.83264067,\"longitude\":2.38241063"
loc[4]="\"latitude\":48.87271113,\"longitude\":2.41003503"
loc[5]="\"latitude\":48.85622622,\"longitude\":2.32093532"
loc[6]="\"latitude\":48.87130089,\"longitude\":2.29844248"
loc[7]="\"latitude\":48.82564409,\"longitude\":2.38957391"
loc[8]="\"latitude\":48.85527117,\"longitude\":2.3856615"
loc[9]="\"latitude\":48.87087659,\"longitude\":2.40038267"
loc[10]="\"latitude\":48.86690777,\"longitude\":2.32481094"
loc[11]="\"latitude\":48.87507402,\"longitude\":2.3445646"
loc[12]="\"latitude\":48.83231524,\"longitude\":2.30109995"
loc[13]="\"latitude\":48.85855022,\"longitude\":2.35063534"
loc[14]="\"latitude\":48.88254953,\"longitude\":2.32513477"
loc[15]="\"latitude\":48.8240137,\"longitude\":2.30617445"
loc[16]="\"latitude\":48.86878975,\"longitude\":2.34366826"
loc[17]="\"latitude\":48.84711221,\"longitude\":2.33362103"
loc[18]="\"latitude\":48.82095423,\"longitude\":2.33250929"
loc[19]="\"latitude\":48.85016744,\"longitude\":2.3591944"
loc[20]="\"latitude\":48.89067266,\"longitude\":2.35118522"
loc[21]="\"latitude\":48.87115073,\"longitude\":2.40595814"
loc[22]="\"latitude\":48.87222086,\"longitude\":2.36763719"
loc[23]="\"latitude\":48.87105674,\"longitude\":2.35903826"
loc[24]="\"latitude\":48.8874027,\"longitude\":2.39999766"
loc[25]="\"latitude\":48.88394043,\"longitude\":2.34757553"
loc[26]="\"latitude\":48.88039189,\"longitude\":2.34087643"
loc[27]="\"latitude\":48.86618938,\"longitude\":2.36140112"
loc[28]="\"latitude\":48.86436574,\"longitude\":2.34664627"
loc[29]="\"latitude\":48.84563706,\"longitude\":2.31910027"
loc[30]="\"latitude\":48.81646011,\"longitude\":2.36939906"
loc[31]="\"latitude\":48.8716985,\"longitude\":2.36782236"
loc[32]="\"latitude\":48.88170794,\"longitude\":2.34022195"
loc[33]="\"latitude\":48.81337192,\"longitude\":2.34730631"
loc[34]="\"latitude\":48.85145109,\"longitude\":2.3931047"
loc[35]="\"latitude\":48.85132635,\"longitude\":2.33597136"
loc[36]="\"latitude\":48.8828706,\"longitude\":2.34563113"
loc[37]="\"latitude\":48.82584863,\"longitude\":2.34337952"
loc[38]="\"latitude\":48.8294067,\"longitude\":2.37092682"
loc[39]="\"latitude\":48.84285369,\"longitude\":2.31948268"
loc[40]="\"latitude\":48.85458383,\"longitude\":2.37913293"
loc[41]="\"latitude\":48.89552984,\"longitude\":2.33207521"
loc[42]="\"latitude\":48.83740769,\"longitude\":2.30006613"
loc[43]="\"latitude\":48.87535395,\"longitude\":2.38974944"
loc[44]="\"latitude\":48.83904062,\"longitude\":2.3546621"
loc[45]="\"latitude\":48.87657616,\"longitude\":2.35709565"
loc[46]="\"latitude\":48.85049151,\"longitude\":2.30744238"
loc[47]="\"latitude\":48.86109454,\"longitude\":2.29174629"
loc[48]="\"latitude\":48.86117143,\"longitude\":2.3531691"
loc[49]="\"latitude\":48.85975504,\"longitude\":2.30574866"
loc[50]="\"latitude\":48.8467706,\"longitude\":2.39126205"
loc[51]="\"latitude\":48.86203196,\"longitude\":2.3346377"
loc[52]="\"latitude\":48.82673383,\"longitude\":2.33221558"
loc[53]="\"latitude\":48.84336573,\"longitude\":2.33098571"
loc[54]="\"latitude\":48.81730204,\"longitude\":2.34779054"
loc[55]="\"latitude\":48.86700855,\"longitude\":2.38617953"
loc[56]="\"latitude\":48.88156615,\"longitude\":2.31537935"
loc[57]="\"latitude\":48.87519105,\"longitude\":2.39443137"
loc[58]="\"latitude\":48.88679586,\"longitude\":2.34902124"
loc[59]="\"latitude\":48.85083715,\"longitude\":2.32574268"
loc[60]="\"latitude\":48.85223075,\"longitude\":2.35063883"
loc[61]="\"latitude\":48.87115332,\"longitude\":2.4126984"
loc[62]="\"latitude\":48.87743031,\"longitude\":2.31370703"
loc[63]="\"latitude\":48.89199106,\"longitude\":2.38596879"
loc[64]="\"latitude\":48.83396728,\"longitude\":2.34473491"
loc[65]="\"latitude\":48.84186944,\"longitude\":2.34675189"
loc[66]="\"latitude\":48.87299599,\"longitude\":2.38985756"
loc[67]="\"latitude\":48.86251894,\"longitude\":2.31123618"
loc[68]="\"latitude\":48.87595199,\"longitude\":2.29198011"
loc[69]="\"latitude\":48.83962534,\"longitude\":2.33715573"
loc[70]="\"latitude\":48.88565515,\"longitude\":2.30270542"
loc[71]="\"latitude\":48.83508946,\"longitude\":2.3564399"
loc[72]="\"latitude\":48.83469236,\"longitude\":2.34324677"
loc[73]="\"latitude\":48.88653668,\"longitude\":2.32773226"
loc[74]="\"latitude\":48.85601673,\"longitude\":2.36863621"
loc[75]="\"latitude\":48.87943896,\"longitude\":2.36698375"
loc[76]="\"latitude\":48.89146025,\"longitude\":2.37106876"
loc[77]="\"latitude\":48.82695442,\"longitude\":2.40236961"
loc[78]="\"latitude\":48.84569645,\"longitude\":2.37260321"
loc[79]="\"latitude\":48.82803397,\"longitude\":2.32671737"
loc[80]="\"latitude\":48.87909402,\"longitude\":2.406223"
loc[81]="\"latitude\":48.82621032,\"longitude\":2.33920239"
loc[82]="\"latitude\":48.88278663,\"longitude\":2.3266193"
loc[83]="\"latitude\":48.86877402,\"longitude\":2.40710809"
loc[84]="\"latitude\":48.89506261,\"longitude\":2.3340444"
loc[85]="\"latitude\":48.87608811,\"longitude\":2.36138706"
loc[86]="\"latitude\":48.88625866,\"longitude\":2.31483567"
loc[87]="\"latitude\":48.84344053,\"longitude\":2.40407653"
loc[88]="\"latitude\":48.8605075,\"longitude\":2.40528418"
loc[89]="\"latitude\":48.84994203,\"longitude\":2.38696297"
loc[90]="\"latitude\":48.86575294,\"longitude\":2.30585845"
loc[91]="\"latitude\":48.89717966,\"longitude\":2.34898016"
loc[92]="\"latitude\":48.8254944,\"longitude\":2.36769567"
loc[93]="\"latitude\":48.82618292,\"longitude\":2.39190189"
loc[94]="\"latitude\":48.89248431,\"longitude\":2.36864697"
loc[95]="\"latitude\":48.88109546,\"longitude\":2.33358696"
loc[96]="\"latitude\":48.87261465,\"longitude\":2.40912737"
loc[97]="\"latitude\":48.83205249,\"longitude\":2.35941157"
loc[98]="\"latitude\":48.89191617,\"longitude\":2.31248472"
loc[99]="\"latitude\":48.85324053,\"longitude\":2.28456752"
loc[100]="\"latitude\":48.8546225,\"longitude\":2.34327372"

event=1;

for i in `seq 3 100`;
do
    # insert user
    curl -sS -H 'Content-Type: application/json' -X PUT -d '{"email":"user'$i'@$database.fr","password":"password'$i'","team_id":'`shuf -i 1-3 -n 1`'}' $domain/users;

    token=`echo -n "password$i" | openssl sha1 -hmac "219340800000" | sed -e 's/^.* //'`;

    echo "UPDATE users set created_at=FROM_UNIXTIME("`shuf -i 1469090595-1482313404 -n 1`") where id="$i | mysql -h $host -u $username -p$password $database

    # delete random users
    # curl -sS -H 'Content-Type: application/json' -X DELETE $domain/users/$i;

    for j in `seq 1 10`;
    do
        rand=$[ $RANDOM % 100 + 1]

        #token=`echo -n "password$i" | openssl sha1 -hmac "219340800000" | sed -e 's/^.* //'`;

        # insert event
        curl -sS -u "$i:$token" -H 'Content-Type: application/json' -X PUT -d '{"user_id":'$i',"start_at":'`shuf -i 1469090595-1482313404 -n 1`',"event_type":'`shuf -i 1-3 -n 1`',"location":{'`echo ${loc[$rand]}`'}}' $domain/users/$i/events;

        echo "UPDATE events set start_at=FROM_UNIXTIME("`shuf -i 1469090595-1482313404 -n 1`"), created_at=FROM_UNIXTIME("`shuf -i 1469090595-1482313404 -n 1`") where id="$event | mysql -h $host -u $username -p$password $database
        ((event++))

        # update event
        # curl -sS -H 'Content-Type: application/json' -X POST -d '{"title":"test toto 4","user_id":1,"start_at":1467991639,"end_at":1467991839,"event_type":1,"description":"tupeuxpastest","location":{"latitude":44,"longitude":24}}' $domain/users/1/events/9;

        # delete event
        # curl -sS -H 'Content-Type: application/json' -X DELETE $domain/users/1/events/100
    done;

done;

for i in `seq 3 100`;
do
    for j in `seq 1 10`;
    do
        token=`echo -n "password$i" | openssl sha1 -hmac "219340800000" | sed -e 's/^.* //'`;

        rand=$[ $RANDOM % 980 + 1]

        if (( $rand % 5 == 0 ))
        then
            curl -u "$i:$token" -H 'Content-Type: application/json' -X POST $domain/users/$i/events/$rand/go;
        fi
    done;
done;