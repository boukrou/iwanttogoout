var classSearchbar = function(container, searchbar_input, searchbar_submit, has_changed_input, validated_input, map) {
	this.map = map;
	this.container = container;
	this.searchbar_input = searchbar_input;
	this.searchbar_submit = searchbar_submit;
	this.has_changed_input = has_changed_input;
	this.validated_input = validated_input;

	this.searchbar_input.addEventListener('keyup', function(e) {
		var address = this.searchbar_input.value.trim();
		if (address!="") {
			if (e.keyCode == 13) {
				e.preventDefault();
				e.stopPropagation();
				this.map.geocode(address, this.map.geocodeSuccess, this.map.geocodeError);
			} else {
				this.validated_input.value="0";
				this.has_changed_input.value="1";
				removeClass(this.container, ["hideSubmit"]);
				if (!hasClass(this.container, "showSubmit"))
					addClass(this.container, "showSubmit");
			}
		} else {
			this.validated_input.value="0";
			this.has_changed_input.value="0";
			removeClass(this.container, ["showSubmit"]);
			if (!hasClass(this.container, "hideSubmit"))
				addClass(this.container, "hideSubmit");
		}
	}.bind(this));

	this.searchbar_submit.addEventListener('click', function() {
		if (this.has_changed_input.value=="1") {
			var address = this.searchbar_input.value.trim();
			this.map.geocode(address, this.map.geocodeSuccess, this.map.geocodeError);
		}
	}.bind(this));
};