var classMap = function() {
    this.google = {};
    this.marker = [];

    if (typeof coordinates!=="undefined") {
        user_params = {"latitude":coordinates.latitude, "longitude":coordinates.longitude, "title":current_User.username, "image":"/assets/img/user_marker.png"};
        latitude = coordinates.latitude;
        longitude = coordinates.longitude;
    } else {
        user_params = {"latitude":current_User.latitude, "longitude":current_User.longitude, "title":current_User.username, "image":"/assets/img/user_marker.png"};
        latitude = current_User.latitude;
        longitude = current_User.longitude;
    }

    this.setUser = function(coordinates) {
        var latitude = longitude = "";
        if (this.user!=null)
            this.user.setMap(null);

        if (typeof coordinates!=="undefined") {
            user_params = {"latitude":coordinates.latitude, "longitude":coordinates.longitude, "title":current_User.username, "image":"/assets/img/user_marker.png"};
            latitude = coordinates.latitude;
            longitude = coordinates.longitude;
        } else {
            user_params = {"latitude":current_User.latitude, "longitude":current_User.longitude, "title":current_User.username, "image":"/assets/img/user_marker.png"};
            latitude = current_User.latitude;
            longitude = current_User.longitude;
        }
        
        this.user = this.setMarker(user_params);
        this.user.setMap(this.google.map);
        this.setCenter(latitude, longitude);
    };

    this.setCenter = function(latitude, longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.google.map.setCenter({lat: latitude, lng:longitude});
    };

    this.setMarker = function(paramsObject) {
        var latLngObject = {lat: paramsObject.latitude, lng: paramsObject.longitude};
        var defaultParams = {
            position: latLngObject
        };

        if (typeof paramsObject.title!=="undefined")
            defaultParams.title = paramsObject.title;

        if (typeof paramsObject.image!=="undefined")
            defaultParams.icon = paramsObject.image;

        return new google.maps.Marker(defaultParams);
    };

    this.addMarker = function(latitude, longitude, image) {
        var marker = null;
        if (typeof image!=="undefined") {
            marker = this.setMarker({"latitude":latitude, "longitude":longitude, "image":image});
        } else
            marker = this.setMarker({"latitude":latitude, "longitude":longitude});

        marker.setMap(this.google.map);
        this.marker.push(marker);
    };

    this.removeMarker = function(index, resetAll) {
        if (typeof resetAll!=="undefined") {
            var marker_length = this.marker.length;

            for (var i=0; i<marker_length; i++) {
                this.marker[i].setMap(null);
            }
            this.marker = [];

            return;
        }

        this.marker[index].setMap(null);
        this.marker.splice(index, 1);
    };

    this.geocode = function(address, callbackSuccess, callbackError) {
        var _this = this;
        this.google.geocoder.geocode({'address': address}, function(results, status) {
            (status == google.maps.GeocoderStatus.OK) ? callbackSuccess(_this, results[0].formatted_address, {"latitude": results[0].geometry.location.lat(), "longitude": results[0].geometry.location.lng()}, results[0]) : callbackError();
        });
    };
};


var classMapHome = function() {
    this.init = function() {
        this.google.geocoder = new google.maps.Geocoder();

        return this;
    };
};
classMapHome.prototype = new classMap();


var classMapEvent = function(map, coordinates) {
    this.layout = map;
    this.google = {};
    this.marker = [];
    this.user = null;
    if (typeof coordinates!=="undefined") {
        this.latitude = coordinates.latitude;
        this.longitude = coordinates.longitude;
    } else {
        this.latitude = current_User.latitude;
        this.longitude = current_User.longitude;
    }


    this.init = function() {
        this.setDimension();

        this.google.map = new google.maps.Map(this.layout, {
            center: {lat: this.latitude, lng: this.longitude},
            zoom: 16
        });

        this.google.geocoder = new google.maps.Geocoder();

        return this;
    };

    this.setUser = function(coordinates) {
        var latitude = longitude = "";
        if (this.user!=null)
            this.user.setMap(null);

        if (typeof coordinates!=="undefined") {
            user_params = {"latitude":coordinates.latitude, "longitude":coordinates.longitude, "title":current_User.username, "image":"assets/img/user_marker.png"};
            latitude = coordinates.latitude;
            longitude = coordinates.longitude;
        } else {
            user_params = {"latitude":current_User.latitude, "longitude":current_User.longitude, "title":current_User.username, "image":"assets/img/user_marker.png"};
            latitude = current_User.latitude;
            longitude = current_User.longitude;
        }
        
        this.user = this.setMarker(user_params);
        this.user.setMap(this.google.map);
        this.setCenter(latitude, longitude);
    };

    this.resize = function() {
        this.setCenter(this.latitude, this.longitude);
        this.setDimension();
    };
    resizeObj.addFunction(this.resize.bind(this));
    
    this.setDimension = function() {
        var map_height = window.innerHeight - document.getElementById("mainHeader").offsetHeight;
        this.layout.style.height = map_height + "px";
        document.getElementById("events_container").style.height = map_height + "px";
    };

    this.geocode = function(address, callback) {
        var _this = this;
        this.google.geocoder.geocode({'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat(), longitude = results[0].geometry.location.lng()
                _this.setUser({"latitude":latitude, "longitude":longitude});

                document.getElementById("location").value=results[0].formatted_address;
                document.getElementById("location_validated").value="1";
                document.getElementById("location_has_changed").value="0";

                callback({"latitude": latitude, "longitude": longitude});
            } else {
                if (document.getElementById("location").className !="error") {
                    addClass(document.getElementById("location"), "error");
                    setTimeout(function() {
                        removeClass(document.getElementById("location"), "error");
                    }, 1000);
                }
            }
        });
    };

    this.geocodeSuccess = function(coordinates) {
        url_parameters = cookie_default.get("form_event");
        event_api.getEvents("near", coordinates, url_parameters);
    };

    this.show = function() {
        document.getElementById("map_container").style.visibility = "visible";
    };
    this.hide = function() {
        document.getElementById("map_container").style.visibility = "hidden";
    };

    this.addEvent = function() {
        document.querySelector("#map_container #close").addEventListener("click", function() {
            this.hide();
        }.bind(this));

        document.getElementById("refresh").addEventListener("click", function() {
            layout_continuousRender.emptyDOM();
            url_parameters = cookie_default.get("form_event");
            event_api.getEvents(form_events.active_filter, null, url_parameters);
        }.bind(this));

        document.getElementById("geoloc").addEventListener("click", function() {
            getUserPosition(function (position) {
                current_User.setPosition(position);
                layout_continuousRender.emptyDOM();
                url_parameters = cookie_default.get("form_event");
                event_map.setUser();
                event_api.getEvents(form_events.active_filter, null, url_parameters);
            });
        }.bind(this));
    };

    this.addEvent();
};
classMapEvent.prototype = new classMap();


var classMapEventAdd = function(map) {
    this.layout = map;
    this.google = {};
    this.marker = [];

    if (typeof coordinates!=="undefined") {
        user_params = {"latitude":coordinates.latitude, "longitude":coordinates.longitude, "title":current_User.username, "image":"assets/img/user_marker.png"};
        latitude = coordinates.latitude;
        longitude = coordinates.longitude;
    } else {
        user_params = {"latitude":current_User.latitude, "longitude":current_User.longitude, "title":current_User.username, "image":"assets/img/user_marker.png"};
        latitude = current_User.latitude;
        longitude = current_User.longitude;
    }

    this.init = function() {
        this.google.map = new google.maps.Map(this.layout, {
            zoom: 16
        });

        this.google.geocoder = new google.maps.Geocoder();
        
        this.user = this.setMarker(user_params);
        this.user.setMap(this.google.map);
        this.setCenter(latitude, longitude);

        return this;
    };

    this.geocodeSuccess = function(parent, address, coordinates, geocode) {
        parent.setUser(coordinates);
        document.getElementById("form_coordinates").value = coordinates.latitude + "," + coordinates.longitude;
        document.getElementById("form_location_data").value = JSON.stringify(geocode);
        var errorGeocode = document.querySelector('div[data-for="map_add_event"]');
        addClass(errorGeocode, "hidden");
    };

    this.geocodeError = function() {
        var errorGeocode = document.querySelector('div[data-for="map_add_event"]');
        errorGeocode.innerHTML = "Address not found, please try another";
        removeClass(errorGeocode, ["hidden"]);
    };
}
classMapEventAdd.prototype = new classMap();
