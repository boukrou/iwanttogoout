var classEvent = function(jsonObject, structure) {
    this.id                 = jsonObject.id;
    this.type               = parseInt(jsonObject.event_type);
    this.start_at           = jsonObject.start_at;
    this.end_at             = jsonObject.end_at;
    this.latitude           = parseFloat(jsonObject.location.latitude);
    this.longitude          = parseFloat(jsonObject.location.longitude);
    this.distance           = parseInt(jsonObject.distance_in_meters);
    this.votes              = parseInt(jsonObject.votes_total);
    this.user               = {};
    this.user.id            = jsonObject.user.id;
    this.user.username      = jsonObject.user.username;
    this.user.team_id       = parseInt(jsonObject.user.team_id);
    this.layout             = {};
    this.layout.width       = 320;
    this.layout.structure   = structure;
    this.user.is_going      = parseInt(jsonObject.is_going);

    this.getStructure = function(index) {
        var element = document.createElement("article"), img_src = img_alt = "";
        // return;

        var structure = this.layout.structure;
        switch (this.user.team_id) {
            case 1:
                this.user.team_name = "valor";
                this.user.color   = "red";
            break;
            case 2:
                this.user.team_name = "mysthic";
                this.user.color   = "blue";
            break;
            case 3:
                this.user.team_name = "instinct";
                this.user.color   = "yellow";
            break;
        }
        switch (this.type) {
            case 1:
                img_src = "/assets/img/gym.svg";
                img_alt = "PokéGym";
            break;
            case 2:
                img_src = "/assets/img/pokestop.svg";
                img_alt = "PokéStop";
            break;
            case 3:
                img_src = "/assets/img/walk.svg";
                img_alt = "Group Walk";
            break;
        }

        var link = document.createElement("a");
        link.className="event_action";
        link.setAttribute("data-id", this.id);
        if (this.user.is_going==0) { // Join event
            link.innerHTML = event_language_array["event_join"];
            link.dataset.action="join";
        } else {
            link.innerHTML = event_language_array["event_in"];
            link.dataset.action="leave";
        }

        structure = structure.replace("_wording_join_", link.outerHTML);
        structure = structure.replace("_title_", event_language_array["event_title_type_"+jsonObject.event_type]);
        structure = structure.replace('src=""', 'src="' + img_src + '"');
        structure = structure.replace("_img_alt_", img_alt);
        structure = structure.replace("_distance_", this.distance);
        structure = structure.replace("_username_", this.user.username);
        structure = structure.replace("_userprofile_", window.location.protocol + "//" + window.location.hostname + "/users/" + this.user.id);
        structure = structure.replace("_votes_", this.votes);

        var datetime  = new Date(this.start_at*1000).getTime();
        var datenow   = new Date().getTime();
        this.gap_time = datetime - datenow;

        var pin_color = "pin-green";
        if (this.gap_time < 0) {
            pin_color = "pin-orange";
        }
        structure = structure.replace("_pin_color_", pin_color);

        element.innerHTML = structure;
        element.id = this.id;

        this.updateTimer(datetime, datenow, element);
        this.timer = setInterval(function() {
            this.updateTimer(datetime, datenow, this.layout.object)
        }.bind(this), 1000);

        return element;
    };

    this.updateTimer = function(datetime, datenow, elem) {
        currentTime = new Date().getTime();
        if (datetime - currentTime > 0) {
            var timeLeft = getTimeLeft(datetime);

            elem.querySelector(".date span").innerHTML = event_language_array['event_starts_in'] + " ";
            if (timeLeft.days > 1) {
                elem.querySelector(".date strong").innerHTML = timeLeft.days + " " + event_language_array["event_day"] + "s";
            } else if (timeLeft.days >= 1) {
                elem.querySelector(".date strong").innerHTML = timeLeft.days + " " + event_language_array["event_day"];
            } else {
                elem.querySelector(".date strong").innerHTML = timeLeft.hours + ":" + timeLeft.minutes + ":" + timeLeft.seconds;
            }
        } else {
            var timeSince = getTimeSince(datetime);

            elem.querySelector(".pin").className         = elem.querySelector(".pin").className.replace( /(?:^|\s)pin\-green(?!\S)/g , ' pin-orange');
            elem.querySelector(".date span").innerHTML   = "";
            elem.querySelector(".date strong").innerHTML = event_language_array["event_has_started"];

            var d = new Date(this.start_at*1000);
            elem.querySelector(".date").title = event_language_array["event_has_started_at"] + " " + (d.getHours() < 10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();
        }
        return;
    }

    this.addEvents = function() {
        this.layout.object.addEventListener("click", function(e) {
            if (e.target.dataset.action == "join") {
                event_api.joinEvent(e.target, this, this.updateVote);
                return;
            } else if (e.target.dataset.action == "leave") {
                event_api.quitEvent(e.target, this, this.updateVote);
                return;
            }

            event_map.setCenter(this.latitude, this.longitude);
            if (window.innerWidth < 1110)
                event_map.show();

        }.bind(this));
    };

    this.updateVote = function(eventObject, votes) {
        eventObject.layout.object.querySelector('[data-id="votes"] strong').innerHTML = votes;
    };

    this.layout.object  = this.getStructure(this.id);
    this.addEvents();
};