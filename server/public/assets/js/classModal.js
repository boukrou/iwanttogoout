var classModal = function() {
    this.layout = {};

    this.init = function() {
        document.body.className = "modal-close";
        document.body.insertBefore(this.layout.object, document.body.firstChild);

        return this;
    };

    this.open = function(element) {
        this.element = element;
        this.target = element.dataset.target;
        (typeof element.dataset.callback!=="undefined") ? this.callback = element.dataset.callback : this.callback = null;
        document.body.className = "modal-open";

        this.setState("loading");
        this.loadContent();
    };

    this.setState = function(state) {
        removeClass(this.layout.object, ["loading", "loaded"]);
        addClass(this.layout.object, state);
    };

    this.getStructure = function() {
        var structure = document.createElement("div");
        structure.className = "modal";
        structure.id = "modal";

        var content = document.createElement("div");
        content.id = "content";
        structure.appendChild(content);

        return structure;
    };

    this.layout.object = this.getStructure();

    this.close = function() {
        document.body.className = "modal-close";
    };

    this.loadContent = function() {
        var req = new XMLHttpRequest();
        req.open("GET", this.target, true);
        req.onreadystatechange = function () {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    var content = document.getElementById("content");
                    content.innerHTML = req.responseText;

                    if (this.element.id=="add_event")
                        this.loadMap();
                    else if (this.element.id=="edit_event")
                        this.loadMap(parseFloat(this.element.dataset.latitude), parseFloat(this.element.dataset.longitude));

                    this.setState("loaded");
                } else {
                    this.close();
                    location.reload();
                }
            }
        }.bind(this);
        req.send(null);
    };

    this.addEvents = function() {
        this.layout.object.addEventListener("click", function(e) {
            if (e.target.id == "close") {
                this.close();
                delete this;
            }
        }.bind(this));
    };

    this.loadMap = function(latitude, longitude) {
        var add_event_map_layout = document.getElementById("map_add_event");
        add_event_map = new classMapEventAdd(add_event_map_layout).init();
        if (typeof latitude==="undefined") {
            var latitude = current_User.latitude
            var longitude = current_User.longitude;
        }

        add_event_map.addMarker(latitude, longitude)
        add_event_map.setCenter(latitude, longitude);

        document.getElementById("form_coordinates").value = current_User.latitude + "," + current_User.longitude;
       
        if (typeof this.searchbar!=="undefined")
            delete this.searchbar;

        this.searchbar = new classSearchbar(document.getElementById("search_bar"), document.getElementById("location_add_event"), document.getElementById("submit_location_add_event"), document.getElementById("location_has_changed_add_event"), document.getElementById("location_validated_add_event"), add_event_map);
        
        if (typeof this.form==="undefined")
            delete this.form;
        
        this.form = new classFormAddEvent();
    };

    this.addEvents();
};
var global_modal = new classModal().init();
