var classForm = function() {

	this.request = function(form) {
		event.preventDefault();

		this.form = form;
		this.redirect = form.dataset.redirect;
		var action = form.action;
		var params = this.serializeInput();

		this.hideError();

		var xhttp = new XMLHttpRequest();
        xhttp.open("POST", action, true);
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4) {
                if (xhttp.status==200 ||xhttp.status==201) {
                    this.treatResponse(JSON.parse(xhttp.responseText));
                } else {
                    this.container.querySelector(".global-error").innerHTML("An error occured during submission, try again later.");
                }
            }
        }.bind(this);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(params);

	};

	this.hideError = function() {
		var error_collection = this.form.getElementsByClassName("alert-danger");
		var error_collection_length = error_collection.length;

		for (var i=0; i<error_collection_length; i++) {
			addClass(error_collection[i], "hidden");
		}
	};

	this.serializeInput = function() {
        if (!this.form || this.form.nodeName !== "FORM") {
            return;
        }
        var i, j, q = [];
        for (i = this.form.elements.length - 1; i >= 0; i = i - 1) {
            if (this.form.elements[i].name === "" || hasClass(this.form.elements[i], "ignore")) {
                continue;
            }

            switch (this.form.elements[i].nodeName) {
                case 'INPUT':
                switch (this.form.elements[i].type) {
                    case 'text':
                    case 'hidden':
                    case 'password':
                    case 'button':
                    case 'reset':
                    case 'submit':
                    q.push(this.form.elements[i].name + "=" + encodeURIComponent(this.form.elements[i].value));
                    break;
                    case 'checkbox':
                    case 'radio':
                    if (this.form.elements[i].checked) {
                        q.push(this.form.elements[i].name + "=" + encodeURIComponent(this.form.elements[i].value));
                    }
                    break;
                    case 'file':
                    break;
                }
                break;
                case 'TEXTAREA':
                q.push(this.form.elements[i].name + "=" + encodeURIComponent(this.form.elements[i].value));
                break;
                case 'SELECT':
                switch (this.form.elements[i].type) {
                    case 'select-one':
                    q.push(this.form.elements[i].name + "=" + encodeURIComponent(this.form.elements[i].value));
                    break;
                    case 'select-multiple':
                    for (j = this.form.elements[i].options.length - 1; j >= 0; j = j - 1) {
                        if (this.form.elements[i].options[j].selected) {
                            q.push(this.form.elements[i].name + "=" + encodeURIComponent(this.form.elements[i].options[j].value));
                        }
                    }
                    break;
                }
                break;
                case 'BUTTON':
                switch (this.form.elements[i].type) {
                    case 'reset':
                    case 'submit':
                    case 'button':
                    q.push(this.form.elements[i].name + "=" + encodeURIComponent(this.form.elements[i].value));
                    break;
                }
                break;
            }
        }
        return q.join("&");
	};

	this.treatResponse = function(jsonObject) {
		if (jsonObject.state=="success") {
			window.location = this.redirect;
		} else {
			if (typeof jsonObject.data.fields!=="undefined") {
				for (field in jsonObject.data.fields) {
					var label_error = this.form.querySelector('div.alert-danger[data-id="' + field + '"]');
					label_error.innerHTML = jsonObject.data.fields[field];
					removeClass(label_error, ["hidden"]);
				}
			} else {
				var label_error = this.form.querySelector(".global-error");
				label_error.innerHTML = jsonObject.data.global[0];
				removeClass(label_error, ["hidden"]);
			}
		}
	};
};
form_global = new classForm();

var classFormEvent = function(container) {
	this.input_collection = container.getElementsByTagName("input");
	this.active_filter = "near";
	var input_collection_length = this.input_collection.length;

	for (var i=0; i<input_collection_length; i++) {
		this.input_collection[i].addEventListener('change', function() {
			var url_parameters = this.getFormParameters(false, true);
    		event_api.getEvents(this.active_filter, null, url_parameters);
			
			this.updateFilters();
		}.bind(this));
	}

	var shortcut = document.getElementsByClassName('shortcut');
	var shortcut_length = shortcut.length;
	for (var i=0; i<shortcut_length; i++) {
		shortcut[i].addEventListener('click', function() {
			var input = document.querySelectorAll('input[name="' + this.dataset.target + '"]');
			var input_length = input.length;
			for (var j=0; j<input_length; j++) {
				input[j].checked = "checked";
			}

			var event = new Event("change");
			input[0].dispatchEvent(event);

			this.updateFilters();
		}.bind(this));
	}

	var filters_container = document.getElementById("elements");
	var filters = filters_container.getElementsByClassName("element");
	var filters_length = filters.length, _this = this;

	for (var i=0; i<filters_length; i++) {
		filters[i].addEventListener('click', function() {
			if (!hasClass(this, "active")) {
				var action = this.id;
    			_this.setActiveFilter(action);
				var url_parameters = _this.getFormParameters(false);
    			event_api.getEvents(_this.active_filter, null, url_parameters);
			}
		});
	}

	this.searchbar = new classSearchbar(document.getElementById("mainHeader"), document.getElementById("location"), document.getElementById("submit_location"), document.getElementById("location_has_changed"), document.getElementById("location_validated"), event_map);


	this.getFormParameters = function(all, setCookie) {
		var active_fields = this.getActiveFields(all), parameters = {}, url_parameters = "";
		var active_fields_length = active_fields.length;

		for (var i=0; i<active_fields_length; i++) {
			if (typeof parameters[active_fields[i].name]==="undefined")
				parameters[active_fields[i].name] = active_fields[i].value+",";
			else
				parameters[active_fields[i].name] += active_fields[i].value+",";
		}

		url_parameters_length = parameters.length;
		for (param in parameters) {
			url_parameters += param + "=" + parameters[param].substring(0, parameters[param].length - 1) + "&";
		}
		url_parameters = url_parameters.substring(0, (url_parameters.length-1));

		if (typeof setCookie!=="undefined" && cookie_default.get("form_event")!=null)
			cookie_default.set("form_event", url_parameters);
		
		return url_parameters;
	};

	this.getActiveFields = function(all) {
		var active_fields = [], input_collection_length = this.input_collection.length;

		if (all!=false) {
			for (var i=0; i<input_collection_length; i++) {
				active_fields.push(this.input_collection[i]);
			}
		} else {
			for (var i=0; i<input_collection_length; i++) {
				if (this.input_collection[i].checked)
					active_fields.push(this.input_collection[i]);
			}
		}

		return active_fields;
	};

	this.loadForm = function() {
        var form_parameters_string = cookie_default.get("form_event");
        var parameters = form_parameters_string.split("&");
        var parameters_length = parameters.length;

        for (var i=0; i<parameters_length; i++) {
        	var fields = parameters[i].split("=");
        	var values = [];
        	values.push(fields[1]);
        	if (values[0].indexOf(",")!=-1)
        		values = values[0].split(",");

        	var values_length = values.length;
        	for (var j=0; j<values_length; j++) {
        		document.querySelector('input[name="' + fields[0] + '"][value="' + values[j] + '"]').checked = "checked";
        	}
        }
	};

	this.checkOptions = function() {
		var url_parameters = this.getFormParameters(true);
		cookie_default.set("form_event", url_parameters);
		
		var input_collection_length = this.input_collection.length;
		for (var i=0; i<input_collection_length; i++) {
			this.input_collection[i].checked = "checked";
		}
	};

	this.updateFilters = function() {
		var filters_container = document.getElementById("elements");
		var filters = filters_container.getElementsByClassName("element");
		var filters_length = filters.length, url_base = window.location.protocol + "//" + window.location.hostname + "/events/", url = "";
        var url_parameters = cookie_default.get("form_event");

		for (var i=0; i<filters_length; i++) {
			var type = filters[i].dataset.type;

			switch (type) {
				case "recent":
    				url = url_base + "recent/" + event_map.latitude + "/" + event_map.longitude;
				break;
				case "near":
    				url = url_base + "near/" + event_map.latitude + "/" + event_map.longitude;
				break;
				case "popularity":
    				url = url_base + "popular/" + event_map.latitude + "/" + event_map.longitude;
				break;
			}
			
			if (url_parameters!=null)
				url += "?" + url_parameters;

			filters[i].dataset.request = url;
		}
	};

	this.setActiveFilter = function(action) {
		if (hasClass(document.getElementById(this.active_filter), "active"))
			removeClass(document.getElementById(this.active_filter), ["active"]);

		addClass(document.getElementById(action), "active");
		this.active_filter = action;
        cookie_default.set("event_action", action);
	};
};
classFormEvent.prototype = new classForm();


var classFormHome = function() {
	var _this = this;
	document.getElementById("location").addEventListener('keyup', function(e) {
		var address = this.value.trim();
		if (address!="" && e.keyCode == 13)
			home_map.geocode(address, _this.geocodeSuccess);
	});

	document.getElementById("submit_location").addEventListener('click', function() {
		if (document.getElementById("location_has_changed").value=="1") {
			var address = document.getElementById("location").value.trim();
			home_map.geocode(address, this.geocodeSuccess);
		}
	}.bind(this));

	this.geocodeSuccess = function(parentObject, address, coordinates) {
		var location_input = document.getElementById("location");
		if (address!=false)
			location_input.value = address;
		else {
			if (location_input.className !="error") {
                addClass(location_input, "error");
                setTimeout(function() {
                    removeClass(location_input, "error");
                }, 1000);
            }
       	}

		if (address != false)
			location.href = "/events?location=" + address + "&latitude=" + coordinates.latitude + "&longitude=" + coordinates.longitude;
	};
};
classFormHome.prototype = new classForm();


var classFormAddEvent = function() {
	this.form = document.getElementById("add_event");
	this.field = {};

	var input_collection = this.form.getElementsByTagName("input");
	var input_collection_length = input_collection.length;
	var select_collection = this.form.getElementsByTagName("select");
	var select_collection_length = select_collection.length;

	for (var i=0; i<input_collection_length; i++) {
		this.field[input_collection[i].id] = input_collection[i];
	}
	for (var i=0; i<select_collection_length; i++) {
		this.field[select_collection[i].id] = select_collection[i];
	}

	this.form.addEventListener("submit", function(e) {
		e.preventDefault();

    	var errorGeocode = document.querySelector('div[data-for="map_add_event"]');
		addClass(errorGeocode, "hidden");

		var params = this.serializeInput();
		var xhttp = new XMLHttpRequest();
        xhttp.open("POST", this.form.getAttribute("action"), true);
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4) {
                if (xhttp.status==200 ||xhttp.status==201) {
                    var jsonObject = JSON.parse(xhttp.responseText);
                    if (jsonObject.state=="success") {
                    	global_modal.close();

                        layout_continuousRender.emptyDOM();
                        url_parameters = cookie_default.get("form_event");
                        event_api.getEvents(form_events.active_filter, null, url_parameters);
                    } else {
        				errorGeocode.innerHTML = "An error occured during submission, try again later";
        				removeClass(errorGeocode, ["hidden"]);
        			}
                } else {
    				errorGeocode.innerHTML = "An error occured during submission, try again later";
    				removeClass(errorGeocode, ["hidden"]);
                }
            }
        }.bind(this);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(params);
	}.bind(this));

	document.getElementById("submit_add_event").addEventListener("click", function() {
		var event = new Event('submit');
		this.form.dispatchEvent(event);
	}.bind(this));
};
classFormAddEvent.prototype = new classForm();