var classColumn = function(index) {
    this.index              = index;
    this.height             = 0;
    this.elements_number    = 0;
};
classColumn.prototype.empty = function() {
    this.height = 0;
    this.elements_number = 0;
};