var classResize = function() {
	this.functionList = [];

	window.addEventListener("resize", function(event) {
        var resizeTimeout;
        clearTimeout(resizeTimeout);
        resizeTimeout = setTimeout(function(){
            var funcLength = this.functionList.length;

            for (var i=0; i<funcLength; i++) {
            	this.functionList[i].call();
            }
        }.bind(this), 200);
    }.bind(this), false);
};
classResize.prototype.addFunction = function(func) {
	this.functionList.push(func);
};
resizeObj = new classResize();