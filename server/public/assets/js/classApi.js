var classApiEvent = function() {
	this.base_url = window.location.protocol + "//" + window.location.hostname + "/events/";

	this.getEvents = function(action, coordinates, url_parameters) {
		this.url = (coordinates==null) ? this.base_url + action + "/" + event_map.latitude + "/" + event_map.longitude : this.base_url + action + "/" + coordinates.latitude + "/" + coordinates.longitude;
		
		if (url_parameters!=null)
	        this.url += "?"+url_parameters;

		var req = new XMLHttpRequest();
	    req.open("GET", this.url, true);
	    req.onreadystatechange = function () {
	        if (req.readyState == 4) {
	            if (req.status == 200) {
	        		layout_continuousRender.emptyDOM();
                    layout_continuousRender.column.list = [];
                    layout_continuousRender.element.list = [];
	                var jsonObject = JSON.parse(req.responseText);
	                if (jsonObject.data.length > 0) {

	                    /* Method should be called here to get diff of markers */
	                    event_map.removeMarker(0, true);
	                    
	                    layout_continuousRender.addColumns();
	                    layout_continuousRender.addElements(jsonObject.data);

	                    return;
	                } else {
	        			layout_continuousRender.emptyDOM();

	                    var error = document.createElement("p");
	                    error.className = "alert alert-warning";
	                    error.innerHTML = "There are no events close from you right now";
	                }
	            } else {
	        		layout_continuousRender.emptyDOM();
                    layout_continuousRender.column.list = [];
                    layout_continuousRender.element.list = [];
	        		
	                var error = document.createElement("p");
	                error.className = "alert alert-danger";
	                error.innerHTML = "A problem has occured";
	            }

                layout_continuousRender.container.appendChild(error);
                layout_continuousRender.setContainerWidth();
	        }
	    };
	    req.send(null);
	};

	this.joinEvent = function(link, eventObject, callback) {
		if (typeof link.dataset.id==="undefined")
            return;

		this.url = this.base_url + link.dataset.id + "/go";
		
		var req = new XMLHttpRequest();
	    req.open("POST", this.url, true);
	    req.onreadystatechange = function () {
	        if (req.readyState==4) {
	            if (req.status==200) {
	                var jsonObject = JSON.parse(req.responseText);
	            	if (jsonObject.state=="success") {
		            	link.dataset.action="leave";
		            	link.innerHTML=event_language_array["event_in"];
		            	callback(eventObject, jsonObject.data.votes_total);
	            	} else
	                	console.warn("error to leave event");
                } else if (req.status==401)
                    global_modal.open(window.location.protocol + "//" + window.location.hostname + '/signin');
	            else
	                console.warn("error to join event");
	        }
	    };
	    req.send(null);
	};

	this.quitEvent = function(link, eventObject, callback) {
		if (typeof link.dataset.id==="undefined")
            return;

		this.url = this.base_url + link.dataset.id + "/ungo";
		
		var req = new XMLHttpRequest();
	    req.open("POST", this.url, true);
	    req.onreadystatechange = function () {
	        if (req.readyState==4) {
	            if (req.status==200) {
	                var jsonObject = JSON.parse(req.responseText);
	            	if (jsonObject.state=="success") {
		            	link.dataset.action="join";
		            	link.innerHTML=event_language_array["event_join"];
		            	callback(eventObject, jsonObject.data.votes_total);
	            	} else
	                	console.warn("error to leave event");
	            } else if (req.status==401)
                    global_modal.open(window.location.protocol + "//" + window.location.hostname + '/signin');
	            else
	                console.warn("error to leave event");
	        }
	    };
	    req.send(null);
	};

};

var event_api = new classApiEvent();