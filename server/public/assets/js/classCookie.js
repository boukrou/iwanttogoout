var classCookie = function() {

	this.set = function(name, value) {
		if (value!=null && value!="")
			document.cookie = name + "=" + value;
	};

	this.get = function(name) {
		var name = name + "="; cookies = document.cookie.split(";");

	    for (var i = 0; i < cookies.length; i++) {
	        var current_cookie = cookies[i];
	        while (current_cookie.charAt(0) == " ") {
	            current_cookie = current_cookie.substring(1, current_cookie.length);
	        }
	        if (current_cookie.indexOf(name) == 0) {
	            return current_cookie.substring(name.length, current_cookie.length);
	        }
	    }

	    return null;
	};

	this.remove = function(name, value) {
		if (typeof value !== "undefined")
	        this.addCookie(name, "", -1, value);
	    else
	        this.addCookie(name, "", -1);
	}
};
var cookie_default = new classCookie();

