var classContinuousRender = function(container, element_width, gutter_value) {
    this.container              = container;
    this.container_minWidth     = 340;
    this.element                = {};
    this.element.width          = element_width;
    this.element.list           = [];
    this.element.gutter         = gutter_value;
    this.column                 = {};
    this.column.list            = [];

    this.addColumns = function() {
        var column_number = this.setContainerWidth();

        for (var i=0; i<column_number; i++) {
            var current_column = new classColumn(i);
            this.column.list.push(current_column);
        }
    };

    this.setContainerWidth = function() {
        var container_width = container.parentElement.offsetWidth;

        if (container_width < this.container_minWidth)
            container_width = this.container_minWidth

        var column_number = Math.floor(container_width / this.element.width);
        this.container.style.width = ((column_number * this.element.width) - this.element.gutter) + "px";

        return column_number;
    };

    this.setContainerHeight = function() {
        var required_column = maximum_height = 0;
        var column_length = this.column.list.length;
        for (var i=0; i<column_length; i++) {
            if (maximum_height < this.column.list[i].height) {
                maximum_height = this.column.list[i].height;
                required_column = i;
            } else if (i == 0) {
                maximum_height = this.column.list[i].height;
                required_column = i;
            }
        }
        var object_biggest_column = this.column.list[required_column];
        
        this.container.style.height = (object_biggest_column.height + this.element.gutter) + "px";
    };

    this.resize = function() {
        this.column.list = [];
        this.addColumns();

        var element_length = this.element.list.length;
        for (var i=0; i<element_length; i++) {
            this.injectInColumn(this.element.list[i]);
        }
    };
    resizeObj.addFunction(this.resize.bind(this));

    this.injectInColumn = function(element) {
        var column_length = this.column.list.length;
        var required_column = minimum_height = 0;
        for (var i=0; i<column_length; i++) {
            if (minimum_height > this.column.list[i].height) {
                minimum_height = this.column.list[i].height;
                required_column = i;
            } else if (i == 0) {
                minimum_height = this.column.list[i].height;
                required_column = i;
            }
        }
        var object_smallest_column = this.column.list[required_column];

        var windowWidth = window.innerWidth;
        if (object_smallest_column.height == 0) {
            if (windowWidth < 680 || (windowWidth > 768 && windowWidth <= 1662) || (windowWidth > 1110 && windowWidth < 1645)) {
                object_smallest_column.height = (current_User.id!="") ? 200 : 155;
            }
            else
                object_smallest_column.height = 125;
        }

        var top = (object_smallest_column.height + this.element.gutter), left = (required_column * this.element.width);

        element.top = top;
        element.left = left;

        element.layout.object.style.top = top + "px";
        element.layout.object.style.left = left + "px";
        element.layout.object.className = "card " + element.user.team_name + " " + element.user.color + " visible";

        object_smallest_column.height = object_smallest_column.height + element.layout.object.offsetHeight + this.element.gutter;
        object_smallest_column.elements_number++;
    };

    this.addElements = function(json_object) {
        var json_length = json_object.length;

        for (var i=0; i<json_length; i++) {
            var current_element = new classEvent(json_object[i], this.element.structure);
            this.element.list.push(current_element);
            this.container.appendChild(this.element.list[i].layout.object);
            var image = "";
            switch (current_element.type) {
                case 1:
                    switch (current_element.user.team_id) {
                        case 1:
                            image = "assets/img/marker-gym-red.png";
                        break;
                        case 2:
                            image = "assets/img/marker-gym-blue.png";
                        break;
                        case 3:
                            image = "assets/img/marker-gym-yellow.png";
                        break;
                    }
                break;
                case 2:
                    switch (current_element.user.team_id) {
                        case 1:
                            image = "assets/img/marker-pokestop-red.png";
                        break;
                        case 2:
                            image = "assets/img/marker-pokestop-blue.png";
                        break;
                        case 3:
                            image = "assets/img/marker-pokestop-yellow.png";
                        break;
                    }
                break;
                case 3:
                    switch (current_element.user.team_id) {
                        case 1:
                            image = "assets/img/marker-walk-red.png";
                        break;
                        case 2:
                            image = "assets/img/marker-walk-blue.png";
                        break;
                        case 3:
                            image = "assets/img/marker-walk-yellow.png";
                        break;
                    }
                break;
            }
            event_map.addMarker(current_element.latitude, current_element.longitude, image);
        }

        var element_length = this.element.list.length;
        for (var i=0; i<element_length; i++) {
            this.injectInColumn(this.element.list[i]);
        }

        this.setContainerHeight();
    };

    this.emptyDOM = function() {
        var structure = document.getElementById("model");
        if (structure!=null) {
            this.element.structure = structure.innerHTML;
            structure.parentNode.removeChild(structure);
        }


        var event_collection = this.container.getElementsByClassName("card");
        var event_collection_length = event_collection.length;

        for (var i=0; i<event_collection_length; i++) {
            this.container.removeChild(event_collection[0]);
        }

        var alert_collection = this.container.getElementsByClassName("alert");
        var alert_collection_length = alert_collection.length;
        for (var i=0; i<alert_collection_length; i++) {
            this.container.removeChild(alert_collection[i]);
        }
    };
};
