function addClass(target, className) {
   target.className = (target.className != "") ? target.className + " " + className : className;
}

function removeClass(target, arrayClassName) {
	var length = arrayClassName.length;
	for (var i=0; i<length; i++) {
		var regExp = new RegExp(arrayClassName[i], "gi");
		className = target.className.replace(regExp, "");
        target.className = className.trim();
	}
}

function hasClass(target, className) {
    if (target.className.indexOf(className)!=-1)
        return true;

    return false;
}

function getUserPosition(callback) {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(callback, errorPosition);
    } else {
        console.warn("Semi-static position from user");
        callback({
            'coords': {
                'latitude': getRandomArbitrary(48.83, 48.85),
                'longitude': getRandomArbitrary(2.3, 2.32)
            }
        });
    }
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

function errorPosition(err) {
    console.warn("ERROR(" + err.code + "): " + err.message);
}

function getTimeLeft(endtime){
    var t       = endtime - Date.parse(new Date());
    var seconds = "0" + Math.floor((t / 1000) % 60);
    var minutes = "0" + Math.floor((t / 1000 / 60) % 60);
    var hours   = "0" + Math.floor((t / (1000 * 60 * 60)) % 24);
    var days    = Math.floor(t / (1000 * 60 * 60 * 24));

    return {
        'seconds' : seconds.substr(-2),
        'minutes' : minutes.substr(-2),
        'hours'   : hours.substr(-2),
        'days'    : days
    };
}

function getTimeSince(date) {
    var t       = Math.floor((new Date() - date) / 1000);
    var seconds = "0" + Math.floor(t);
    var minutes = "0" + Math.floor(t / 60);
    var hours   = "0" + Math.floor(t / 3600);
    var days    = "0" + Math.floor(t / 86400);

    return {
        'seconds' : seconds.substr(-2),
        'minutes' : minutes.substr(-2),
        'hours'   : hours.substr(-2),
        'days'    : days.substr(-2)
    };
}

document.addEventListener("DOMContentLoaded", function(event) {
    var collection_clickTrigger = document.getElementsByClassName("clickTrigger");
    collection_clickTrigger_length = collection_clickTrigger.length;
    for (var i=0; i<collection_clickTrigger.length; i++) {
        collection_clickTrigger[i].addEventListener("click", function() {
            var target_id = this.dataset.target;
            document.getElementById(target_id).style.display = (document.getElementById(target_id).offsetParent === null) ? "block" : "none";
        });
    }

    var collection_popinTrigger = document.getElementsByClassName("popin");
    collection_popinTrigger_length = collection_popinTrigger.length;
    for (var i=0; i<collection_popinTrigger_length; i++) {
        collection_popinTrigger[i].addEventListener("click", function() {
            global_modal.open(this);
        });
    }
});

