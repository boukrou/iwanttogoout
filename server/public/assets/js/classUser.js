var classUser = function(jsonObject) {
    this.id         = jsonObject.id;
    this.username   = jsonObject.name;
    this.latitude   = 0;
    this.longitude  = 0;

    var _this = null;

    this.init = function() {
        if (!_this) {
            _this = this;
        }
        return _this;
    };

    this.setPosition = function(position) {
        _this.latitude  = position.coords.latitude;
        _this.longitude = position.coords.longitude;
    };

    function getPosition(position) {
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;

        initializeWorld();
    }

    return this.init();
};
