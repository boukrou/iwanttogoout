<?php

require __DIR__ . '/../src/bootstrap.php';

$app = new Application\Www();

$directoryMapping = [
    'users' => 'uploads'
];

$authorizedFormats = [
    'small' => [
        'width'  => 100,
        'height' => 100
    ],
    'large' => [
        'width'  => 400,
        'height' => 400
    ]
];

$params = explode('/', substr($_SERVER['REQUEST_URI'], 1));

if (count($params) != 3) {
    header("HTTP/1.0 204 No Content");
    die;
}

$imageType   = $params[0];
$imageFormat = $params[1];
$imageFile   = $params[2];

if (!in_array($imageType, array_keys($directoryMapping))) {
    header("HTTP/1.0 204 No Content");
    die;
}

if (!in_array($imageFormat, array_keys($authorizedFormats))) {
    header("HTTP/1.0 204 No Content");
    die;
}

$filePath = dirname(__FILE__).'/'.$directoryMapping[$imageType].'/'.$imageFile;

if (!file_exists($filePath)) {
    header("HTTP/1.0 204 No Content");
    die;    
}

$img = $app['utils.images']->resizeAndCrop(
    $filePath,
    [
        'width'  => $authorizedFormats[$imageFormat]['width'],
        'height' => $authorizedFormats[$imageFormat]['height'],
        'save'   => false
    ]
);

if (!empty($img)) {
    header('Content-Type: image/jpeg');
    echo $img;
    die;
}

header("HTTP/1.0 204 No Content");
die;