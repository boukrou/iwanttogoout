<?php

header("Access-Control-Allow-Origin: *");
require __DIR__ . '/../src/bootstrap.php';

use Application\Api;

$app = new Api();
$app->run();